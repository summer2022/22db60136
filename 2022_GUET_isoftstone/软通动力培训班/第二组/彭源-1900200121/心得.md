# 心得体会

    从一开始只听过openHarmony，到现在学会基于openHarmony的物联网项目编程，我经历过迷茫，也有过喜悦。在这两个周的学习中，我学会了基于openHarmony的各种IoT设备的控制代码的编写，同时也提高了我对openHarmony的学习兴趣，让我更期待能为openHarmony的发展做出贡献。

## 实训期待


    希望能了解到openHarmony目前的发展形势及趋势，并学习openHarmony的相关内容，理清openHarmony的学习路线和方向。


## 实训过程

    第一天：老师主要介绍了openHarmony及openHarmony的发展趋势和前景。并教我们如何搭建openHarmony的开发环境。

    第二天：学习了openHarmony编程及仿真，并以hello world程序和软件定时器为切入点，带领我们学习了基于openHarmony的代码编写。

    第三天：学习了任务管理系统及任务间通信的消息队列，从中，我了解到了openHarmony的任务管理机制及相关代码的编写，也学会了消息队列的应用。

    第四天：学习了OLED的显示，及按键的使用。 

    第五天：学习了基于openHarmony的wifi编程。涉及了ap及sta模式，并学习了UDP协议，尝试了客户端及服务端代码的编写。

    第六天：学习了TCP服务器的单客户端和多客户端的编写，及I2C的原理。
    
    第七天：学习I2C的编程，尝试读取温湿度传感器的数据。

    第八天：学习gitee的使用方式，尝试使用gitee进行小组合作及参与开源项目的贡献。

    第九天：小组通过gitee合作，完成物联网项目智能风扇的开发。

## 未来展望

    希望未来能够坚持openHarmony的学习，也希望能够在今后参与openHarmony的的贡献。





