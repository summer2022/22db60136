1. 三人行 必有我师。经石老师的指导及和同学们的交流，收获满满的一天。

2. WSL-docker-vscode 基于windows系统环境进行open harmony开发的三架马车
WSL2(Windows subsystem linux v2)：提供docker的运行环境
docker：提供资源消耗少的虚拟化环境
vscode：通过Remote-Containers插件连接容器

今天初次尝试了基于这种配置组合的开发环境，后续还要多实践和总结。 

3. git 及 gitee
git之前用的不多，下一步在工作中可以尝试用起来。