#心得体会
经过两周的生产实习实训，我们小组的成员团结协作，一起努力，克服困难，做出了一款智能风扇产品。
这两周里，我学到了很多关于openharmony的知识，如wifi联网，I2C通信，pwm控制电机，mqtt收发消息等等，
对各种传感器也有了一定的了解，知道了智能家居等产品是如何用网络控制的，还有就是团队之间如何合作，
自己完成自己那部分的代码，然后再组装到一起。
本次实习，我最大的收获就是收获了实际工作中的开发经验，这将对我以后的工作有很大的帮助。
最后，感谢石老师的辛勤付出和悉心教导，希望软通动力越办越好。 
#
