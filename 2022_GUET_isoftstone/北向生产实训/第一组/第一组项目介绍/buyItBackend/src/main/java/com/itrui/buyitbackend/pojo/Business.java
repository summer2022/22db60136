package com.itrui.buyitbackend.pojo;

import lombok.Data;

@Data
public class Business {

    private Integer     businessId      ;//'商家id',
    private String      businessName    ;// '商家名',
    private Integer     businessAccount ;// '商家账号',
    private String      businessPassword;// '商家密码',

}
