package com.itrui.buyitbackend.pojo;

import lombok.Data;

@Data
public class ProductPicture {
    private Integer picId  ;//    'id ',
    private String  picUrl ;//    '商品图片url',
    private Integer proId  ;// '商品',

}
