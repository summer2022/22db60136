package com.itrui.buyitbackend.service.Impl;

import com.itrui.buyitbackend.mapper.ProductMapper;
import com.itrui.buyitbackend.pojo.Product;
import com.itrui.buyitbackend.pojo.ProductPicture;
import com.itrui.buyitbackend.service.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductMapper productMapper;

    @Override
    public List<Product> getAllProducts() {
        return productMapper.getAllProducts();
    }

    @Override
    public boolean addProduct(Product product) {
        return productMapper.addProduct(product) > 0;
    }

    @Override
    public boolean addProductPicture(ProductPicture picture) {
        return productMapper.addProductPicture(picture) > 0;
    }

    @Override
    public Product getProductByNameAndTypeAndStatus(Product product) {
        return productMapper.getProductByNameAndTypeAndStatus(product);
    }

    @Override
    public List<Product> getProductByName(String name) {
        return productMapper.getProductByName(name);
    }

    @Override
    public List<Product> getProductByType(String type) {
        return productMapper.getProductByType(type);
    }

    @Override
    public boolean delProductById(Integer id) {
        return productMapper.delProductById(id) > 0;
    }

    @Override
    public boolean delProductPic(Integer picId) {
        return productMapper.delProductPic(picId) > 0;
    }

    @Override
    public boolean updateProduct(Product product) {
        return productMapper.updateProduct(product) > 0;
    }

    @Override
    public Product getProductById(Integer id) {
        return productMapper.getProductById(id);
    }
}
