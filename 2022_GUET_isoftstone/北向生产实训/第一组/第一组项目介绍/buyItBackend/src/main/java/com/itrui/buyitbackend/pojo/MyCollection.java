package com.itrui.buyitbackend.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class MyCollection {

    private Integer     collectionId        ;// '收藏id',
    private Date        collectionCreatetime;//'收藏时间',

    private Integer   collectionUserid    ;//'收藏人',
    private User        collectionUser      ; //一对一

    private Integer   collectionProid     ;//'收藏商品',
    private Product     collectionPro       ; //一对一
    private Integer collectionNum;

}
