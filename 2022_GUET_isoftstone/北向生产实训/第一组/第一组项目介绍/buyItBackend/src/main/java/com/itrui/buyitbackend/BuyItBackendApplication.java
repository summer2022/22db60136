package com.itrui.buyitbackend;

import com.itrui.buyitbackend.controller.WebSocketController;
import com.itrui.buyitbackend.service.MessageReceiveService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class BuyItBackendApplication {

    public static void main(String[] args) {
        //SpringApplication.run(BuyItBackendApplication.class, args);
        SpringApplication springApplication = new SpringApplication(BuyItBackendApplication.class);
        ApplicationContext applicationContext = springApplication.run(args);
        // 获取Spring IOC容器中的Service并注入
        MessageReceiveService myService = applicationContext.getBean(MessageReceiveService.class);
        WebSocketController.setMyService(myService);

    }

}
