package com.itrui.buyitbackend.mapper;

import com.itrui.buyitbackend.pojo.MyCollection;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface MyCollectionMapper {

    /**
     * 查找用户收藏
     * @param account
     * @return
     */
    public List<MyCollection> getAllCollection(@Param("account") Integer account);

    /**
     * 删除用户收藏
     * @param id
     * @return
     */
    public int delCollection(@Param("id") Integer id);


    /**
     * 添加收藏
     * @param myCollection
     * @return
     */
    public int addCollection(MyCollection myCollection);

    /**
     * 获取收藏数量
     * @param account
     * @return
     */
    public int getCollectionNumInt(@Param("account") Integer account);
}
