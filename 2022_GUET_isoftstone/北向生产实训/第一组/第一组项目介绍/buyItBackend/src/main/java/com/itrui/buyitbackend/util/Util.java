package com.itrui.buyitbackend.util;

import com.itrui.buyitbackend.pojo.Product;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Util {
    /**
     * @param data 指定字符串
     * @param str 需要定位的特殊字符或者字符串
     * @param num   第n次出现
     * @return  第n次出现的位置索引
     */
    public static int getIndexOf(String data, String str, int num){


        Pattern pattern = Pattern.compile(str);
        Matcher findMatcher = pattern.matcher(data);
        //标记遍历字符串的位置
        int indexNum=0;
        while(findMatcher.find()) {
            indexNum++;
            if(indexNum==num){
                break;
            }
        }
        return findMatcher.start();
    }

}
