package com.itrui.buyitbackend.controller;


import com.itrui.buyitbackend.common.Code;
import com.itrui.buyitbackend.common.Result;
import com.itrui.buyitbackend.pojo.Car;
import com.itrui.buyitbackend.pojo.MyCollection;
import com.itrui.buyitbackend.pojo.Product;
import com.itrui.buyitbackend.service.CarMapperService;
import com.itrui.buyitbackend.util.SetProductPicHeader;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/car")
public class CarController {
    @Autowired
    private CarMapperService carMapperService;

    /**
     * 查找用户收藏
     * @param account
     * @return
     */
    @GetMapping("/{account}")
    public Result getAllCar(HttpServletRequest request, @PathVariable Integer account){
        String url = request.getRequestURL().toString();
        List<Car> allCar = carMapperService.getAllCar(account);
        for (int i = 0; i < allCar.size();i++){
            Product product = SetProductPicHeader.setOnlyProduct(allCar.get(i).getCarPro(), url);
            allCar.get(i).setCarPro(product);
        }

        return new Result(Code.GET_OK,allCar,"查询成功");
    };

    /**
     * 删除用户收藏
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public Result delCar(@PathVariable Integer id){

        boolean flag = carMapperService.delCar(id);

        return new Result(flag?Code.DELETE_OK:Code.DELETE_ERR,flag?"删除成功":"删除失败");
    };



    /**
     * 添加收藏
     * @param car
     * @return
     */
    @PostMapping
    public Result addCar(@RequestBody Car car){
        car.setCarCreatetime(new Date());
        boolean flag = carMapperService.addCar(car);
        return new Result(flag?Code.SAVE_OK:Code.SAVE_ERR,flag?"添加成功":"添加失败");
    };

    /**
     * @param account
     * @return
     */
    @GetMapping("/num/{account}")
    public int getCarNum(@PathVariable Integer account){

        log.info("account购物车数量账号："+account);

        int carNum = carMapperService.getCarNum(account);
        return carNum;

    };
}
