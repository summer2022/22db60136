package com.itrui.buyitbackend.controller;

import com.itrui.buyitbackend.pojo.BlackList;
import com.itrui.buyitbackend.service.MessageReceiveService;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/unread")
public class MessageController {
    @Autowired
    private MessageReceiveService messageReceiveService;

    /**
     * 获取用户未读消息数量
     * @param user
     * @param target
     * @return
     */
    @GetMapping("/{user}/{target}")
    public Integer getUnreadCount(@PathVariable Integer user, @PathVariable Integer target){
        log.info("user："+user);
        log.info("target："+target);
        return messageReceiveService.getUnreadCount(user, target);
    };

    /**
     * 接受参数：同上，类型
     * 返回：成功或者失败
     * 函数：添加黑名单
     */
    @GetMapping("/black/{user}/{target}/{type}")
    public boolean addBlackList(@PathVariable Integer user, @PathVariable Integer target,@PathVariable Integer type){
        boolean flag = messageReceiveService.addBlackList(user, target, type);
        return flag;
    };

    /**
     * 清除未读消息
     * @param user
     * @param target
     * @return
     */
    @GetMapping("/delm/{user}/{target}")
    public boolean delMessage(@PathVariable Integer user, @PathVariable Integer target){
       return messageReceiveService.delMessgeById(user, target) > 0;
    };

    /**
     * 清除历史消息
     * @param user
     * @param target
     * @return
     */
    @GetMapping("/dela/{user}/{target}")
    public boolean delALlMessage(@PathVariable Integer user, @PathVariable Integer target){

        return messageReceiveService.delALlMessage(user, target);
    };

    /**
     * 检查用户是否存在黑名单
     * @param user
     * @param target
     * @param type
     * @return
     */
    @GetMapping("/isblack/{user}/{target}/{type}")
    public boolean isBlack(@PathVariable Integer user,@PathVariable Integer target, @PathVariable Integer type){
       log.info("用户是否存在黑名单："+user+"/"+target+"/"+type);
        boolean flag = false;
        List<BlackList> black = messageReceiveService.isBlack(user, target, type);
        log.info("黑名单信息：" + black);
        log.info("黑名单信息长度：" + black.size());
        if (black.size() != 0){
            flag = true;
        }
        return flag;
    };


    /**
     * 解除黑名单
     * @param user
     * @param target
     * @return
     */
    @GetMapping("/delblack/{user}/{target}/{type}")
    public boolean delBlack(@PathVariable Integer user, @PathVariable Integer target, @PathVariable Integer type){
        log.info("解除的黑名单："+user+"/"+target+"/"+type);
        boolean b = messageReceiveService.delBlack(user, target, type);
        return b;
    };

}
