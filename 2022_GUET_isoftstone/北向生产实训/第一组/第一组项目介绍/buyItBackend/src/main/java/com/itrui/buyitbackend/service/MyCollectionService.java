package com.itrui.buyitbackend.service;

import com.itrui.buyitbackend.pojo.MyCollection;
import org.apache.ibatis.annotations.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface MyCollectionService {
    /**
     * 查找用户收藏
     * @param account
     * @return
     */
    public List<MyCollection> getAllCollection(@Param("account") Integer account);

    /**
     * 删除用户收藏
     * @param id
     * @return
     */
    public boolean delCollection(@Param("id") Integer id);


    /**
     * 添加收藏
     * @param myCollection
     * @return
     */
    public boolean addCollection(MyCollection myCollection);

    /**
     * 获取收藏数量
     * @param account
     * @return
     */
    public int getCollectionNumInt(Integer account);
}
