package com.itrui.buyitbackend.mapper;

import com.itrui.buyitbackend.pojo.Business;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface BusinessMapper {
    /**
     * 通过账号登录
     * @param account
     * @return
     */
    public Business login(@Param("account") Integer account);

    /**
     * 商户注册
     * @param business
     * @return
     */
    @Insert("insert into business (business_name, business_account, business_password) " +
            "VALUES (#{businessName}, #{businessAccount}, #{businessPassword})")
    public int businessRegister(Business business);

    /**
     * 商户信息
     * @param id
     * @return
     */
    public Business getBusinessInfo(@Param("id") Integer id);
}
