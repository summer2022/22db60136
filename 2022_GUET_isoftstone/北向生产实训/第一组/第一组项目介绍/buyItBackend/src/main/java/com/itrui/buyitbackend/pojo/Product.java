package com.itrui.buyitbackend.pojo;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class Product {

    private Integer proId;//   商品id',
    private String proType;// '商品分类',
    private String proName;// '商品名',
    private Float price;// '价格',
    private Integer stock;// '库存',
    private String proMainPicture;// '商品主图',

    private Integer proPicture;// '商品图片',
    private List<ProductPicture> pictures;//一对多

    /*private Integer business;// '商家',*/
    private Business business; //一对一

    /*private Integer buyer;// '购买人',*/
    private User buyer; //一对一

    private Date proCreatetime;// '发布时间',
    private Date proBuytime;// '被购买时间',
    private String proStatus;// 状态
    private String introduce;// 商品介绍

}
