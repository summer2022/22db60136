package com.itrui.buyitbackend.service;

import com.itrui.buyitbackend.pojo.Order;
import org.apache.ibatis.annotations.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface OrderService {
    /**
     * 获取用户订单
     * @param account
     * @return
     */
    public List<Order> getALlOrderByUserAccount(Integer account);

    /**
     * 支付成功后修改用户订单装状态
     * @param order
     * @return
     */
    public boolean updateOrderStatusById(Order order);

    /**
     * 分类查找
     * @param account
     * @param type
     * @return
     */
    public List<Order> getAllOrderByType(Integer account, String type);

    /**
     * 添加订单
     * @param order
     * @return
     */
    public boolean addOrder(Order order);

    /**
     * 删除订单通过id
     * @param id
     * @return
     */
    public boolean delOrderById(Integer id);
}
