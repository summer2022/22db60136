package com.itrui.buyitbackend.service.Impl;

import com.itrui.buyitbackend.mapper.MessageReceiveMapper;
import com.itrui.buyitbackend.pojo.AllMessage;
import com.itrui.buyitbackend.pojo.BlackList;
import com.itrui.buyitbackend.pojo.MessageReceive;
import com.itrui.buyitbackend.service.MessageReceiveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MessageReceiveServiceImpl implements MessageReceiveService {

    @Autowired
    private MessageReceiveMapper messageReceiveMapper;

    @Override
    public boolean saveMessage(MessageReceive message) {
        return messageReceiveMapper.saveMessage(message) > 0;
    }

    @Override
    public List<MessageReceive> getMessage(Integer userId) {
        return messageReceiveMapper.getMessage(userId);
    }

    @Override
    public Integer delMessgeById(Integer userId,Integer target) {
        return messageReceiveMapper.delMessgeById(userId,target);
    }

    @Override
    public List<MessageReceive> isUserExist(Integer userId) {
        return messageReceiveMapper.isUserExist(userId);
    }

    @Override
    public boolean addBlackList(Integer userId, Integer target, Integer type) {
        return messageReceiveMapper.addBlackList(userId,target,type) > 0;
    }

    @Override
    public BlackList isExistenceBlackList(Integer userId, Integer target, Integer type) {
        return messageReceiveMapper.isExistenceBlackList(userId, target, type);
    }

    @Override
    public boolean saveAllMessage(AllMessage allMessage) {
        return messageReceiveMapper.saveAllMessage(allMessage) > 0;
    }

    @Override
    public List<AllMessage> getAllMessage(Integer userId, Integer target) {
        return messageReceiveMapper.getAllMessage(userId, target);
    }

    @Override
    public Integer getUnreadCount(Integer userId, Integer target) {
        return messageReceiveMapper.getUnreadCount(userId, target);
    }

    @Override
    public boolean delALlMessage(Integer userId, Integer target) {
        return messageReceiveMapper.delALlMessage(userId, target) >0;
    }

    @Override
    public List<BlackList> isBlack(Integer userId, Integer target, Integer type) {
        return messageReceiveMapper.isBlack(userId, target, type);
    }

    @Override
    public boolean delBlack(Integer userId, Integer target,Integer type) {
        return messageReceiveMapper.delBlack(userId,target,type) > 0;
    }
}
