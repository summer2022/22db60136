package com.itrui.buyitbackend.controller;

import com.itrui.buyitbackend.common.Code;
import com.itrui.buyitbackend.common.Result;
import com.itrui.buyitbackend.pojo.Product;
import com.itrui.buyitbackend.pojo.ProductPicture;
import com.itrui.buyitbackend.service.ProductService;
import com.itrui.buyitbackend.util.SetProductPicHeader;
import com.itrui.buyitbackend.util.Util;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/products")
public class ProductController {

    @Autowired
    private ProductService productService;

    /**
     * 查找全部图片
     * @return
     */
    @GetMapping
    public Result getAllProducts(HttpServletRequest request){
        String url = request.getRequestURL().toString();
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd :hh:mm:ss");
        try {
            List<Product> allProducts = productService.getAllProducts();
            log.info("查询到的数据"+allProducts.toString());
            List<Product> products = SetProductPicHeader.setProductList(allProducts, url);
            return new Result(Code.GET_OK,products,"查询成功");

        }catch (Exception e){

        }
        return new Result(Code.GET_ERR,"查询失败");
    };

    @PostMapping()
    public Result addProduct(@RequestBody Product product) {

        log.info("添加的信息："+product.toString());
        //设置发布日期
        Date date = new Date();
        product.setProCreatetime(date);
        boolean addProFlag = productService.addProduct(product);
        for (ProductPicture picture : product.getPictures()){
            picture.setProId(product.getProPicture());
            boolean flag = productService.addProductPicture(picture);
            if (!flag){
                return new Result(Code.SAVE_ERR,"图片上传失败");
            }
        }
        return new Result(addProFlag?Code.SAVE_OK:Code.SAVE_ERR,addProFlag?"添加成功":"添加失败");
    }

    /**
     * 条件查询
     * @param product
     * @return
     */
    @PostMapping("/condition")
    public Result getProductByNameAndTypeAndStatus(HttpServletRequest request, @RequestBody Product product){
        String url = request.getRequestURL().toString();
        String proName = "%"+product.getProName()+"%";
        product.setProName(proName);
        Product productByNameAndTypeAndStatus = productService.getProductByNameAndTypeAndStatus(product);
        Product product1 = SetProductPicHeader.setProduct(productByNameAndTypeAndStatus, url);

        log.info("查询的信息："+product1.toString());
        return new Result(Code.GET_OK,product1,"查询成功");
    };

    /**
     * 通过商品名查询
     * @param name
     * @return
     */
    @GetMapping("name/{name}")
    public Result getProductByName(HttpServletRequest request, @PathVariable String name){
        String url = request.getRequestURL().toString();

        String selectName = "%"+name+"%";
        log.info("查询的商品名"+selectName);

        List<Product> productByName = productService.getProductByName(selectName);
        List<Product> products = SetProductPicHeader.setProductList(productByName, url);

        log.info("查询到的商品信息:"+products);

        return new Result(Code.GET_OK,products,"查询成功");
    };


    /**
     * 通过商品类查找商品
     * @param type
     * @return
     */
    @GetMapping("type/{type}")
    public Result getProductByType(HttpServletRequest request, @PathVariable String type){
        String url = request.getRequestURL().toString();
        log.info("查找的商品类型" + type);
        List<Product> productByType = productService.getProductByType(type);
        List<Product> products = SetProductPicHeader.setProductList(productByType, url);
        log.info("按类型查找到的商品" + products);
        return new Result(Code.GET_OK,products,"查询成功");
    };

    /**
     * 通过id删除商品
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public Result delProductById(@PathVariable Integer id){
        log.info("要删除商品的id"+id);
        boolean delProductByIdFlag = productService.delProductById(id);
        boolean delProductPicFlag = productService.delProductPic(id);

        return new Result(delProductByIdFlag && delProductByIdFlag?Code.DELETE_OK:Code.DELETE_ERR,
                delProductByIdFlag && delProductByIdFlag?"删除成功":"删除失败");

    };


    /**
     * 修改商品信息
     * @param product
     * @return
     */
    @PutMapping
    public Result updateProduct(@RequestBody  Product product){
        log.info("修改的商品信息" + product.toString());
        boolean flag = productService.updateProduct(product);
        return new Result(flag?Code.UPDATE_OK:Code.UPDATE_ERR,flag?"修改成功":"修改失败");
    };

    /**
     * 通过商品id查找
     * @param id
     * @return
     */
    @GetMapping("/id/{id}")
    public Result getProductById(HttpServletRequest request,@PathVariable Integer id){
        String url = request.getRequestURL().toString();
        Product productById = productService.getProductById(id);
        Product product = SetProductPicHeader.setProduct(productById, url);
        return new Result(Code.GET_OK,product,"查询成功");
    };

}
