package com.itrui.buyitbackend.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class MessageReceive {
    private int type;
    private int sender;
    private int target;
    private String content;
    private String datetime;
}
