package com.itrui.buyitbackend;

import com.itrui.buyitbackend.pojo.MessageReceive;
import com.itrui.buyitbackend.pojo.Product;
import com.itrui.buyitbackend.service.MessageReceiveService;
import com.itrui.buyitbackend.service.ProductService;
import com.sun.xml.internal.ws.api.client.WSPortInfo;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class BuyItBackendApplicationTests {

    @Autowired
    private MessageReceiveService messageReceiveService;

    @Autowired
    private ProductService productService;

    @Test
    void contextLoads() {


        MessageReceive messageReceive = new MessageReceive();
        messageReceive.setDatetime("11111");
        messageReceive.setSender(10086);
        messageReceive.setContent("在吗？");
        messageReceive.setTarget(100);
        boolean b = messageReceiveService.saveMessage(messageReceive);
        System.out.println(b);

        List<MessageReceive> message = messageReceiveService.getMessage(10086);
        System.out.println(message.toString());

    }

}
