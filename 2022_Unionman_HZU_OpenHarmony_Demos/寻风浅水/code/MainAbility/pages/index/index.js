import router from '@ohos.router';
export default {
    data:{
      record:'0%',
        timeInt: 60
    },

    onShow(){

        var c=this.$refs.myCan;
        var ctx=c.getContext("2d");


        //画出敌人
        function drawenemy(imge){
            var img=new Image();
            img.src=imge;
            ctx.drawImage(img,enemy.position.x,enemy.position.y,enemy.width,enemy.height);

        }

        //让敌人运动
        function EnemyRun(){
            drawenemy('common/images/enemy.jpg');
            enemy.position.x+=enemy.velocity.x;
            if(enemy.position.x+enemy.height-20==platform1.position.x) {
                enemy.velocity.x=2;
            }
            else if(enemy.position.x+enemy.width+enemy.height-20==platform1.position.x+platform1.width){
                enemy.velocity.x=-2;
            }
        }

        //画出人物
        function draw(imge){
            var img=new Image();
            img.src=imge;
            ctx.drawImage(img,player.position.x,player.position.y,player.width,player.height);
        }


        //画出悬浮物
        function drawx1(platform){
            ctx.fillStyle='white';
            ctx.fillRect(platform.position.x,platform.position.y,platform.width,platform.height);
        }


        //画出道具
        function drawd(imge){
            var img=new Image();
            img.src=imge;
            ctx.drawImage(img,tool.position.x,tool.position.y,tool.width,tool.height);
        }


        //使物块在悬浮物上站立
        function levitation(object){
            if(player.position.y+player.height<=object.position.y&&
            player.position.y+player.height+player.velocity.y>=object.position.y&&
            player.position.x+player.width>=object.position.x&&
            player.position.x<=object.position.x+object.width){
                player.velocity.y=0;
            }
        }


        //提供重力
        function update(){
            draw('common/images/2.png');
            player.position.x+=player.velocity.x;
            player.position.y+=player.velocity.y;
            if(player.position.y+player.height+player.velocity.y<=250){
                player.velocity.y+=gravity;
            }else{
                player.velocity.y=0;
            }
        }


        function GameStart(){
            router.push({
                url:'pages/second/second'
            })
            player.position.x=70;
            player.position.y=100;
            platform.position.x=50;
            platform.position.y=200;
            platform1.position.x=500;
            platform1.position.y=100;
            tool.position.x=600;
            tool.position.y=55;
            platform2.position.x=700;
            platform2.position.y=130;
            enemy.position.x=600;
            enemy.position.y=70;
            platform3.position.x=300;
            platform3.position.y=150;
            drawd('red');
        }

        //判断水滴是否掉下，即死亡
        //或碰到敌人
        function isDied(){
            if(player.position.y+player.height==250){
                GameStart();
            }
            //碰到敌人
            if((enemy.position.x<=player.position.x+player.width&&enemy.position.x>=player.position.x)||
            (enemy.position.x+enemy.width>=player.position.x&&enemy.position.x+enemy.width<=player.position.x+player.width)) {
                if(enemy.position.y<=player.position.y+player.height&&enemy.position.y>=player.position.y){
                    GameStart();
                }
            }

        }

        function animate(){
            requestAnimationFrame(animate);
            ctx.clearRect(0,0,800,250);
            var img=new Image();
            img.src='common/images/bg.png';
            ctx.drawImage(img,0,0,800,250);
            update();
            //道具和敌人有先后顺序的
            getTool();
            EnemyRun();
            drawx1(platform);
            drawx1(platform1);
            drawx1(platform3);
            //使物块在悬浮物上站立
            levitation(platform);
            levitation(platform1);
            levitation(platform2);
            levitation(platform3);
            //实现第三块悬浮物的出场
            if(platform.position.x<=100&&platform1.position.x+platform1.width<=600){
                drawx1(platform2);
            }
            //一开始的platform变值则（-50）也要变，同理platform1也要
            if(platform.position.x==-50&&platform1.position.x+platform1.width==600){
                platform2.position.x=700;
            }

            isDied();
             var countDown = setInterval(() => {
            this.timeInt--;

        }, 1000)

        }
        //获得道具
        function getTool(){
            if((tool.position.x<=player.position.x+player.width&&tool.position.x>=player.position.x)||
            (tool.position.x+tool.width>=player.position.x&&tool.position.x+tool.width<=player.position.x+player.width)){
                if(tool.position.y<=player.position.y+player.height&&tool.position.y>=player.position.y){
                    touchtool=true;
                }
            }
            //如果获得道具则清除道具
            if(touchtool){
                tool.position.x=-10;//移到画布外
                tool.position.y=-40;
                touchtool=false;//可以在重新开始时道具出现

            }else{
                drawd('common/images/tool.jpg');
            }
        }
        animate();

    },


    //向左移动
    left(){
        platform1.position.x+=20;
        platform.position.x+=20;
        tool.position.x+=20;
        enemy.position.x+=20;
        platform3.position.x+=20;
        //判断悬浮物什么时候出现
        if(platform.position.x<=100&&platform1.position.x+platform1.width<=600){
            platform2.position.x+=20;
        }
    },

    //向右移动
    right(){
        //让物品不超出画布范围
        platform1.position.x-=20;
        platform.position.x-=20;
        tool.position.x-=20;
        enemy.position.x-=20;
        platform3.position.x-=20;
        //判断悬浮物什么时候出现
        if(platform.position.x<=100&&platform1.position.x+platform1.width<=600){
            platform2.position.x-=20;
        }
    },

    //向上跳跃
    jump(){
        player.velocity.y=-7;
    },


}
var touchtool=false;//道具是否获取
const gravity=0.5;
class Player{
    constructor(){
        this.position={
            x:70,
            y:100,
        };
        this.velocity={
            x:0,
            y:0,
        };
        this.width=50;
        this.height=50;
    }
}

const player=new Player();

class Platform{
    constructor(xx,yy,width,height){
        this.position={
            x:xx,
            y:yy
        }
        this.width=width;
        this.height=height
    }
}
const platform=new Platform(50,200,200,25);

const platform1=new Platform(500,100,200,25);

class Tool{
    constructor(xx,yy,wi,he){
        this.position={
            x:xx,
            y:yy
        }
        this.width=wi;
        this.height=he
    }
}

class Enemy{
    constructor(xx,yy,wi,he){
        this.position={
            x:xx,
            y:yy
        };
        this.velocity={
            x:2,
            y:0
        }
        this.width=wi;
        this.height=he
    }

}
const tool=new Tool(600,55,40,40);
const platform2=new Platform(700,130,200,25);
const platform3=new Platform(300,150,100,25);
const enemy=new Enemy(600,70,40,30);
