import http from '@ohos.net.http';
export default{
    predict(){
        let httpRequest = http.createHttp();

        httpRequest.request(
            // 填写http请求的url地址，可以带参数也可以不带参数。URL地址需要开发者自定义。请求的参数可以在extraData中指定
            "http://127.0.0.1:5000/predict",
            {
                method: http.RequestMethod.POST, // 数据请求方式POST
                // 此字段用于通过POST传递内容
                extraData: {
                    "data": "",//由于目前未能完成摄像头的适配以及相机的调用，遂未能给出具体的代码
                },
                connectTimeout: 30000,
                readTimeout: 30000,
            },
        );
    }
}