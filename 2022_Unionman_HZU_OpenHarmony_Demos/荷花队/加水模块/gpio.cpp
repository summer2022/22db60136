#include <stdio.h>
#include "gpio_if.h"
#include "hdf_log.h"
#include "osal_irq.h"
#include "osal_time.h"
#include "AdcFunction.h" 

#ifdef HDF_LOG_ON
#define LOGD(format, ...)                                                    \
    do {                                                                             \
            HDF_LOGD("[%{public}s:%{public}d] " format "\n", \
                __FUNCTION__, __LINE__,                               \
                ##__VA_ARGS__);                                                      \
    } while (0)

#define LOGI(format, ...)                                                                    \
    do {                                                                                     \
            HDF_LOGI("[%{public}s:%{public}d] " format "\n", __FUNCTION__, __LINE__, \
                ##__VA_ARGS__);                                                                  \
    } while (0)

#define LOGW(format, ...)                                                                    \
    do {                                                                                     \
        HDF_LOGW("[%{public}s:%{public}d] " format "\n", __FUNCTION__, __LINE__, \
            ##__VA_ARGS__);                                                                  \
    } while (0)

#define LOGE(format, ...)                                   \
    do {                                                            \
        HDF_LOGE(                                       \
            "\033[0;32;31m"                                         \
            "[%{public}s:%{public}d] " format "\033[m"   \
            "\n",                                                   \
            __FUNCTION__,  __LINE__, ##__VA_ARGS__);  \
    } while (0)
#else
#define LOGD(format, ...)                                   \
    do {                                                    \
            printf("[%s:%d] " format "\n",  \
                __FUNCTION__, __LINE__,                     \
                ##__VA_ARGS__);                             \
    } while (0)

#define LOGI(format, ...)                                                      \
    do {                                                                       \
        printf("[%s:%d] " format "\n", __FUNCTION__, __LINE__, \
            ##__VA_ARGS__);                                                    \
    } while (0)

#define LOGW(format, ...)                                                      \
    do {                                                                       \
        printf("[%s:%d] " format "\n", __FUNCTION__, __LINE__, \
            ##__VA_ARGS__);                                                    \
    } while (0)

#define LOGE(format, ...)                                   \
    do {                                                    \
        printf("\033[0;32;31m"                              \
            "[%s:%d] " format "\033[m"      \
            "\n",                                           \
            __FUNCTION__,  __LINE__, ##__VA_ARGS__);  \
    } while (0)
#endif

int32_t main(int argc, char *argv[])
{
    uint16_t gpio = 380; /* GPIO_01 */
    uint32_t count = 0;
    uint16_t valRead;
    int32_t ret;

    LOGD("GpioSetDir GPIO_DIR_OUT\n");

    ret = GpioSetDir(gpio, GPIO_DIR_OUT);
    if (ret != HDF_SUCCESS) {
        LOGE("%s: set dir fail! ret:%d\n", __func__, ret);
        return ret;
    }
    
    for(;;){
    double data= Adc_read(1);
    if(count<=5) {
        (void)GpioRead(gpio, &valRead);
        LOGD("GpioRead valRead : %d\n", valRead);
        (void)GpioWrite(gpio,GPIO_VAL_HIGH);
        printf("\n******************\n");
        printf("\n waterdepth = %.2f",data);
        printf("\n******************\n");
    }
    
    else {
    	(void)GpioWrite(gpio,GPIO_VAL_LOW);
    	(void)GpioRead(gpio, &valRead);
        LOGD("GpioRead valRead : %d\n", valRead);
    	printf("\n******************\n");
    	printf("\n waterdapth = %.2f",data);
    	printf("\n******************\n");
    	LOGE("%s: wait timeout break!\n", __func__);
    	break;
	}
	count++;
        OsalMSleep(1000);
    }

    return HDF_SUCCESS;
}
