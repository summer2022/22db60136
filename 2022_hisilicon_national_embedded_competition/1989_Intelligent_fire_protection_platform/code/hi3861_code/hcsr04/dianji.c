#include "dianji.h"     // 头文件
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "ohos_init.h"
#include "cmsis_os2.h"
#include "iot_errno.h"
#include "iot_gpio.h"
#include "iot_gpio_ex.h"
#include "iot_adc.h"
#include "iot_uart.h"
#include "hi_io.h"
#include "hi_uart.h"

static const char data1[17] = {0xFE, 0xEF, 0x0D, 0x01, 0x3E, 0xFA, 0xE1, 0x47, 0x37,0x27, 0xC5, 0xAC,  0x37,0x27, 0xC5, 0xAC, 0xF9};  // x(线速度)=0.5m/s y(线速度)=0m/s z(角速度)=0m/s
static const char data2[17] = {0xFE, 0xEF, 0x0D, 0x01, 0xBE, 0xFA, 0xE1, 0x47, 0x37,0x27, 0xC5, 0xAC,  0x37,0x27, 0xC5, 0xAC, 0x79};  // x(线速度)=-0.5m/s y(线速度)=0m/s z(角速度)=0m/s
static const char data3[17] = {0xFE, 0xEF, 0x0D, 0x01, 0x37,0x27, 0xC5, 0xAC,  0x37, 0x27, 0xC5, 0xAC, 0x3F, 0x33, 0x33, 0x33,  0x71};  // x(线速度)=0m/s y(线速度)=0m/s z(角速度)=0.7m/s
static const char data4[17] = {0xFE, 0xEF, 0x0D, 0x01, 0x37,0x27, 0xC5, 0xAC,  0x37, 0x27, 0xC5, 0xAC, 0xBF, 0x33, 0x33, 0x33, 0xF1};  // x(线速度)=0m/s y(线速度)=0m/s z(角速度)=-0.7m/s
static const char data5[17] = {0xFE, 0xEF, 0x0D, 0x01, 0x3E, 0x4C, 0xCC, 0xCC, 0x37,0x27, 0xC5, 0xAC, 0x37, 0x27, 0xC5, 0xAC, 0xBB}; // x(线速度)=0.2m/s y(线速度)=0m/s z(角速度)=0m/s
//static const char data[17] = {0xFE, 0xEF, 0x0D, 0x01, 0x37,0x27, 0xC5, 0xAC,  0x37, 0x27, 0xC5, 0xAC, 0x3E, 0x4C, 0xCC, 0xCC, 0xBB};  // x(线速度)=0m/s y(线速度)=0m/s z(角速度)=0.2m/s

extern hi_s32 distance;
void UART(int sudu)
{
    //使用GPIO0与GPIO1作为UART串口
    IoSetFunc(HI_IO_NAME_GPIO_11, IOT_IO_FUNC_GPIO_0_UART1_TXD);
    IoSetFunc(HI_IO_NAME_GPIO_12, IOT_IO_FUNC_GPIO_1_UART1_RXD);

    //UART1初始化配置 按照电机驱动所要求的进行控制
    hi_uart_attribute uart_attr = {
        .baud_rate = 230400,
        .data_bits = 8,
        .stop_bits = 1,
        .parity = 0,
    };

    //Hi3861 UART 初始化，通道选择，将结构体配置信息配置好。
    hi_uart_init(HI_UART_IDX_2, &uart_attr, NULL);

    switch (sudu){
        case 1:
            while (1)
            {
                hi_uart_write(HI_UART_IDX_2, (unsigned char *)data1, strlen(data1));
                usleep(500000);//保证在心跳周期内发送数据包，否则底盘运动会停止
            }
            break;
        case 2:
            while (1)
            {
                hi_uart_write(HI_UART_IDX_2, (unsigned char *)data2, strlen(data2));
                usleep(500000);
            }
            break;
        case 3:
            while (1)
            {
                hi_uart_write(HI_UART_IDX_2, (unsigned char *)data3, strlen(data3));
                usleep(500000);
                if (distance > 200)
                {
                    break;
                }
            }
            break;
        case 4:
            while (1)
            {
                hi_uart_write(HI_UART_IDX_2, (unsigned char *)data4, strlen(data4));
                usleep(500000);
                // if (distance > 600)
                // {
                //     return;
                // }
            }
            break;
        case 5:
            while (1)
            {
                hi_uart_write(HI_UART_IDX_2, (unsigned char *)data5, strlen(data5));
                usleep(500000);
                if (distance < 200)
                {
                    break;
                }
            }
            break;
        default:printf("error\n");
    }

    // while (1)
    // {
    //     hi_uart_write(HI_UART_IDX_2, (unsigned char *)data1, strlen(data1));
    //     usleep(500000);//保证在心跳周期内发送数据包，否则底盘运动会停止
    // }
}


// static void UART_ExampleEntry(void)
// {
//     //底盘控制任务    优先级25
//     osThreadAttr_t attr;

//     attr.name = "UART_Task";
//     attr.attr_bits = 0U;
//     attr.cb_mem = NULL;
//     attr.cb_size = 0U;
//     attr.stack_mem = NULL;
//     attr.stack_size = 4096;
//     attr.priority = 25;

//     if (osThreadNew((osThreadFunc_t)UART_Task, NULL, &attr) == NULL)
//     {
//         printf("[ADCExample] Falied to create UART_Task!\n");
//     }
// }

// SYS_RUN(UART_ExampleEntry);
