

#ifndef __zitaijiesuan_H__
#define __zitaijiesuan_H__

#include <math.h>


typedef struct //用于存储三轴的加速度和角速度
{
    double x;
    double y;
    double z;
}Vector3f;

typedef struct   //存储最终结算后的三个角度
{
    float pitch;
    float roll;
    float yaw;
}Attitude;

#endif