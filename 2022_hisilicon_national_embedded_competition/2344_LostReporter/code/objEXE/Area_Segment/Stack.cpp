/********************************************************************
	created:	2010/11/15
	created:	15:11:2010   16:13
	filename: 	Stack.C
	author:		Hu Jun
	
	purpose:	Stack Operator
*********************************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "Stack.h"




/*-----------------------------栈基本操作的函数定义------------------------------*/

//构造一个空栈
STATUS CreateStack( PSTACK pStack )
{
	if ( ( pStack->pArray = (ELEMENT_TYPE*)calloc(STACK_INITIAL_SIZE, sizeof(ELEMENT_TYPE)) ) == NULL )
		exit(OVERFLOW);

	pStack->nTopElement = -1;
	pStack->nStackSize = STACK_INITIAL_SIZE;

	return OK;
}

//若栈不为空，则用e返回栈的栈顶元素，并返回OK
//否则，返回ERROR
STATUS GetTopElement( STACK Stack, ELEMENT_TYPE* e )
{
	if ( Stack.nTopElement == -1 )
		return ERROR;

	memcpy(e, Stack.pArray+Stack.nTopElement, sizeof(ELEMENT_TYPE));
	return OK;
}

//压栈
STATUS Push( PSTACK pStack, ELEMENT_TYPE e )
{
	if ( pStack->nTopElement+2 >= (int)pStack->nStackSize )
	{
		pStack->pArray = (ELEMENT_TYPE *)realloc( pStack->pArray, (pStack->nStackSize+STACK_INCREMENT)*sizeof(ELEMENT_TYPE) );
		if (pStack->pArray==NULL)
			exit(OVERFLOW);

		pStack->nStackSize += STACK_INCREMENT;
	}

	pStack->pArray[++pStack->nTopElement] = e;
	return OK;
}

//出栈
//栈不空，出栈，返回OK；否则，返回ERROR
STATUS Pop( PSTACK pStack, ELEMENT_TYPE* e )
{
	if ( pStack->nTopElement == -1 )
		return ERROR;

	e = pStack->pArray + (pStack->nTopElement--);
	return OK;
}

//销毁堆栈
STATUS DestroyStack( PSTACK pStack )
{
	free( pStack->pArray ), pStack->pArray=NULL;
	pStack->nTopElement = -1 ;
	pStack->nStackSize = 0;
	return OK;
}

//清空堆栈
STATUS ClearStack( PSTACK pStack )
{
	if ( pStack->nTopElement > -1 )
	{
		memset(pStack->pArray, 0, pStack->nStackSize*sizeof(ELEMENT_TYPE));
		pStack->nTopElement = -1;
	}
	return OK;
}

//判断堆栈是否为空
//若空，返回TRUE；不空，返回FALSE
STATUS IsStackEmpty( STACK Stack )
{
	if( Stack.nTopElement == -1 )
		return TRUE ;
	else
		return FALSE ;
}