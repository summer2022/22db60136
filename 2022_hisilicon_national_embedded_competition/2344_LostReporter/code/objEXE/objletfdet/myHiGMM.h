#ifndef MYHIGMM_H_
#define MYHIGMM_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <semaphore.h>
#include <pthread.h>
#include <sys/prctl.h>

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/core/core.hpp"
#include "myDIP.h"
#include "sample_assist.h"
#include "hi_type.h"
#include "sample_file.h"
#include "sample_define.h"
#include "mpi_ive.h"

extern "C"{
  #include "sample_comm_ive.h"
}

using namespace cv;

enum
{
	CVGRAY_IVEU8C1 = 0,
	CVBGR_IVEU8C3PACKAGE = 1,

	IVEU8C1_CVGRAY = 3,
	IVEU8C3PACKAGE_CVBGR = 4
};

typedef struct hiSAMPLE_IVE_GMM_S
{
    IVE_SRC_IMAGE_S stSrc;
    IVE_DST_IMAGE_S stFg;
    IVE_DST_IMAGE_S stBg;
    IVE_MEM_INFO_S  stModel;
    IVE_IMAGE_S    stImg1;
    IVE_IMAGE_S    stImg2;
    IVE_DST_MEM_INFO_S stBlob;
    IVE_GMM_CTRL_S  stGmmCtrl;
    IVE_CCL_CTRL_S  stCclCtrl;
    IVE_FILTER_CTRL_S stFltCtrl;
    IVE_DILATE_CTRL_S stDilateCtrl;
    IVE_ERODE_CTRL_S stErodeCtrl;
    SAMPLE_RECT_ARRAY_S stRegion;
} SAMPLE_IVE_GMM_S;

typedef struct hiSAMPLE_IVE_GMM_INFO_S
{
    SAMPLE_IVE_GMM_S stGmm;
    HI_BOOL bEncode;
    HI_BOOL bVo;
} SAMPLE_IVE_GMM_INFO_S;

class MyHiGMM{
public:
	MyHiGMM();
	~MyHiGMM();
	//HI_U16 SAMPLE_COMM_IVE_CalcStride(HI_U32 u32Width, HI_U8 u8Align);
	//HI_S32 SAMPLE_COMM_IVE_CreateImage(IVE_IMAGE_S* pstImg, IVE_IMAGE_TYPE_E enType, HI_U32 u32Width, HI_U32 u32Height);
	HI_VOID SAMPLE_IVE_Gmm_Uninit(SAMPLE_IVE_GMM_S* pstGmm);
	HI_S32 SAMPLE_IVE_Gmm_Init(SAMPLE_IVE_GMM_S* pstGmm, HI_U32 u32Width, HI_U32 u32Height);
	void Set_Learning_Rate(int learn_rate);
	HI_S32 SAMPLE_IVE_GmmProc(SAMPLE_IVE_GMM_INFO_S* pstGmmInfo, VIDEO_FRAME_INFO_S* pstExtFrmInfo,HI_U32 u32BaseWidth,HI_U32 u32BaseHeight);
	void Set_Frame_Width_Height(int width, int height);
	void mat2IveImg(Mat *src, IVE_IMAGE_S *pstDst, int type);
	void iveImage2Mat(IVE_IMAGE_S *pstSrc, Mat *dst, int type);

public:
  HI_BOOL s_bStopSignal = HI_FALSE;
  //static pthread_t s_hIveThread = 0;
  SAMPLE_IVE_GMM_INFO_S s_stGmmInfo;
  IVE_SRC_IMAGE_S yuv420Img;
  //SAMPLE_IVE_SWITCH_S s_stGmmSwitch;
  //SAMPLE_VI_CONFIG_S s_stViConfig;// = {0};
  int frame_width;
  int frame_height;
  VIDEO_FRAME_INFO_S stframe;
};

#endif
