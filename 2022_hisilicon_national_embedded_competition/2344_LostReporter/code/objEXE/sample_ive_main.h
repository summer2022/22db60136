#ifndef __SAMPLE_IVE_MAIN_H__
#define __SAMPLE_IVE_MAIN_H__
#include "hi_type.h"

/******************************************************************************
* function : show Gmm sample
******************************************************************************/
HI_VOID SAMPLE_IVE_Gmm(HI_CHAR chEncode, HI_CHAR chVo);

#endif

