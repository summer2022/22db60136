# 基于视觉的自助停车场收费管理系统

## Hi3516

采用YoloV2目标检测算法和EasyPR车牌识别算法获取出入车场车辆信息，实现混合自助车场智能化便捷化收费管理

## Hi3861

通过串口互联获取识别到的车辆信息，并通过OLED屏显示，实现混合自助车场智能化便捷化收费管理

## 详细过程请参考同名技术文档