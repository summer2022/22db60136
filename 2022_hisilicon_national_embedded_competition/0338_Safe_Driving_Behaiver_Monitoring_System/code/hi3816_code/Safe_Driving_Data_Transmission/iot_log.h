
#ifndef IOT_LOG_H_
#define IOT_LOG_H_

typedef enum {
    // < this is used as the trace function,like the function enter and function out
    EN_IOT_LOG_LEVEL_TRACE = 0,
    // < this is used as the debug, you could add any debug as you wish
    EN_IOT_LOG_LEVEL_DEBUG,
    // < which means it is import message, and you should known
    EN_IOT_LOG_LEVEL_INFO,
    // < this is used as the executed result,which means the status is not what we expected
    EN_IOT_LOG_LEVEL_WARN,
    // < this is used as the executed result,which means the status is not what we expected
    EN_IOT_LOG_LEVEL_ERROR,
    // < this is used as the parameters input for the api interface, which could not accepted
    EN_IOT_LOG_LEVEL_FATAL,
    EN_IOT_LOG_LEVEL_MAX,
}EnIotLogLevel;

EnIotLogLevel IoTLogLevelGet(void);

const char *IoTLogLevelGetName(EnIotLogLevel logLevel);

int IoTLogLevelSet(EnIotLogLevel level);

#ifndef IOT_PRINT
#define IOT_PRINT(fmt, ...) \
    do \
    { \
        printf(fmt, ##__VA_ARGS__); \
    }while (0)
#endif

#ifdef CONFIG_LINKLOG_ENABLE

#define IOT_LOG(level, fmt, ...) \
    do \
    { \
        IOT_PRINT("[%s][%s] " fmt "\r\n", \
        IoTLogLevelGetName((level)), __FUNCTION__, ##__VA_ARGS__); \
    } while (0)

#define IOT_LOG_TRACE(fmt, ...) \
    do \
    { \
        if ((EN_IOT_LOG_LEVEL_TRACE) >= IoTLogLevelGet()) \
        { \
            IOT_LOG(EN_IOT_LOG_LEVEL_TRACE, fmt, ##__VA_ARGS__); \
        } \
    } while (0)

#define IOT_LOG_DEBUG(fmt, ...) \
    do \
    { \
        if ((EN_IOT_LOG_LEVEL_DEBUG) >= IoTLogLevelGet()) \
        { \
            IOT_LOG(EN_IOT_LOG_LEVEL_DEBUG, fmt, ##__VA_ARGS__); \
        } \
    } while (0)

#else
#define IOT_LOG(level, fmt, ...)
#define IOT_LOG_TRACE(fmt, ...)
#define IOT_LOG_DEBUG(fmt, ...)
#endif

#define IOT_LOG_TRACE(fmt, ...)   IOT_LOG(EN_IOT_LOG_LEVEL_TRACE, fmt, ##__VA_ARGS__)
#define IOT_LOG_INFO(fmt, ...)   IOT_LOG(EN_IOT_LOG_LEVEL_INFO, fmt, ##__VA_ARGS__)
#define IOT_LOG_WARN(fmt, ...)   IOT_LOG(EN_IOT_LOG_LEVEL_WARN, fmt, ##__VA_ARGS__)
#define IOT_LOG_ERROR(fmt, ...)  IOT_LOG(EN_IOT_LOG_LEVEL_ERROR, fmt, ##__VA_ARGS__)
#define IOT_LOG_FATAL(fmt, ...)  IOT_LOG(EN_IOT_LOG_LEVEL_FATAL, fmt, ##__VA_ARGS__)

#endif /* IOT_LOG_H_ */