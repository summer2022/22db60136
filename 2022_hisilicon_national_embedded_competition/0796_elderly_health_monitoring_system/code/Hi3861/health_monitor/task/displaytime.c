
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

#include "ohos_init.h"
#include "cmsis_os2.h"
#include "iot_i2c.h"
#include "iot_gpio.h"
#include "iot_errno.h"

#include "oled_ssd1306.h"

#include "task_start.h"
#include "max30102_func.h"
#include "orientation.h"

uint32_t  second=0,minute=10,hour=8,year=2022,month=6,day=10;
uint32_t  seconds[2],minutes[2],hours[2];
static void TimeTask(const char *arg)
{      
    printf("enter the timetask");
    (void)arg;
    while(1)
    {
        msleep(1000);
        second++;
        if(second==60)
        {
          second=0;
          minute++;
          if(minute==60)
            {
                minute=0;
                hour++;
                if(hour==24)
                    hour=0;
            }
        }
    }  
    printf("hour=%d,minute=%d,second=%d\n",hour,minute,second);
    sprintf(second,"%d",seconds);
    sprintf(minute,"%d",minutes);
    sprintf(hours,"%d",hours);
    printf("%s:%s:%s\n",hours,minutes,seconds);
}

static void TimeDemo(void)
{
    osThreadAttr_t attr;
    attr.name = "TimeTask";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = 4096; /* �����С4096 */
    attr.priority = osPriorityNormal;

    if (osThreadNew(TimeTask, NULL, &attr) == NULL) {
        printf("[TimeTask] Falied to create TimeTask!\n");
    }
}

APP_FEATURE_INIT(TimeDemo);

