
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

#include <ohos_init.h>
#include <cmsis_os2.h>
#include <iot_i2c.h>
#include <iot_gpio.h>
#include <iot_errno.h>

#include "max30102_func.h"


static void *Max30102Task(const char *arg)
{
    (void)arg;
    Max30102Init();
    Dete_signalRang();
    start_measure();
    printf("in the task\n");
   // OledShowString(20, 7, "Hello  World", 1); /* 屏幕第20列3行显示1行 */
    return NULL;
}

static void Max30102Demo(void)
{
    osThreadAttr_t attr;
    attr.name = "Max30102Task";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = 4096; /* 任务大小4096 */
    attr.priority = osPriorityNormal;

    if (osThreadNew((osThreadFunc_t)Max30102Task, NULL, &attr) == NULL) {
        printf("[Max30102Task] Falied to create Max30102Task!\n");
    }
}

SYS_RUN(Max30102Demo);