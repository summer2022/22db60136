#ifndef _ORENTATION_H_
#define _ORENTATION_H_
//定义数组长度

#include "sys.h"

#define GPS_Buffer_Length 80
#define UTCTime_Length 11
#define latitude_Length 11
#define N_S_Length 2
#define longitude_Length 12
#define E_W_Length 2 
#define UART_NUM   1//UART1 

void errorLog(int num);
void parseGpsBuffer(void);
void printGpsBuffer(void);
void clrStruct(void);
void GPS_Read(void);
void GPS_init(void);


hi_u16 FindStr(char *str,char *ptr);
typedef struct SaveData 
{
	char GPS_Buffer[GPS_Buffer_Length];
	char isGetData;		//是否获取到GPS数据
	char isParseData;	//是否解析完成
	char UTCTime[UTCTime_Length];		//UTC时间
	char latitude[latitude_Length];		//纬度
	char N_S[N_S_Length];		//N/S
	char longitude[longitude_Length];		//经度
	char E_W[E_W_Length];		//E/W
	char isUsefull;		//定位信息是否有效
} _SaveData;
extern _SaveData Save_Data;

#define u8 hi_u8 
#define BAUDRATE 9600
#define AHT20_I2C_IDX 0

#endif