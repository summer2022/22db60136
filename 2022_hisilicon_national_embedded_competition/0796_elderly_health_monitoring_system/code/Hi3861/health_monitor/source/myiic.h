#ifndef __myiic_H
#define __myiic_H
#include "sys.h"
  	   		   
//IIC��������
#define SCL_GPIO 11
#define SDA_GPIO 12

//IO��������
#define SCL_IN  IoTGpioSetDir(SCL_GPIO,0)
#define SCL_OUT IoTGpioSetDir(SCL_GPIO,1)
#define SDA_IN  IoTGpioSetDir(SDA_GPIO,0)
#define SDA_OUT IoTGpioSetDir(SDA_GPIO,1)

//IO��������	 
#define IIC_SCL_1    IoTGpioSetOutputVal(SCL_GPIO, 1) //SCL
#define IIC_SCL_0    IoTGpioSetOutputVal(SCL_GPIO, 0) //SCL
#define IIC_SDA_1    IoTGpioSetOutputVal(SDA_GPIO, 1) //SDA
#define IIC_SDA_0    IoTGpioSetOutputVal(SDA_GPIO, 0) //SDA	 
//#define READ_SDA      IoTGpioGetInputVal(SDA_GPIO,READ_SDA_ptr);




//IIC���в�������
void IIC_Init(void);                //��ʼ��IIC��IO��				 
void IIC_Start(void);				//����IIC��ʼ�ź�
void IIC_Stop(void);	  			//����IICֹͣ�ź�
void IIC_Send_Byte(u8 txd);			//IIC����һ���ֽ�
u8 IIC_Read_Byte(unsigned char ack);//IIC��ȡһ���ֽ�
u8 IIC_Wait_Ack(void); 				//IIC�ȴ�ACK�ź�
void IIC_Ack(void);					//IIC����ACK�ź�
void IIC_NAck(void);				//IIC������ACK�ź�

void IIC_Write_One_Byte(u8 daddr,u8 addr,u8 data);
void IIC_Read_One_Byte(u8 daddr,u8 addr,u8* data);

void IIC_WriteBytes(u8 WriteAddr,u8* data,u8 dataLength);
void IIC_ReadBytes(u8 deviceAddr, u8 writeAddr,u8* data,u8 dataLength);
#endif
















