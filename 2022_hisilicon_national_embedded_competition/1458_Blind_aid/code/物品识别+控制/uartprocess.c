#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/prctl.h>

#include "hi_mipi_tx.h"
#include "sdk.h"
#include "sample_comm.h"
#include "hisignalling.h"

int main()
{
    int uartFd;
    uartFd = UartOpenInit();
    if (uartFd < 0) {
        printf("uart1 open failed\r\n");
    } else {
        printf("uart1 open successed\r\n");
    }
    unsigned char *buf;
    int readLen=0;
    unsigned int RecvLen = 10;
    int b;
    int i;

    while(1){  
        for(i=0;i<10;i++)
        {
            printf("begin read\n");
            readLen = UartRead(uartFd, buf, RecvLen, 10000);//1000 :time out 
        b = buf[2];
        printf("the receive signal is %d\n",b);
        }
        
    }
    return HI_NULL;
}