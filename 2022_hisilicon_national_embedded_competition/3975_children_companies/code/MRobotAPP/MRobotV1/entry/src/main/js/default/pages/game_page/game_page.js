import router from '@system.router';
import prompt from '@system.prompt';
export default {
    data: {
        title: "寓教于乐",
        isHarmonyOS:true,
        work_status :'寓教于乐',
        game_list: [
            {
                title: 'Hello World',
                subtitle: '认识宇宙',
                img:'/common/images/english.png',
            },
            {
                title: '随机算术',
                subtitle: 'e^(iπ)+1=0 \n 随机计算题',
                img:'/common/images/mathmatic.png',
            },
            {
                title: '唐诗宋词',
                subtitle: '床前明月光',
                img:'/common/images/poetry.png',
            },
             {
                title: '更多探索',
                subtitle: '还有啥',
                img:'/common/images/more.png',
            },
        ],

    },

    ToHomePage(){
        this.work_status ='退出';
        router.replace({
            uri: 'pages/jscontrol/jscontrol',
        });
    },
    ToEnglishPage(){
        this.work_status ='退出';
        router.replace({
            uri: 'pages/english/english',
        });
    },
    ToMathmaticPage(){
        this.work_status ='退出';
        router.replace({
            uri: 'pages/mathmatic/mathmatic',
        });
    },
    ToPoetyPage(){
        this.work_status ='退出';
        router.replace({
            uri: 'pages/poetry/poetry',
        });
    },
    ToLabyrinthPage(){
        this.work_status ='退出';
        router.replace({
            uri: 'pages/labyrinth/labyrinth',
        });
    },
    MoreGame()
    {
        this.game_list[4].subtitle=(this.game_list[4].subtitle==='还有啥')?"敬请期待\n等你加入":"还有啥";
        this.MRobot_more();
    },

    MRobot_more(){
        this.$element('dialog_mrobot').show();
        prompt.showToast({
            message: '感谢使用'
        })
    },


}