
//页面路由
import router from '@system.router';
//弹窗
import prompt from '@system.prompt';
// 振动器
import vibrator from '@system.vibrator';
import app from '@system.app';


export default {

    data: {
        title: "随机计算题",
        number1:1,
        number2:1,
        formula:"1 + 1 = ?",
        calculate_result:2,
        input_answer:0,
        user_log:'等待计算中',
        isRight:0,
        operation_list:[
            {
                operation:'+',
            },
            {
            operation:'+',
            },
            {
            operation:'-',
            },
            {
            operation:'*',
            },
            {
            operation:'/',
            },
            {
            operation:'/',
            },
        ],
        img_list:[{src:'common/images/heibot2.png'},{src:'common/images/heibot.png'},],
    },

    ToGamePage(){
        this.work_status ="寓教于乐界面"
        router.replace({
            uri: 'pages/game_page/game_page'
        });
    },
    ToHomePage(){
        this.work_status ='退出';
        router.replace({
            uri: 'pages/jscontrol/jscontrol',
        });
    },

    Calculate_random_operation()
    {
        this.number1=(Math.random()*101)+'';
        this.number2=(Math.random()*101)+''; //随机数[0-101)之间
        var random_operation=Math.random()*5+''; //0-5 可能不会有5

        //截取小数点前的整数【0-10之间】
        this.number1= this.number1.substring(0,this.number1.indexOf('.'));
        this.number2= this.number2.substring(0,this.number2.indexOf('.'));
        random_operation= random_operation.substring(0,random_operation.indexOf('.'));
        var operation = this.operation_list[random_operation].operation;

        console.log("number1 "+this.number1);
        console.log("number2 "+this.number2);
        console.log("random_operation "+random_operation);
        console.log("operation="+operation);

        switch(operation.toString())
        {
            case '+': this.calculate_result=Number(this.number1)+Number(this.number2);break; //转回数字
            case '-': this.calculate_result=this.number1-this.number2;break;
            case '*': this.calculate_result=this.number1*this.number2;break;
            case "/": { //排除掉分母为0
                if(this.number2==0)
                {
                    this.number2=1;
                }
                this.calculate_result=this.number1/this.number2;
                if(this.calculate_result%1!=0) //剔除无法整除的数
                {
                    console.log("数据无法整除：this.calculate_result="+this.calculate_result);
                    this.number2=1;
                    this.calculate_result=this.number1/this.number2;
                    break;
                }
                break;
            }
        }
        //显示随机生成的数
        this.formula=this.number1.toString()+operation.toString()+this.number2.toString()+' = ?';
    },

    //获取input值
    change(e){
        this.input_answer = e.value;
        //console.log("input_answer=",this.input_answer);
        prompt.showToast({
            message: "当前输入: " + this.input_answer,
            duration: 3000,
        });
    },
    //支持键盘输入entry
    enterkeyclick()
    {
        this.ConfirmAnswer();
    },
    ConfirmAnswer()
    {
        // 显示input1
        console.log("input_answer=",this.input_answer);
        //测试加法
        // this.input_answer= Number(this.number1)+Number(this.number2);
        //测试减法
        // this.input_answer= Number(this.number1)-Number(this.number2);
        //测试乘法
        // this.input_answer= Number(this.number1)*Number(this.number2);
        //测试除法
        // this.input_answer= Number(this.number1)/Number(this.number2);
        if(this.calculate_result==this.input_answer)
        {
            this.user_log = "恭喜你，回答正确！";
            //弹窗显示
            this.showPrompt(this.user_log);
            this.isRight = 0;

            //刷新题目
            this.Calculate_random_operation();
        }
        else{
            this.user_log = " 再想想看！";
            this.isRight = 1;
            this.showPrompt(this.user_log);
            //输入框提示
            this.$element("input1").showError({error: '回答错误'});
            this.$element("input1").value=' ';

            //震动
            vibrator.vibrate({
                mode: 'short',
                success: function() {
                     console.log('vibrate is successful');
                },
                fail: function() {
                    console.log('vibrate is failed');
                },
                complete: function() {
                     console.log('vibrate is completed');
                }
            });
        }

        //this.getInfo();
        //this.terminate();
    },
    //弹框
    showPrompt(msg) {
        prompt.showToast({
            message: msg,
            duration: 3000,
        });
    },
    //获取app信息
    getInfo(){
        var info = app.getInfo();
        console.log(JSON.stringify(info));
    },
    //退出当前Ability
    terminate(){
        app.terminate();
    },
    //进入界面自动弹出
    setTimeOut(){
        var timeoutID = setTimeout(function() {
            console.log('delay 1s');
            //this.terminate();
        }, 1000);
        //clearTimeout(timeoutID);
    },
};

