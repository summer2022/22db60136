/**
 * @file bluetooth.h
 * @author hellokun (www.hellokun.cn)
 * @brief 蓝牙模块驱动程序
 * @version 0.1
 * @date 2022-03-16
 *
 * @copyright Copyright (c) 2022
 *
 */

#ifndef __BLUETOOTH_H__
#define __BLUETOOTH_H__

#define BLUE_RX_1 1
#define BLUE_TX_0 0

int Bluetooth_Init();
int Bluetooth_read(char *get_data, int data_len);
int Bluetooth_write(char *get_data, int data_len);

#endif