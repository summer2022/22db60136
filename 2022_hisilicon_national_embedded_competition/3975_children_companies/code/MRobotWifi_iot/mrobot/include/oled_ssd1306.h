
#ifndef OLED_SSD1306_H
#define OLED_SSD1306_H

#include <stdint.h>

#define OLED_I2C_IDX 0  //==I2c0  Hi3861两个i2c
#define I2C1_SDA_IO13 13
#define I2C1_SCL_IO14 14 //==总线分别对应GPIO13、14

#define OLED_WIDTH    (128)
#define OLED_I2C_ADDR 0x78 // 默认地址为 0x78
#define OLED_I2C_CMD 0x00 // 0000 0000       写命令
#define OLED_I2C_DATA 0x40 // 0100 0000(0x40) 写数据
#define OLED_I2C_BAUDRATE (400*1000) // 400k

/**
 * @brief ssd1306 OLED Initialize.
 */
uint32_t OledInit(void);

/**
 * @brief Set cursor position
 *
 * @param x the horizontal posistion of cursor
 * @param y the vertical position of cursor 
 * @return Returns {@link WIFI_IOT_SUCCESS} if the operation is successful;
 * returns an error code defined in {@link wifiiot_errno.h} otherwise.
 */
void OledSetPosition(uint8_t x, uint8_t y);

void OledFillScreen(uint8_t fillData);

enum Font {
    FONT6x8 = 1,
    FONT8x16
};
typedef enum Font Font;

void OledShowChar(uint8_t x, uint8_t y, uint8_t ch, Font font);
void OledShowString(uint8_t x, uint8_t y, const char* str, Font font);
//显示中文
void OledShowChinese(uint8_t x, uint8_t y,uint8_t num);
//显示表情
void OledShowSmile(uint8_t x, uint8_t y, uint8_t num);

#endif // OLED_SSD1306_H
