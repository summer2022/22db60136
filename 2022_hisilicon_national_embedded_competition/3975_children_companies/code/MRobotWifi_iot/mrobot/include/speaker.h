/**
 * @file speaker.h
 * @author hellokun (www.hellokun.cn)
 * @brief 语音模块驱动程序
 * @version 0.1
 * @date 2022-04-19
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef __SPEAKER_H__
#define __SPEAKER_H__

#define SPEAKER_RX_12 12
#define SPEAKER_TX_11 11
#define SUT03_LEN 5  // get、send Commnd length eg: AA 55 01 55 AA
#define TEMP_LEN 7 // send temperature eg:AA 55 06 20 06 55 AA 


int Speaker_Init();

/**
 * @brief 
 * 
 * @param get_data //eg: AA 55 01 55 AA
                   //Format head + index + data_H data_L + tail
                   //index 01 02 03 04 05 : Turn_left right forward backward circle
                           06 : get temprature cmd
                           07 : watering 
                           08 : shutdown PC
                           
 * @param data_len 
 * @return int 
 */
int Speaker_read(char *get_data, int data_len);

/**
 * @brief 
 * 
 * @param get_data //eg: AA 55 06 20 06 55 AA
                   //Format head + index + data_H data_L + tail
                   //index 01 :  sound effect_1 咕噜咕噜
                           06 :  temprature 
 * @param data_len  
 * @return int >=0 success else false
 */
int Speaker_write(char *get_data, int data_len);

#endif