#include <stdio.h>
#include <unistd.h>
#include "ohos_init.h"
#include "cmsis_os2.h"

#include "iot_gpio.h"
#include "iot_pwm.h"
#include "hi_io.h" //==hi_io_set_func()、hi_io_set_pull()

#include "motor.h"

void Motor_Init(void)
{
    IoTGpioInit(IN1);
    IoTGpioInit(IN2);
    IoTGpioInit(IN3);
    IoTGpioInit(IN4);

    hi_io_set_func(IN1, HI_IO_FUNC_GPIO_5_GPIO);
    hi_io_set_func(IN2, HI_IO_FUNC_GPIO_6_GPIO);
    hi_io_set_func(IN3, HI_IO_FUNC_GPIO_7_GPIO);
    hi_io_set_func(IN4, HI_IO_FUNC_GPIO_8_GPIO);
    // hi_io_set_pull(HI_IO_NAME_GPIO_5, HI_IO_PULL_DOWN);
    // hi_io_set_pull(HI_IO_NAME_GPIO_6, HI_IO_PULL_DOWN);
    // hi_io_set_pull(HI_IO_NAME_GPIO_7, HI_IO_PULL_DOWN);
    // hi_io_set_pull(HI_IO_NAME_GPIO_8, HI_IO_PULL_DOWN);

    IoTGpioSetDir(IN1, IOT_GPIO_DIR_OUT);
    IoTGpioSetDir(IN2, IOT_GPIO_DIR_OUT);
    IoTGpioSetDir(IN3, IOT_GPIO_DIR_OUT);
    IoTGpioSetDir(IN4, IOT_GPIO_DIR_OUT);

    // motor init stop
    IoTGpioSetOutputVal(IN1, IOT_GPIO_VALUE0);
    IoTGpioSetOutputVal(IN2, IOT_GPIO_VALUE0);
    IoTGpioSetOutputVal(IN3, IOT_GPIO_VALUE0);
    IoTGpioSetOutputVal(IN4, IOT_GPIO_VALUE0);
}
void Turn_left(unsigned int circle_time)
{
    IoTGpioSetOutputVal(IN1, IOT_GPIO_VALUE0);
    IoTGpioSetOutputVal(IN2, IOT_GPIO_VALUE1);
    IoTGpioSetOutputVal(IN3, IOT_GPIO_VALUE0);
    IoTGpioSetOutputVal(IN4, IOT_GPIO_VALUE1);
    usleep(circle_time);
}

void Turn_right(unsigned int circle_time)
{
    IoTGpioSetOutputVal(IN1, IOT_GPIO_VALUE1);
    IoTGpioSetOutputVal(IN2, IOT_GPIO_VALUE0);
    IoTGpioSetOutputVal(IN3, IOT_GPIO_VALUE1);
    IoTGpioSetOutputVal(IN4, IOT_GPIO_VALUE0);
    usleep(circle_time);
}

void  Forward(unsigned int circle_time)
{
    IoTGpioSetOutputVal(IN1, IOT_GPIO_VALUE0);
    IoTGpioSetOutputVal(IN2, IOT_GPIO_VALUE1);
    IoTGpioSetOutputVal(IN3, IOT_GPIO_VALUE1);
    IoTGpioSetOutputVal(IN4, IOT_GPIO_VALUE0);
    usleep(circle_time);
}

void Backward(unsigned int circle_time)
{
   
    IoTGpioSetOutputVal(IN1, IOT_GPIO_VALUE1);
    IoTGpioSetOutputVal(IN2, IOT_GPIO_VALUE0);
    IoTGpioSetOutputVal(IN3, IOT_GPIO_VALUE0);
    IoTGpioSetOutputVal(IN4, IOT_GPIO_VALUE1);
    usleep(circle_time);
}

void Circle(unsigned int circle_time)
{
   IoTGpioSetOutputVal(IN1, IOT_GPIO_VALUE1);
    IoTGpioSetOutputVal(IN2, IOT_GPIO_VALUE0);
    IoTGpioSetOutputVal(IN3, IOT_GPIO_VALUE1);
    IoTGpioSetOutputVal(IN4, IOT_GPIO_VALUE0);
    usleep(circle_time);
    usleep(circle_time);
}

void Stop_motor()
{
    // motor init stop
    IoTGpioSetOutputVal(IN1, IOT_GPIO_VALUE0);
    IoTGpioSetOutputVal(IN2, IOT_GPIO_VALUE0);
    IoTGpioSetOutputVal(IN3, IOT_GPIO_VALUE0);
    IoTGpioSetOutputVal(IN4, IOT_GPIO_VALUE0);
}