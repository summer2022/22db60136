#include "hi_io.h" //==hi_io_set_func()、hi_io_set_pull()
#include "hi_gpio.h"
#include "hi_adc.h"
#include "iot_gpio.h"
#include "touch_sensor.h"

void TouchSensorInit(void)
{
    IoTGpioInit(TOUCH_ADC9);
    hi_io_set_func(TOUCH_ADC9, HI_IO_FUNC_GPIO_9_GPIO);
    hi_gpio_set_dir(TOUCH_ADC9, HI_GPIO_DIR_IN);
    hi_io_set_pull(HI_IO_NAME_GPIO_9, HI_IO_PULL_DOWN);
    
}

//读取touch sensor电压值
float Get_touch_val(void)
{
    unsigned short int data;
    if (hi_adc_read(HI_ADC_CHANNEL_4, &data, HI_ADC_EQU_MODEL_1, HI_ADC_CUR_BAIS_DEFAULT, 0) == 0)
    {
        printf("ADC4 %d \n", data);
        //  (float)data * 1.8 * 4 / 4096; /* adc code equals: voltage/4/1.8*4096  */
        //  printf("ADC2 float %.3f \n", data);
        return data;
    }
    else
        return -1;
}