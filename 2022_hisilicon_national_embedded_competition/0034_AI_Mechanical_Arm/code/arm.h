#ifndef _ARM_H
#define _ARM_H

#include <hi_stdlib.h>
#include <hisignalling_protocol.h>
#include <hi_uart.h>
#include <iot_uart.h>
#include <hi_gpio.h>
#include <hi_io.h>
#include <math.h>

#define TOPLINE   15.5   //cm
#define MIDLINE   9.6    //cm  
#define UNDERLINE 11.4    //cm
#define BASEHIGH  7.5    //cm
#define PI        3.1415926


void SendRadToMoto(int id,int rad);
int PointToAngle(float x_point,float y_poing,float z_point,int cmd);
int JudgeTriangle(float p);
int AngleToMotoRad(void);
void ArmToCatchPoint(void);
void ArmReset(void);
double sq(double x);



#endif /*  _ARM_H */