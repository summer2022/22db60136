/*
 * Copyright (C) 2022 HiHope Open Source Organization .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 *
 * limitations under the License.
 */

#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include "ohos_init.h"
#include "cmsis_os2.h"
#include "iot_gpio.h"
#include "hi_io.h"
#include "hi_time.h"
#include "led_example.h"
#define GPIO0 0
#define COUNT 10
#define TASK_STAK_SIZE    (1024*10)

void set_angle(unsigned int duty)
{
    unsigned int time = 20000;
    
    IoTGpioSetDir(GPIO0, IOT_GPIO_DIR_OUT);
    IoTGpioSetOutputVal(GPIO0, IOT_GPIO_VALUE1);
    hi_udelay(duty);
    IoTGpioSetOutputVal(GPIO0, IOT_GPIO_VALUE0);
    hi_udelay(time - duty);
}

/* Turn 45 degrees to the left of the steering gear
1、依据角度与脉冲的关系，设置高电平时间为1000微秒
2、不断地发送信号，控制舵机向左旋转45度
*/
void engine_turn_left_45(void)
{
    unsigned int angle = 1000;
    for (int i = 0; i < COUNT; i++) {
        set_angle(angle);
    }
}

void engine_turn_right_45(void)
{
    unsigned int angle = 2000;
    for (int i = 0; i < COUNT; i++) {
        set_angle(angle);
    }
}


void regress_middle(void)
{
    unsigned int angle = 1500;
    for (int i = 0; i < COUNT; i++) {
        set_angle(angle);
    }
}


/* 任务实现 */
void RobotTask1(void* parame)
{
    (void)parame;
    
    printf("start test sg90\r\n");
    IoTGpioInit(GPIO0);
    printf("Turn 45 degrees to the left of the steering gear\r\n");
 
   while(1)
   {    
       if(yr==1){
       
        printf("start");
        engine_turn_left_45();
        printf("turn left!   \r\n");
        
         hi_udelay(2500000);
        engine_turn_right_45();
        printf("turn right!   \r\n");
        hi_udelay(2500000);
      
        printf("The steering gear is centered");
        IoTGpioInit(GPIO0);
        yr=0;
   }
   else{
       printf("no no no \n");

   }

    }
    
}


static void RobotDemo(void)
{
    osThreadAttr_t attr;

    attr.name = "RobotTask1";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = TASK_STAK_SIZE;
    attr.priority = osPriorityNormal;

    if (osThreadNew(RobotTask1, NULL, &attr) == NULL) {
        printf("[RobotDemo] Falied to create RobotTask!\n");
    }
}
APP_FEATURE_INIT(RobotDemo);
