
# 查看.pth模型文件的结构
import torch
from models.ResNet_LSTM import Res_LSTM


# weight_path = r"D:\Works\Hisi_codes\SLR-master\reslstm_models\slr_convlstm_epoch001.pth"
# weight_path = r'D:\Works\Hisi_codes\pytorch_to_caffe_master\\res50lstm_epoch026.pth' # !!!!记得修改权重路径
weight_path = r"D:\\Works\\Hisi_codes\\SLR-master\\res50lstm_models\\res50lstm_epoch026.pth" # Res50+LSTM 256x256
# weight_path = r"D:\\Works\\Hisi_codes\\SLR-master\\res50lstm_models\\res50lstm_epoch026-lstm.pth"


print("Load params from : ", weight_path)
model = torch.load(weight_path)

print("model:\n", model)
print("type: \n", type(model)) # 类型是 <class 'collections.OrderedDict'>
print("len: ", len(model)) # 长度为 126


for k in model.keys(): # 查看每个键
    print(k)        # 键名称
    # print(model[k]) # 键值
print("-----------------------")


net = Res_LSTM(sample_size=256, sample_duration=16, num_classes=101, arch="resnet50")
net.load_state_dict(model)
net_lstm = net.lstm
print("net.lstm:\n", net.lstm)
print("type: \n", type(net.lstm)) # 类型是 <class 'collections.OrderedDict'>


print("-----------------------")

