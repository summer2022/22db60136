#include "bind.h"

#include <stdio.h>

#include "mpi_sys.h"

HI_S32 VI_Bind_VPSS(VI_PIPE ViPipe, VI_CHN ViChn, VPSS_GRP VpssGrp, VPSS_CHN VpssChn)
{
    HI_S32 s32Ret = 0;

    MPP_CHN_S stSrcChn =
        {
            .enModId = HI_ID_VI,
            .s32DevId = ViPipe,
            .s32ChnId = ViChn,
        };
    MPP_CHN_S stDstChn =
        {
            .enModId = HI_ID_VPSS,
            .s32DevId = VpssGrp,
            .s32ChnId = VpssChn,
        };

    s32Ret = HI_MPI_SYS_Bind(&stSrcChn, &stDstChn);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] HI_MPI_SYS_Bind: %x\n", s32Ret);
        return HI_FAILURE;
    }

    return HI_SUCCESS;
}

HI_VOID VI_UnBind_VPSS(VI_PIPE ViPipe, VI_CHN ViChn, VPSS_GRP VpssGrp, VPSS_CHN VpssChn)
{
    HI_S32 s32Ret = 0;

    MPP_CHN_S stSrcChn =
        {
            .enModId = HI_ID_VI,
            .s32DevId = ViPipe,
            .s32ChnId = ViChn,
        };
    MPP_CHN_S stDstChn =
        {
            .enModId = HI_ID_VPSS,
            .s32DevId = VpssGrp,
            .s32ChnId = VpssChn,
        };

    s32Ret = HI_MPI_SYS_UnBind(&stSrcChn, &stDstChn);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] HI_MPI_SYS_UnBind: %x\n", s32Ret);
    }
}

HI_S32 VPSS_Bind_VO(VPSS_GRP VpssGrp, VPSS_CHN VpssChn, VO_LAYER VoLayer, VO_CHN VoChn)
{
    HI_S32 s32Ret = 0;

    MPP_CHN_S stSrcChn =
        {
            .enModId = HI_ID_VPSS,
            .s32DevId = VpssGrp,
            .s32ChnId = VpssChn,
        };
    MPP_CHN_S stDstChn =
        {
            .enModId = HI_ID_VO,
            .s32DevId = VoLayer,
            .s32ChnId = VoChn,
        };

    s32Ret = HI_MPI_SYS_Bind(&stSrcChn, &stDstChn);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] HI_MPI_SYS_Bind: %x\n", s32Ret);
        return HI_FAILURE;
    }

    return HI_SUCCESS;
}

HI_VOID VPSS_UnBind_VO(VPSS_GRP VpssGrp, VPSS_CHN VpssChn, VO_LAYER VoLayer, VO_CHN VoChn)
{
    HI_S32 s32Ret = 0;

    MPP_CHN_S stSrcChn =
        {
            .enModId = HI_ID_VPSS,
            .s32DevId = VpssGrp,
            .s32ChnId = VpssChn,
        };
    MPP_CHN_S stDstChn =
        {
            .enModId = HI_ID_VO,
            .s32DevId = VoLayer,
            .s32ChnId = VoChn,
        };

    s32Ret = HI_MPI_SYS_UnBind(&stSrcChn, &stDstChn);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] HI_MPI_SYS_UnBind: %x\n", s32Ret);
    }
}