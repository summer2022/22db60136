#ifndef __COMMON_ISP_H__
#define __COMMON_ISP_H__

#include <pthread.h>

#include "hi_types.h"
#include "hi_common.h"

HI_S32 ISP_Init(VI_PIPE ViPipe);

HI_VOID ISP_DeInit(ISP_DEV IspDev);

static pthread_t s_hIspThread = 0;

#endif