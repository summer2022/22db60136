#ifndef __HTTP_POST__
#define __HTTP_POST__
 
#define SERVER_ADDR	"192.168.131.60"
#define SERVER_PORT	 80
#define SERVER_PATH	"/HTTP/"
 
#define HTTP_HEAD   "POST /HTTP HTTP/1.1\r\n"\
		            "Host: 192.168.131.60\r\n"\
                    "Connection: keep-alive\r\n"\
                    "Cache-Insecure-Requests:1\r\n"\
                    "Origin: null\r\n"\
                    "Content-Type: multipart/form-data; boundary=%s\r\n"\
		            "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.66 Safari/537.36 Edg/103.0.1264.44\r\n"\
		            "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9\r\n"\
                    "Accept-Encoding: gzip, deflate\r\n"\
		            "Accept-Language: zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6\r\n"\
		            "Content-Length: %ld\r\n\r\n"\
					
					
#define UPLOAD_REQUEST	"--%s\r\n"\
			"Content-Disposition: form-data; name=\"file\"; filename=\"%s\"\r\n"\
			"Content-Type: text/plain\r\n\r\n"
						
unsigned long get_file_size(const char *path);
 
int http_post_upload_pic(const unsigned char *IP, const unsigned int port, const char *filepath,
									char *ack_json, int ack_len); //Post方式上传图片
int http(void);
 
#endif