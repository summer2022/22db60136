/*
 * Copyright (c) 2022 HiSilicon (Shanghai) Technologies CO., LIMITED.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
#include <hi_stdlib.h>
#include <hisignalling_protocol.h>
#include <hi_uart.h>
#include <app_demo_uart.h>
#include <iot_uart.h>
#include <hi_gpio.h>
#include <hi_io.h>
#include "iot_gpio_ex.h"
#include "ohos_init.h"
#include "cmsis_os2.h"


//process增加的代码
#define COUNT1 10
#define GPIO6  9
#define TASK_STAK_SIZE    (1024*10)
#include "iot_gpio.h"
unsigned int standard = 1500;
void set_angle(unsigned int duty)
{
    unsigned int time = 20000; 
    IoTGpioInit(GPIO6);
    IoTGpioSetDir(GPIO6, IOT_GPIO_DIR_OUT);
    IoTGpioSetOutputVal(GPIO6, IOT_GPIO_VALUE1);
    hi_udelay(duty);
    IoTGpioSetOutputVal(GPIO6, IOT_GPIO_VALUE0);
    hi_udelay(time - duty);
}
void engine_turn(unsigned int angle)
{
    for (int i = 0; i < COUNT1; i++) {
        set_angle(angle);
    }
}











UartDefConfig uartDefConfig = {0};

static void Uart1GpioCOnfig(void)
{
#ifdef ROBOT_BOARD
    IoSetFunc(HI_IO_NAME_GPIO_5, IOT_IO_FUNC_GPIO_5_UART1_RXD);
    IoSetFunc(HI_IO_NAME_GPIO_6, IOT_IO_FUNC_GPIO_6_UART1_TXD);
    /* IOT_BOARD */
#elif defined (EXPANSION_BOARD)
    IoSetFunc(HI_IO_NAME_GPIO_0, IOT_IO_FUNC_GPIO_0_UART1_TXD);
    IoSetFunc(HI_IO_NAME_GPIO_1, IOT_IO_FUNC_GPIO_1_UART1_RXD);
#endif
}

int SetUartRecvFlag(UartRecvDef def)
{
    if (def == UART_RECV_TRUE) {
        uartDefConfig.g_uartReceiveFlag = HI_TRUE;
    } else {
        uartDefConfig.g_uartReceiveFlag = HI_FALSE;
    }
    
    return uartDefConfig.g_uartReceiveFlag;
}

int GetUartConfig(UartDefType type)
{
    int receive = 0;

    switch (type) {
        case UART_RECEIVE_FLAG:
            receive = uartDefConfig.g_uartReceiveFlag;
            break;
        case UART_RECVIVE_LEN:
            receive = uartDefConfig.g_uartLen;
            break;
        default:
            break;
    }
    return receive;
}

void ResetUartReceiveMsg(void)
{
    (void)memset_s(uartDefConfig.g_receiveUartBuff, sizeof(uartDefConfig.g_receiveUartBuff),
        0x0, sizeof(uartDefConfig.g_receiveUartBuff));
}

unsigned char *GetUartReceiveMsg(void)
{
    return uartDefConfig.g_receiveUartBuff;
}

static hi_void *UartDemoTask(char *param)
{

    printf("UartDemoTask begin\r\n");

    hi_u8 uartBuff[UART_BUFF_SIZE] = {0};
    hi_unref_param(param);
    printf("Initialize uart demo successfully, please enter some datas via DEMO_UART_NUM port...\n");
    Uart1GpioCOnfig();
    for (;;) {
        uartDefConfig.g_uartLen = IoTUartRead(DEMO_UART_NUM, uartBuff, UART_BUFF_SIZE);

        if ((uartDefConfig.g_uartLen > 0) && (uartBuff[0] == 0xaa) && (uartBuff[1] == 0x55)) { //检验帧头   process增加的注释
            if (GetUartConfig(UART_RECEIVE_FLAG) == HI_FALSE) {//如果接收完成；则执行复制到uartDefConfig.g_receiveUartBuff内  process增加的注释
                (void)memcpy_s(uartDefConfig.g_receiveUartBuff, uartDefConfig.g_uartLen,
                    uartBuff, uartDefConfig.g_uartLen);//C中复制函数；将uartBuff内容复制到uartDefConfig.g_receiveUartBuff内  process增加的注释
                (void)SetUartRecvFlag(UART_RECV_TRUE);    //此处相当于改变uartDefConfig.g_uartReceiveFlag的值；后面用于if内条件是否成立  process增加的注释
            }
                printf("receine buff[5] == %c\n",uartDefConfig.g_receiveUartBuff[5]);
                switch (uartBuff[5])
                {
                   case 0x1:
                        printf("11111=-=-=-=-\n");
                        standard+=50;
                        engine_turn(standard);
                        osDelay(200);
                        break;
                   case 0x2:
                        printf("22222-=-=-=-=-\n");
                        standard-=50;
                        engine_turn(standard);
                        osDelay(200);
                        break;
                }
                OledShowString(20, 4,"hello in", 1); /* 屏幕第20列6行显示1行 */       //process增加的代码
            
        }

        TaskMsleep(20); /* 20:sleep 20ms */
//        printf("process test in app_demo_uart.c of interconnection_client_demo\n");//process增加的代码
        //存在疑问；为啥没有添加oled的头文件就能直接调用oled相关的函数？   process增加的注释
        OledFillScreen(0);    //process增加的代码  
        OledShowString(20, 4,"hello out", 1); /* 屏幕第20列6行显示1行 */       //process增加的代码


        // //process增加的代码
        // engine_turn(500);
        // printf("*****test***\n");
        // TaskMsleep(1000); /* 20:sleep 20ms */
        // engine_turn(15000);
        // osDelay(200);
    }
    return HI_NULL;
}



/*
 * This demo simply shows how to read datas from UART2 port and then echo back.
 */
hi_void UartTransmit(hi_void)
{
    hi_u32 ret = 0;

    IotUartAttribute uartAttr = {
        .baudRate = 115200, /* baudRate: 115200 */
        .dataBits = 8, /* dataBits: 8bits */
        .stopBits = 1, /* stop bit */
        .parity = 0,
    };
    engine_turn(standard);
    osDelay(100);


    /* Initialize uart driver */
    ret = IoTUartInit(DEMO_UART_NUM, &uartAttr);
    if (ret != HI_ERR_SUCCESS) {
        printf("Failed to init uart! Err code = %d\n", ret);
        return;
    }
    /* Create a task to handle uart communication */
    osThreadAttr_t attr = {0};
    attr.stack_size = UART_DEMO_TASK_STAK_SIZE;
    attr.priority = UART_DEMO_TASK_PRIORITY;
    attr.name = (hi_char*)"uart demo";
    if (osThreadNew((osThreadFunc_t)UartDemoTask, NULL, &attr) == NULL) {
        printf("Falied to create uart demo task!\n");
    }
}
SYS_RUN(UartTransmit);


// //process增加的代码
// void EngineTask(void* parame)
// {
//     (void)parame;
//     unsigned int time = 200;
//     engine_turn(500);
//     osDelay(200);
//     engine_turn(700);
//     osDelay(time);
//     engine_turn(1500);
//     osDelay(time);
//     printf("The steering gear is centered");
//     engine_turn(900);
//     osDelay(time);
//     engine_turn(1100);
//     osDelay(time);
//     engine_turn(1500);
//     printf("The steering gear is centered");
// }
// static void EngineDemo(void)
// {
//     osThreadAttr_t attr;

//     attr.name = "engineTask";
//     attr.attr_bits = 0U;
//     attr.cb_mem = NULL;
//     attr.cb_size = 0U;
//     attr.stack_mem = NULL;
//     attr.stack_size = TASK_STAK_SIZE;
//     attr.priority = osPriorityNormal;

//     if (osThreadNew(EngineTask, NULL, &attr) == NULL) {
//         printf("[EngineDemo] Falied to create EngineTask!\n");
//     }
// }
// APP_FEATURE_INIT(EngineDemo);