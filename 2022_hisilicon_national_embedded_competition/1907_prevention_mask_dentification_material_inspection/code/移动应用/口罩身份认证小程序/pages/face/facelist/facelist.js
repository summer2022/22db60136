var app = getApp();
var time = require("../../../utils/util.js");

Page({
  /** * 页面的初始数据** */
  data: {
    data: []
  },
  /** * 生命周期函数--监听页面加载*** */
  onLoad: function (options) {
    var that = this
    wx.request({
      url: 'https://dlut.cpolar.cn/data',
      success: function (res) {
        for(var i=0;i<res.data.ans.length;i++) {
          var newDate = new Date(res.data.ans[i]['CreationTimestamp'])
          var newTime = time.formatTime(newDate)
          res.data.ans[i]['Time']=newTime
        }
        console.log(res.data)
        that.setData({
          data: res.data.ans
        })
      }
    })
  },
  onShow: function (options) {
    var that = this
    wx.request({
      url: 'https://dlut.cpolar.cn/data',
      success: function (res) {
        for(var i=0;i<res.data.ans.length;i++) {
          var newDate = new Date(res.data.ans[i]['CreationTimestamp'])
          var newTime = time.formatTime(newDate)
          res.data.ans[i]['Time']=newTime
        }
        console.log(res.data)
        that.setData({
          data: res.data.ans
        })
      }
    })
  },
  /*** 用户点击右上角分享*/
  onShareAppMessage: function () {},

  mefuntion: function (e) {
    wx.navigateTo({
      url: '../faceadd/faceadd',
    })
  }
})