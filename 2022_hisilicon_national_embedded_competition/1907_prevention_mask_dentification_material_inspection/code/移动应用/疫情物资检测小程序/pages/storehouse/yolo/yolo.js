// pages/page1/page1.js
// 获取应用实例
const app = getApp()

Page({
  data: {
    data : []
  },
  onLoad() {
  },
  start() {
    var that = this
    wx.request({
      url: 'https://dlut.cpolar.cn/detect_thing',
      success: function (res) {
        console.log(res.data)
        that.setData({
          data:res.data.ans
        })
      }
    })
  },
  board() {
    wx.navigateTo({
      url: '../dashboard/dashboard',
    })
  }
})