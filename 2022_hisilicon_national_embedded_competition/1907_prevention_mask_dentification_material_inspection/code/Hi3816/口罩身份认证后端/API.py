# -*- coding: utf-8 -*-
from flask import Flask, request, jsonify
import Data
import Mask

app = Flask("my-app")

@app.route('/')
def start():
    answer = {}
    answer['flag']='success'
    return jsonify(answer)

@app.route('/data')
def data():
    answer = {}
    answer['flag']='success'
    ans = Data.get_data()
    answer['ans']=ans
    return jsonify(answer)

@app.route('/url')
def url():
    answer = {}
    answer['flag'] = 'success'
    word = request.args['base64']
    ans_text = Data.base_url(word)
    answer['ans'] = ans_text
    return jsonify(answer)

@app.route('/add')
def add():
    answer = {}
    answer['flag'] = 'success'
    face = request.args['face']
    mask = request.args['mask']
    name = request.args['name']
    id = request.args['id']
    gender = request.args['gender']
    ans_text = Data.add(face,mask,name,id,gender)
    answer['ans'] = ans_text
    return jsonify(answer)

@app.route('/mask_data')
def mask_data():
    answer = {}
    answer['flag']='success'
    ans = Mask.mask_data()
    answer['ans']=ans
    return jsonify(answer)

@app.route('/photo')
def photo():
    answer = {}
    answer['flag']='success'
    ans = Mask.photo()
    answer['ans']=ans
    return jsonify(answer)

@app.route('/mask')
def mask():
    answer = {}
    answer['flag']='success'
    url = request.args['url']
    time = request.args['time']
    ans = Mask.mask(time,url)
    answer['ans']=ans
    return jsonify(answer)

if __name__ == '__main__':
    app.config['JSON_AS_ASCII'] = False
    app.run(host='0.0.0.0', port=7031, debug=True)