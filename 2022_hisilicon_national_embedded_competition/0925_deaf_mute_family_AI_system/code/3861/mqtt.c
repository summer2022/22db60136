int IotSendMsg(int qos, const char *topic, const char *payload)
{
    int rc = -1;
    IoTMsg_t  *msg;
    char *buf;
    unsigned  int bufSize;

    bufSize = strlen(topic) + 1 +strlen(payload) + 1 + sizeof(IoTMsg_t);
    //buf = hi_malloc(0, bufSize);
    buf = readgesture();
    if (buf != NULL) {
        msg = (IoTMsg_t *)buf;
        buf += sizeof(IoTMsg_t);
        bufSize -= sizeof(IoTMsg_t);
        msg->qos = qos;
        msg->type = EN_IOT_MSG_PUBLISH;
        (void)memcpy_s(buf, bufSize, topic, strlen(topic));
        buf[strlen(topic)] = '\0';
        msg->topic = buf;
        buf += strlen(topic) + 1;
        bufSize -= (strlen(topic) + 1);
        (void)memcpy_s(buf, bufSize, payload, strlen(payload));
        buf[strlen(payload)] = '\0';
        msg->payload = buf;
        IOT_LOG_DEBUG("SNDMSG:QOS:%d TOPIC:%s PAYLOAD:%s\r\n", msg->qos, msg->topic, msg->payload);
        if (IOT_SUCCESS != osMessageQueuePut(gIoTAppCb.queueID, &msg, 0, CN_QUEUE_WAITTIMEOUT)) {
            IOT_LOG_ERROR("Write queue failed\r\n");
            hi_free(0, msg);
        } else {
            rc = 0;
        }
    }
    return rc;
}  //MQTT发送报文至对应的Topic中。

{
    int rc = 0, subQos[CN_TOPIC_SUBSCRIBE_NUM] = {1};

    MQTTClient client = NULL;
    MQTTClient_connectOptions connOpts = MQTTClient_connectOptions_initializer;
    char *clientID = CN_CLIENTID;
    char *userID = "homeassistant";
    char *userPwd = "re1ohbie8UoNo3thoosu2eiThae7tohm0oishezei6peixeeShoh0ETee5ogai7m";
    if (userPwd == NULL) {
        hi_free(0, clientID);
        return;
    }
    userPwd = CONFIG_USER_PWD;
    connOpts.keepAliveInterval = CN_KEEPALIVE_TIME;
    connOpts.cleansession = CN_CLEANSESSION;
    connOpts.username = userID;
    connOpts.password = "re1ohbie8UoNo3thoosu2eiThae7tohm0oishezei6peixeeShoh0ETee5ogai7m";
    connOpts.MQTTVersion = MQTTVERSION_3_1_1;
    IOT_LOG_DEBUG("CLIENTID:%s USERID:%s USERPWD:%s, IOTSERVER:%s\r\n",
        clientID, userID, userPwd==NULL?"NULL" : userPwd, CN_IOT_SERVER);
    rc = MQTTClient_create(&client, CN_IOT_SERVER, clientID, MQTTCLIENT_PERSISTENCE_NONE, NULL);
    if (rc != MQTTCLIENT_SUCCESS) {
        if (userPwd != NULL) {
            hi_free(0, userPwd);
        }
        return;
    }
}//连接到对应服务器。
void set_angle(unsigned int Dvalue1 ,unsigned int Dvalue2)
{   unsigned int time = 10000;
    unsigned int duty1 = Dvalue1 + 1500;
    unsigned int duty2 = Dvalue2 + 1500;
    for (int i = 0; i < COUNT; i++)
    {
        IoTGpioSetDir(GPIO8, IOT_GPIO_DIR_OUT);
        IoTGpioSetOutputVal(GPIO8, IOT_GPIO_VALUE1);
        hi_udelay(duty1);
        IoTGpioSetOutputVal(GPIO8, IOT_GPIO_VALUE0);
        hi_udelay(time - duty1);
        IoTGpioSetDir(GPIO7, IOT_GPIO_DIR_OUT);
        IoTGpioSetOutputVal(GPIO7, IOT_GPIO_VALUE1);
        hi_udelay(duty2);
        IoTGpioSetOutputVal(GPIO7, IOT_GPIO_VALUE0);
        hi_udelay(time - duty2);
    }
} //舵机旋转驱动函数。
{
        if (uartBuff[0] == 0x8){
        IoTGpioSetOutputVal(LED_TEST_GPIO, 0);
        usleep(LED_INTERVAL_TIME_US);
        set_angle(uartBuff[3],uartBuff[5]);
    }
}//摄像头跟随