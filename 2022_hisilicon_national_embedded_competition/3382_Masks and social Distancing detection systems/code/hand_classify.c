/*
 * Copyright (c) 2022 HiSilicon (Shanghai) Technologies CO., LIMITED.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <sys/prctl.h>
#include <math.h>

#include "sample_comm_nnie.h"
#include "sample_media_ai.h"
#include "ai_infer_process.h"
#include "yolov2_hand_detect.h"
#include "vgs_img.h"
#include "ive_img.h"
#include "misc_util.h"
#include "hisignalling.h"

#include "audio_aac_adp.h"
#include "base_interface.h"
#include "osd_img.h"
#include "posix_help.h"

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* End of #ifdef __cplusplus */

#define HAND_FRM_WIDTH     640
#define HAND_FRM_HEIGHT    384
#define DETECT_OBJ_MAX     32
#define RET_NUM_MAX        4
#define DRAW_RETC_THICK    2    // Draw the width of the line
#define WIDTH_LIMIT        32
#define HEIGHT_LIMIT       32
#define IMAGE_WIDTH        224  // The resolution of the model IMAGE sent to the classification is 224*224
#define IMAGE_HEIGHT       224
#define MODEL_FILE_GESTURE    "/userdata/models/hand_classify/hand_gesture.wk" // darknet framework wk model

static int biggestBoxIndex;
static IVE_IMAGE_S img;
static DetectObjInfo objs[DETECT_OBJ_MAX] = {0};
static RectBox boxs[DETECT_OBJ_MAX] = {0};
static RectBox objBoxs[DETECT_OBJ_MAX] = {0};
static RectBox remainingBoxs[DETECT_OBJ_MAX] = {0};
static RectBox cnnBoxs[DETECT_OBJ_MAX] = {0}; // Store the results of the classification network
static RecogNumInfo numInfo[RET_NUM_MAX] = {0};
static IVE_IMAGE_S imgIn;
static IVE_IMAGE_S imgDst;
static VIDEO_FRAME_INFO_S frmIn;
static VIDEO_FRAME_INFO_S frmDst;
int uartFd = 0;

static int g_num = 108;
static int g_count = 0;
#define AUDIO_CASE_TWO     2
#define AUDIO_SCORE        40       // Confidence can be configured by yourself
#define AUDIO_FRAME        14       // Recognize once every 15 frames, can be configured by yourself
#define MULTIPLE_OF_EXPANSION 100 
#define SCORE_MAX           4096  
#define UNKOWN_WASTE          3
#define BUFFER_SIZE           16    // buffer size

static HI_BOOL g_bAudioProcessStopSignal = HI_FALSE;
static pthread_t g_audioProcessThread = 0;


static SkPair g_stmChn = {
    .in = -1,
    .out = -1
};

//语音播报的线程函数和线程处理函数
static HI_VOID PlayAudio(const RecogNumInfo items)
{
    if  (g_count < AUDIO_FRAME) {
        g_count++;
        return;
    }

    const RecogNumInfo *item = &items;
    uint32_t score = item->score * MULTIPLE_OF_EXPANSION / SCORE_MAX;
    if ((score > AUDIO_SCORE) && (g_num != item->num)) {
        g_num = item->num;
        if (g_num != UNKOWN_WASTE) {
            //播放录音
            AudioTest(g_num, -1);
        }
    }
    g_count = 0;
}

static HI_VOID* GetAudioFileName(HI_VOID* arg)
{
    RecogNumInfo resBuf = {0};
    int ret;

    while (g_bAudioProcessStopSignal == false) {
        ret = FdReadMsg(g_stmChn.in, &resBuf, sizeof(RecogNumInfo));
        if (ret == sizeof(RecogNumInfo)) {
            PlayAudio(resBuf);
        }
    }

    return NULL;
}
//语音播报函数结束

HI_S32 Yolo2HandDetectResnetClassifyLoad(uintptr_t* model)
{
    SAMPLE_SVP_NNIE_CFG_S *self = NULL;
    HI_S32 ret;

    HI_CHAR audioThreadName[BUFFER_SIZE] = {0};

    ret = CnnCreate(&self, MODEL_FILE_GESTURE);
    *model = ret < 0 ? 0 : (uintptr_t)self;
    HandDetectInit(); // Initialize the hand detection model
    SAMPLE_PRT("Load hand detect claasify model success\n");


    //创建音频处理线程
    if (GetCfgBool("audio_player:support_audio", true)) {
        ret = SkPairCreate(&g_stmChn);
        HI_ASSERT(ret == 0);
        if (snprintf_s(audioThreadName, BUFFER_SIZE, BUFFER_SIZE - 1, "AudioProcess") < 0) {
            HI_ASSERT(0);
        }
        prctl(PR_SET_NAME, (unsigned long)audioThreadName, 0, 0, 0);
        //线程函数为GetAudioFileName
        ret = pthread_create(&g_audioProcessThread, NULL, GetAudioFileName, NULL);
        //异常判断函数
        if (ret != 0) {
            SAMPLE_PRT("audio proccess thread creat fail:%s\n", strerror(ret));
            return ret;
        }
    }
    
    SAMPLE_PRT("jinqu11111111111111\n");

    /* uart open init */
    //实现通讯
    uartFd = UartOpenInit();
    if (uartFd < 0) {
        printf("uart1 open failed\r\n");
    } else {
        printf("uart1 open successed\r\n");
    }
    return ret;
}

HI_S32 Yolo2HandDetectResnetClassifyUnload(uintptr_t model)
{
    CnnDestroy((SAMPLE_SVP_NNIE_CFG_S*)model);
    HandDetectExit(); // Uninitialize the hand detection model
    SAMPLE_PRT("Unload hand detect claasify model success\n");

    if (GetCfgBool("audio_player:support_audio", true)) {
        SkPairDestroy(&g_stmChn);
        SAMPLE_PRT("SkPairDestroy success\n");
        g_bAudioProcessStopSignal = HI_TRUE;
        pthread_join(g_audioProcessThread, NULL);
        g_audioProcessThread = 0;
    }

    return 0;
}

/* Get the maximum hand */
static HI_S32 GetBiggestHandIndex(RectBox boxs[], int detectNum)
{
    HI_S32 handIndex = 0;
    HI_S32 biggestBoxIndex = handIndex;
    HI_S32 biggestBoxWidth = boxs[handIndex].xmax - boxs[handIndex].xmin + 1;
    HI_S32 biggestBoxHeight = boxs[handIndex].ymax - boxs[handIndex].ymin + 1;
    HI_S32 biggestBoxArea = biggestBoxWidth * biggestBoxHeight;

    for (handIndex = 1; handIndex < detectNum; handIndex++) {
        HI_S32 boxWidth = boxs[handIndex].xmax - boxs[handIndex].xmin + 1;
        HI_S32 boxHeight = boxs[handIndex].ymax - boxs[handIndex].ymin + 1;
        HI_S32 boxArea = boxWidth * boxHeight;
        if (biggestBoxArea < boxArea) {
            biggestBoxArea = boxArea;
            biggestBoxIndex = handIndex;
        }
        biggestBoxWidth = boxs[biggestBoxIndex].xmax - boxs[biggestBoxIndex].xmin + 1;
        biggestBoxHeight = boxs[biggestBoxIndex].ymax - boxs[biggestBoxIndex].ymin + 1;
    }

    if ((biggestBoxWidth == 1) || (biggestBoxHeight == 1) || (detectNum == 0)) {
        biggestBoxIndex = -1;
    }

    return biggestBoxIndex;
}

/* hand gesture recognition info */
static void HandDetectFlag(const RecogNumInfo resBuf)
{
    HI_CHAR *gestureName = NULL;
    switch (resBuf.num) {
        case 0u:
            gestureName = " Not wear mask";
            //UartSendRead(uartFd, FistGesture); // 拳头手势
            SAMPLE_PRT("----status name:----:%s\n", gestureName);
            break;
        case 1u:
            gestureName = "Wear mask";
            //UartSendRead(uartFd, ForefingerGesture); // 食指手势
            SAMPLE_PRT("----status name:----:%s\n", gestureName);
            break;
        // case 2u:
        //     gestureName = "gesture OK";
        //     //UartSendRead(uartFd, OkGesture); // OK手势
        //     SAMPLE_PRT("----gesture name----:%s\n", gestureName);
        //     break;
        // case 3u:
        //     gestureName = "gesture palm";
        //     //UartSendRead(uartFd, PalmGesture); // 手掌手势
        //     SAMPLE_PRT("----gesture name----:%s\n", gestureName);
        //     break;
        // case 4u:
        //     gestureName = "gesture yes";
        //     //UartSendRead(uartFd, YesGesture); // yes手势
        //     SAMPLE_PRT("----gesture name----:%s\n", gestureName);
        //     break;
        // case 5u:
        //     gestureName = "gesture pinchOpen";
        //     //UartSendRead(uartFd, ForefingerAndThumbGesture); // 食指 + 大拇指
        //     SAMPLE_PRT("----gesture name----:%s\n", gestureName);
        //     break;
        // case 6u:
        //     gestureName = "gesture phoneCall";
        //     //UartSendRead(uartFd, LittleFingerAndThumbGesture); // 大拇指 + 小拇指
        //     SAMPLE_PRT("----gesture name----:%s\n", gestureName);
        //     break;
        default:
            gestureName = "gesture others";
            //UartSendRead(uartFd, InvalidGesture); // 无效值
            SAMPLE_PRT("----gesture name----:%s\n", gestureName);
            break;
    }
    SAMPLE_PRT("hand gesture success\n");
}

//手部检测加手势识别实现
HI_S32 Yolo2HandDetectResnetClassifyCal(uintptr_t model, VIDEO_FRAME_INFO_S *srcFrm, VIDEO_FRAME_INFO_S *dstFrm)
{
    SAMPLE_SVP_NNIE_CFG_S *self = (SAMPLE_SVP_NNIE_CFG_S*)model;
    HI_S32 resLen = 0;
    int objNum;
    int ret;
    int num = 0;

    //色彩空间转换将图像转换成yuv格式
    ret = FrmToOrigImg((VIDEO_FRAME_INFO_S*)srcFrm, &img);
    SAMPLE_CHECK_EXPR_RET(ret != HI_SUCCESS, ret, "hand detect for YUV Frm to Img FAIL, ret=%#x\n", ret);

    //将手部检测的图片送入手部检测，返回目标检测数
    objNum = HandDetectCal(&img, objs); // Send IMG to the detection net for reasoning
    for (int i = 0; i < objNum; i++) {
        cnnBoxs[i] = objs[i].box;
        RectBox *box = &objs[i].box;
        RectBoxTran(box, HAND_FRM_WIDTH, HAND_FRM_HEIGHT,
            dstFrm->stVFrame.u32Width, dstFrm->stVFrame.u32Height);
        SAMPLE_PRT("yolo2_out: {%d, %d, %d, %d}\n", box->xmin, box->ymin, box->xmax, box->ymax);
        boxs[i] = *box;
    }
    //目标框最大的手
    biggestBoxIndex = GetBiggestHandIndex(boxs, objNum);
    SAMPLE_PRT("biggestBoxIndex:%d, objNum:%d\n", biggestBoxIndex, objNum);

    // When an object is detected, a rectangle is drawn in the DSTFRM
    //当检测到手时判断手的数量，其中最大的手画绿框，其余的手画红框
    // if (biggestBoxIndex >= 0) {
    //     objBoxs[0] = boxs[biggestBoxIndex];
    //     MppFrmDrawRects(dstFrm, objBoxs, 1, RGB888_GREEN, DRAW_RETC_THICK); // Target hand objnum is equal to 1

    //     for (int j = 0; (j < objNum) && (objNum > 1); j++) {
    //         if (j != biggestBoxIndex) {
    //             remainingBoxs[num++] = boxs[j];
    //             // others hand objnum is equal to objnum -1
    //             MppFrmDrawRects(dstFrm, remainingBoxs, objNum - 1, RGB888_RED, DRAW_RETC_THICK);
    //         }
    //     }
    // When an object is detected, a rectangle is drawn in the DSTFRM
    //当检测到手时判断手的数量，其中最大的手画绿框，其余的手画红框
    if (biggestBoxIndex >= 0) {
        double green_box_x = 0;
        double green_box_y = 0;
        objBoxs[0] = boxs[biggestBoxIndex];
        MppFrmDrawRects(dstFrm, objBoxs, 1, RGB888_GREEN, DRAW_RETC_THICK); // Target hand objnum is equal to 1
        //计算最大目标框的最下边坐标
        green_box_x = (objBoxs[0].xmin + objBoxs[0].xmax)/2;
        green_box_y = objBoxs[0].ymin;
        SAMPLE_PRT("GREEN_BOX_X:%lf,GREEN_BOX_Y:%lf\n",green_box_x,green_box_y);

        for (int j = 0; (j < objNum) && (objNum > 1); j++) {
            double red_box_x = 0;
            double red_box_y = 0;
            double distance =0;
            double real_distance =0;
            if (j != biggestBoxIndex) {
                remainingBoxs[num++] = boxs[j];
                // others hand objnum is equal to objnum -1
                MppFrmDrawRects(dstFrm, remainingBoxs, objNum - 1, RGB888_RED, DRAW_RETC_THICK);
                
                //计算其余框的最下边坐标
                red_box_x = (boxs[j].xmin + boxs[j].xmax)/2;
                red_box_y = boxs[j].ymin;
                //SAMPLE_PRT("RED_BOX_X:%lf,RED_BOX_Y:%lf\n",red_box_x,red_box_y);

                //计算距离
                distance = sqrt(pow((green_box_x-red_box_x),2)+pow((green_box_y - red_box_y),2));
                //SAMPLE_PRT("%d and %d keep distance\n",0,j);

                real_distance = distance / 1290;
                SAMPLE_PRT("real distance:%lf\n",real_distance);

                //判断社交距离
                if (real_distance < 1){
                    SAMPLE_PRT("please keep social distance\n");
                }

            }
        }
        
        // Crop the image to classification network
        //截取最大的手
        ret = ImgYuvCrop(&img, &imgIn, &cnnBoxs[biggestBoxIndex]);
        SAMPLE_CHECK_EXPR_RET(ret < 0, ret, "ImgYuvCrop FAIL, ret=%#x\n", ret);

        if ((imgIn.u32Width >= WIDTH_LIMIT) && (imgIn.u32Height >= HEIGHT_LIMIT)) {
            COMPRESS_MODE_E enCompressMode = srcFrm->stVFrame.enCompressMode;
            //色彩空间转换
            ret = OrigImgToFrm(&imgIn, &frmIn);
            frmIn.stVFrame.enCompressMode = enCompressMode;
            SAMPLE_PRT("crop u32Width = %d, img.u32Height = %d\n", imgIn.u32Width, imgIn.u32Height);
            //调整图片大小:224*224
            ret = MppFrmResize(&frmIn, &frmDst, IMAGE_WIDTH, IMAGE_HEIGHT);
            ret = FrmToOrigImg(&frmDst, &imgDst);
            //推理，结果存在numinfo里面
            ret = CnnCalU8c1Img(self,  &imgDst, numInfo, sizeof(numInfo) / sizeof((numInfo)[0]), &resLen);
            SAMPLE_CHECK_EXPR_RET(ret < 0, ret, "CnnCalU8c1Img FAIL, ret=%#x\n", ret);

            if (GetCfgBool("audio_player:support_audio", true)) {
                if (FdWriteMsg(g_stmChn.out, &numInfo[0], sizeof(RecogNumInfo)) != sizeof(RecogNumInfo)) {
                    SAMPLE_PRT("FdWriteMsg FAIL\n");
                }
            }

            HI_ASSERT(resLen <= sizeof(numInfo) / sizeof(numInfo[0]));
            //将其送到标签函数中解析numInfo[0]
            //SAMPLE_PRT("result num size = %d, numInfo[0] = %d,numInfo[1] = %d,numInfo[2] = %d,numInfo[3] = %d\n", sizeof(numInfo) / sizeof((numInfo)[0]), numInfo[0],numInfo[1], numInfo[2], numInfo[3]);
            HandDetectFlag(numInfo[0]);
            MppFrmDestroy(&frmDst);
        }
        IveImgDestroy(&imgIn);
    }

    return ret;
}

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* End of #ifdef __cplusplus */
