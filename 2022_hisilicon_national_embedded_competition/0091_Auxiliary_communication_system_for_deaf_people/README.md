# 基于海思 Taurus&Pegasus AI 的聋哑人辅助沟通系统

## 设计概述

+ 设计目的：实现聋哑人与普通人之间的无障碍沟通；
+ 应用领域：手语识别、语音识别等；
+ 技术特点：基于海思Taurus AI平台，部署推理YOLOv3模型；基于微信小程序端，内部搭载语音识别云；

## 系统概述

项目整体架构可以划分为硬件部分和软件部分。硬件部分使用海思的 Taurus &  Pegasus AI 开发套件，具体来说包含 Hi3516DV300 和 Hi3861V100 两块开发板。其中 3516 主要负责图像采集、处理和神经网络推理部分；3861 作为媒介，和腾讯云 IOT HUB 共同建立 3516 和手机小程序间的通信通路。3516 和 3861 通过 UART 串口实现通信，3861 通过连接 WIFI 实现和腾讯云的互通，最终实现与部署在腾讯云中的小程序的互联。软件部分主要分为手势识别模块、语音识别模块和小程序模块。其中手势识别模块用于训练、测试手势识别网络，并转化成 3516 可用的模型；语音识别模块允许用户通过语音输入，避免繁琐的打字过程；小程序模块则提供一个良好的人机交互 UI，降低用户使用难度，提升可用性。

## 功能说明

用户通过小程序打字或语音转文字的方式编辑待发送的内容；小程序会将信息通过云端发送并以文字形式显示到海思 Pegasus 开发板的Oled显示屏上。聋哑用户阅 读文字信息后，通过手语的方式回复，通过神经网络分析得到文字翻译，并发送回小程序， 显示到对话框中。

## 代码说明

yolov3_gesture_detection中含有用于在Taurus开发板上做推理的代码；hello_world_iot中含有在Pegasus开发板上连接Wifi与Tecent IoT Hub的代码，也包括也Taurus通过串口通信的部分相关代码。