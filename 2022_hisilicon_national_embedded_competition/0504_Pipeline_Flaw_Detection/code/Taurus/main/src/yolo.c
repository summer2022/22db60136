#include <stdlib.h>
#include<stdio.h>
#include<string.h>
#include <sys/timeb.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>

#include "yolo_sample.h"

#include "sample_comm_ive.h"
//#include "ivs_md.h"
//#include "svp.h"
//extern void* svp_pub;

static pthread_t s_hMdThread = 0;
static HI_BOOL s_bStopSignal = HI_FALSE;
static void* pHand = NULL;
static void* yolo_task(void* p);

int yolo_start()
{
  int VpssGrp = 0;
  int VpssChn = 1;
  char ModelPath[256];
  //sprintf(ModelPath, "%s/model/yolov5-voc.wk", home_path);
  sprintf(ModelPath, "/userdata/RTMP/model/yolov5_pipeflaw_inst.wk");///userdata/model/yolov5fire_inst_1113rgb.wk/yolov5_pipeflaw_inst.wk

  printf("ModelPath:[%s]\n", ModelPath);
  if(yolo_init(VpssGrp, VpssChn,  ModelPath) < 0)
  {
      printf("yolo_init err.\n");
      return -1;
  }
  s_bStopSignal = HI_FALSE;
  return pthread_create(&s_hMdThread, NULL, yolo_task, NULL);
}

int yolo_stop()
{
  if(s_hMdThread)
  {
    s_bStopSignal = HI_TRUE;
    pthread_join(s_hMdThread, NULL);
    s_hMdThread = 0;
  }
}



static void* yolo_task(void* p)
{
  while (HI_FALSE == s_bStopSignal)
  {
    //usleep(33/3*1000);
      
    //yolo_boxs_t boxs = {0};
	  //printf("***********\n");
	  //printf("yolo_task begin detect\n");
      int ret = yolo_detect(); //&boxs
      //if(ret == 0)
        //pub_send(&boxs);
  }
  
  //yolo_boxs_t boxs = {0};
  //pub_send(&boxs);
  printf("HSJ Debug: befor yolo_deinit.\n");//HSJ Debug
  yolo_deinit();
  printf("HSJ Debug: after yolo_deinit.\n");//HSJ Debug
  return NULL;
}
