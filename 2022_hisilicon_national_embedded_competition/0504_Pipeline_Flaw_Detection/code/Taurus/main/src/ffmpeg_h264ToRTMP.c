#include <stdio.h>

#define __STDC_CONSTANT_MACROS

#ifdef __cplusplus
extern "C"
{
#endif
#include <libavformat/avformat.h>
#include <libavutil/mathematics.h>
#include <libavutil/time.h>
#include <libavcodec/avcodec.h>
#include <libavformat/avio.h>
#include <libavutil/file.h>

#include "sample_store_rtmp.h"

#ifdef __cplusplus
};
#endif

//sd
/*
FIX: H.264 in some container format (FLV, MP4, MKV etc.) need 
"h264_mp4toannexb" bitstream filter (BSF)
  *Add SPS,PPS in front of IDR frame
  *Add start code ("0,0,0,1") in front of NALU
H.264 in some container (MPEG2TS) don't need this BSF.
*/
//'1': Use H.264 Bitstream Filter 
#define USE_H264BSF 0

/*
FIX:AAC in some container format (FLV, MP4, MKV etc.) need 
"aac_adtstoasc" bitstream filter (BSF)
*/
//'1': Use AAC Bitstream Filter 
#define USE_AACBSF 0


int myfunc_h264ToFlv(void)
{
	
	//Write file header
	if (avformat_write_header(ofmt_ctx, NULL) < 0) {
		printf( "Error occurred when opening output file\n");
		goto end;
	}


	//FIX
#if USE_H264BSF
	AVBitStreamFilterContext* h264bsfc =  av_bitstream_filter_init("h264_mp4toannexb"); 
#endif
#if USE_AACBSF
	AVBitStreamFilterContext* aacbsfc =  av_bitstream_filter_init("aac_adtstoasc"); 
#endif

	while (1) 
	{
		AVFormatContext *ifmt_ctx;
		int stream_index=0;
		AVStream *in_stream, *out_stream;

		//Get an AVPacket
		//if(av_compare_ts(cur_pts_v,ifmt_ctx_v->streams[videoindex_v]->time_base,cur_pts_a,ifmt_ctx_a->streams[audioindex_a]->time_base) <= 0)
		{
			ifmt_ctx=ifmt_ctx_v;
			stream_index=videoindex_out;

			if(av_read_frame(ifmt_ctx, &pkt) >= 0)
			{
				do
				{
					in_stream  = ifmt_ctx->streams[pkt.stream_index];
					out_stream = ofmt_ctx->streams[stream_index];

					if(pkt.stream_index==videoindex_v)
					{
						//FIX：No PTS (Example: Raw H.264)
						//Simple Write PTS
						if(pkt.pts==AV_NOPTS_VALUE)
						{
							//Write PTS
							AVRational time_base1=in_stream->time_base;
							//Duration between 2 frames (us)
							int64_t calc_duration=(double)AV_TIME_BASE/av_q2d(in_stream->r_frame_rate);
							//Parameters
							pkt.pts=(double)(frame_index*calc_duration)/(double)(av_q2d(time_base1)*AV_TIME_BASE);
							pkt.dts=pkt.pts;
							pkt.duration=(double)calc_duration/(double)(av_q2d(time_base1)*AV_TIME_BASE);
							frame_index++;
						}

						cur_pts_v=pkt.pts;
						break;
					}
				}while(av_read_frame(ifmt_ctx, &pkt) >= 0);
			}else{
				break;
			}
		}

#if USE_H264BSF
		av_bitstream_filter_filter(h264bsfc, in_stream->codec, NULL, &pkt.data, &pkt.size, pkt.data, pkt.size, 0);
#endif
#if USE_AACBSF
		av_bitstream_filter_filter(aacbsfc, out_stream->codec, NULL, &pkt.data, &pkt.size, pkt.data, pkt.size, 0);
#endif





int myfunc_FlvToRTMP(void)
{
	AVOutputFormat *ofmt_rtmp = NULL;
	//Input AVFormatContext and Output AVFormatContext
	AVFormatContext *ifmt_ctx = NULL, *ofmt_ctx_rtmp = NULL;
	AVPacket pkt;
	const char *in_filename, *out_filename_rtmp;
	int ret, i;
	int videoindex=-1;
	int frame_index=0;
	int64_t start_time=0;

	in_filename  = "stream_chn0.flv";// push flv data

	
	out_filename_rtmp = "rtmp://127.0.0.1/http_flv/test1";	// rtmp://localhost/publishlive/livestream

	
	

	
	av_register_all();
	//Network
	avformat_network_init();
	
	//@@@@@@@@@@@@@@@@@@ avio_alloc_context @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	struct mybuffer_data mybd = { 0 };
	uint8_t *mybuffer = NULL, *myavio_ctx_buffer = NULL;
	size_t mybuffer_size, myavio_ctx_buffer_size = 4096;

	
	/* slurp file content into buffer */
    ret = av_file_map(in_filename, &mybuffer, &mybuffer_size, 0, NULL);
    if (ret < 0)
        goto end;
	
	
	mybd.ptr  = mybuffer;
    mybd.size = mybuffer_size;
	
	if (!(ifmt_ctx = avformat_alloc_context())) {
        ret = AVERROR(ENOMEM);
        goto end;
    }

    myavio_ctx_buffer = av_malloc(myavio_ctx_buffer_size);
    if (!myavio_ctx_buffer) {
        ret = AVERROR(ENOMEM);
        goto end;
    }
	
	AVIOContext *myavio_ctx = avio_alloc_context(myavio_ctx_buffer, myavio_ctx_buffer_size,
							  0, &mybd, &read_packet, NULL, NULL);
	if (!myavio_ctx) {
        ret = AVERROR(ENOMEM);
        goto end;
	}
	ifmt_ctx->pb = myavio_ctx;
	//@@@@@@@@@@@@@@@@@@ end of avio_alloc_context @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		
		
	//Input
	//if ((ret = avformat_open_input(&ifmt_ctx, in_filename, 0, 0)) < 0) {
	if ((ret = avformat_open_input(&ifmt_ctx, NULL, 0, 0)) < 0) {
		printf( "Could not open input file.");
		goto end;
	}
	if ((ret = avformat_find_stream_info(ifmt_ctx, 0)) < 0) {
		printf( "Failed to retrieve input stream information");
		goto end;
	}

	for(i=0; i<ifmt_ctx->nb_streams; i++) 
		if(ifmt_ctx->streams[i]->codec->codec_type==AVMEDIA_TYPE_VIDEO){
			videoindex=i;
			break;
		}

	av_dump_format(ifmt_ctx, 0, in_filename, 0);

	//Output
	
	avformat_alloc_output_context2(&ofmt_ctx_rtmp, NULL, "flv", out_filename_rtmp); //RTMP
	//avformat_alloc_output_context2(&ofmt_ctx_rtmp, NULL, "mpegts", out_filename_rtmp);//UDP

	if (!ofmt_ctx_rtmp) {
		printf( "Could not create output context\n");
		ret = AVERROR_UNKNOWN;
		goto end;
	}
	ofmt_rtmp = ofmt_ctx_rtmp->oformat;
	for (i = 0; i < ifmt_ctx->nb_streams; i++) {
		//Create output AVStream according to input AVStream
		AVStream *in_stream = ifmt_ctx->streams[i];
		AVStream *out_stream = avformat_new_stream(ofmt_ctx_rtmp, in_stream->codec->codec);
		if (!out_stream) {
			printf( "Failed allocating output stream\n");
			ret = AVERROR_UNKNOWN;
			goto end;
		}
		//Copy the settings of AVCodecContext
		ret = avcodec_copy_context(out_stream->codec, in_stream->codec);
		if (ret < 0) {
			printf( "Failed to copy context from input to output stream codec context\n");
			goto end;
		}
		out_stream->codec->codec_tag = 0;
		if (ofmt_ctx_rtmp->oformat->flags & AVFMT_GLOBALHEADER)
			out_stream->codec->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;
	}
	//Dump Format------------------
	av_dump_format(ofmt_ctx_rtmp, 0, out_filename_rtmp, 1);
	//Open output URL
	if (!(ofmt_rtmp->flags & AVFMT_NOFILE)) {
		ret = avio_open(&ofmt_ctx_rtmp->pb, out_filename_rtmp, AVIO_FLAG_WRITE);
		if (ret < 0) {
			printf( "Could not open output URL '%s'", out_filename_rtmp);
			goto end;
		}
	}
	//Write file header
	ret = avformat_write_header(ofmt_ctx_rtmp, NULL);
	if (ret < 0) {
		printf( "Error occurred when opening output URL\n");
		goto end;
	}

	start_time=av_gettime();
	while (1) {
		AVStream *in_stream, *out_stream;
		//Get an AVPacket
		ret = av_read_frame(ifmt_ctx, &pkt);
		if (ret < 0)
			break;
		//FIX��No PTS (Example: Raw H.264)
		//Simple Write PTS
		if(pkt.pts==AV_NOPTS_VALUE){
			//Write PTS
			AVRational time_base1=ifmt_ctx->streams[videoindex]->time_base;
			//Duration between 2 frames (us)
			int64_t calc_duration=(double)AV_TIME_BASE/av_q2d(ifmt_ctx->streams[videoindex]->r_frame_rate);
			//Parameters
			pkt.pts=(double)(frame_index*calc_duration)/(double)(av_q2d(time_base1)*AV_TIME_BASE);
			pkt.dts=pkt.pts;
			pkt.duration=(double)calc_duration/(double)(av_q2d(time_base1)*AV_TIME_BASE);
		}
		//Important:Delay
		if(pkt.stream_index==videoindex){
			AVRational time_base=ifmt_ctx->streams[videoindex]->time_base;
			AVRational time_base_q={1,AV_TIME_BASE};
			int64_t pts_time = av_rescale_q(pkt.dts, time_base, time_base_q);
			int64_t now_time = av_gettime() - start_time;
			if (pts_time > now_time)
				av_usleep(pts_time - now_time);

		}

		in_stream  = ifmt_ctx->streams[pkt.stream_index];
		out_stream = ofmt_ctx_rtmp->streams[pkt.stream_index];
		/* copy packet */
		//Convert PTS/DTS
		pkt.pts = av_rescale_q_rnd(pkt.pts, in_stream->time_base, out_stream->time_base, (AV_ROUND_NEAR_INF|AV_ROUND_PASS_MINMAX));
		pkt.dts = av_rescale_q_rnd(pkt.dts, in_stream->time_base, out_stream->time_base, (AV_ROUND_NEAR_INF|AV_ROUND_PASS_MINMAX));
		pkt.duration = av_rescale_q(pkt.duration, in_stream->time_base, out_stream->time_base);
		pkt.pos = -1;
		//Print to Screen
		if(pkt.stream_index==videoindex){
			printf("Send %8d video frames to output URL\n",frame_index);
			frame_index++;
		}
		//ret = av_write_frame(ofmt_ctx_rtmp, &pkt);
		ret = av_interleaved_write_frame(ofmt_ctx_rtmp, &pkt);

		if (ret < 0) {
			printf( "Error muxing packet\n");
			break;
		}
		
		av_free_packet(&pkt);
		
	}
	//Write file trailer
	av_write_trailer(ofmt_ctx_rtmp);
end:
	avformat_close_input(&ifmt_ctx);
	
	//@@@@@@@@@@@@@@@ avio_alloc_context @@@@@@@@@@@@
	if (myavio_ctx)
        av_freep(&myavio_ctx->buffer);
    avio_context_free(&myavio_ctx);
	
	av_file_unmap(mybuffer, mybuffer_size);
	//@@@@@@@@@@@@@@@ end of avio_alloc_context @@@@@@@@@@@@
	
	/* close output */
	if (ofmt_ctx_rtmp && !(ofmt_rtmp->flags & AVFMT_NOFILE))
		avio_close(ofmt_ctx_rtmp->pb);
	avformat_free_context(ofmt_ctx_rtmp);
	

	
	if (ret < 0 && ret != AVERROR_EOF) {
		printf( "Error occurred.\n");
		return -1;
	}
	return 0;
}


int h264ToRTMP(void)
{
	myfunc_h264ToFlv();
	myfunc_FlvToRTMP();
}

