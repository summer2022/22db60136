/*
 * Copyright (c) 2022 HiSilicon (Shanghai) Technologies CO., LIMITED.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include <stdint.h>
#include "sample_comm.h"

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* End of #ifdef __cplusplus */

#ifndef SAMPLE_PRT
#define SAMPLE_PRT(fmt...) \
    do { \
        printf("[%s]-%d: ", __FUNCTION__, __LINE__); \
        printf(fmt); \
    } while (0)
#endif

typedef SAMPLE_VI_CONFIG_S ViCfg;

typedef struct SampleStoreInfo {
    SIZE_S stSize[2];
    VENC_GOP_MODE_E enGopMode;
    VENC_GOP_ATTR_S stGopAttr;
    SAMPLE_RC_E     enRcMode;
    ViCfg viCfg;
} SampleStoreInfo;

HI_S32 Pipeflaw_VENC_SaveFile(VENC_CHN VeChn[], HI_S32 s32Cnt);
HI_S32 Pipeflaw_VENC_StopSaveFile(void);

HI_S32 Pipeflaw_VENC_StartRTMP(void);
HI_S32 Pipeflaw_VENC_StopRTMP(void);


int h264ToRTMP(void);
int h264ToRTMP_timing(void);


#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* End of #ifdef __cplusplus */


