/*
 * Copyright (C) 2022 HiHope Open Source Organization .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 *
 * limitations under the License.
 */

#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include "ohos_init.h"
#include "cmsis_os2.h"
#include "iot_gpio.h"
#include "hi_io.h"
#include "hi_gpio.h"
#include "hi_time.h"
#include "iot_pwm.h"
#include "hi_pwm.h"


#include "robot_l9110s.h"

#define GPIO13 13   
#define GPIO14 14
#define GPIO9 9
#define GPIO10 10
#define GPIOFUNC 0

// #define TASK_STAK_SIZE    (1024*10)

#define     PWM_FREQ_FREQUENCY                (16000)
// char web_control[] = '';
// extern char cmd_test[128]= "0";

// void web_recieve_deal(char web_data)
// {

// 	if(web_data[0]=="1" && web_data[2]=="2")
// 		{
// 			web_control[0] = web_data[1];
// 		}
// 	else
// 		{
// 			web_control[0] = "N";
// 		}

// }

// void gpio_control (unsigned int gpio, IotGpioValue value)
// {
//     hi_io_set_func(gpio, GPIOFUNC);
//     IoTGpioSetDir(gpio, IOT_GPIO_DIR_OUT);
//     IoTGpioSetOutputVal(gpio, value);
// }

hi_void gpio_control(hi_io_name gpio, hi_gpio_idx id, hi_gpio_dir  dir,  hi_gpio_value gpio_val, hi_u8 val)
{
  //gpio_control(HI_IO_NAME_GPIO_9, HI_GPIO_IDX_9, HI_GPIO_DIR_OUT, HI_GPIO_VALUE1, HI_IO_FUNC_GPIO_9_GPIO);
  if(gpio==HI_IO_NAME_GPIO_9){
      gpio=HI_IO_NAME_GPIO_10;
      id = HI_GPIO_IDX_10;
      val= HI_IO_FUNC_GPIO_10_GPIO;
  }else if(gpio==HI_IO_NAME_GPIO_10){
      gpio=HI_IO_NAME_GPIO_9;
      id = HI_GPIO_IDX_9;
      val= HI_IO_FUNC_GPIO_9_GPIO;
  }

    hi_io_set_func(gpio, val);
    hi_gpio_set_dir(id, dir);
    hi_gpio_set_ouput_val(id, gpio_val);
}

/*pwm control func*/
hi_void pwm_control(hi_io_name id, hi_u8 val, hi_pwm_port port, hi_u16 duty)
{

    //HI_IO_NAME_GPIO_10,HI_IO_FUNC_GPIO_10_PWM1_OUT,HI_PWM_PORT_PWM1, g_car_speed
    if(id==HI_IO_NAME_GPIO_10){
        id = HI_IO_NAME_GPIO_9;
        val= HI_IO_FUNC_GPIO_9_PWM0_OUT;
        port=HI_PWM_PORT_PWM0;  
    }else if(id==HI_IO_NAME_GPIO_9){
        id = HI_IO_NAME_GPIO_10;
        val= HI_IO_FUNC_GPIO_10_PWM1_OUT;
        port=HI_PWM_PORT_PWM1; 
    }

    hi_io_set_func(id, val);
    hi_pwm_init(port);
    hi_pwm_set_clock(PWM_CLK_160M);
    hi_pwm_start(port, duty, PWM_FREQ_FREQUENCY);
}

/*car go forward */
// hi_void car_go_forward(hi_void)
// {
//     // correct_car_speed();
//     pwm_control(HI_IO_NAME_GPIO_0,HI_IO_FUNC_GPIO_0_PWM3_OUT,HI_PWM_PORT_PWM3, 100); 
//     gpio_control2(HI_IO_NAME_GPIO_1, HI_GPIO_IDX_1, HI_GPIO_DIR_OUT, HI_GPIO_VALUE1, HI_IO_FUNC_GPIO_1_GPIO);
//     pwm_control(HI_IO_NAME_GPIO_9,HI_IO_FUNC_GPIO_9_PWM0_OUT,HI_PWM_PORT_PWM0, 100); 
//     gpio_control2(HI_IO_NAME_GPIO_10, HI_GPIO_IDX_10, HI_GPIO_DIR_OUT, HI_GPIO_VALUE1, HI_IO_FUNC_GPIO_10_GPIO);
// }


void car_backward(void)
{
    // pwm_control(HI_IO_NAME_GPIO_0,HI_IO_FUNC_GPIO_0_PWM3_OUT,HI_PWM_PORT_PWM3, 100); 
    // gpio_control2(HI_IO_NAME_GPIO_1, HI_GPIO_IDX_1, HI_GPIO_DIR_OUT, HI_GPIO_VALUE1, HI_IO_FUNC_GPIO_1_GPIO);
    // pwm_control(HI_IO_NAME_GPIO_9,HI_IO_FUNC_GPIO_9_PWM0_OUT,HI_PWM_PORT_PWM0, 100); 
    // gpio_control2(HI_IO_NAME_GPIO_10, HI_GPIO_IDX_10, HI_GPIO_DIR_OUT, HI_GPIO_VALUE1, HI_IO_FUNC_GPIO_10_GPIO);

    gpio_control(HI_IO_NAME_GPIO_13, HI_GPIO_IDX_13, HI_GPIO_DIR_OUT, HI_GPIO_VALUE0, HI_IO_FUNC_GPIO_13_GPIO);
    pwm_control(HI_IO_NAME_GPIO_14,HI_IO_FUNC_GPIO_14_PWM5_OUT,HI_PWM_PORT_PWM5, 15000);
    gpio_control(HI_IO_NAME_GPIO_9, HI_GPIO_IDX_9, HI_GPIO_DIR_OUT, HI_GPIO_VALUE0, HI_IO_FUNC_GPIO_9_GPIO);
    pwm_control(HI_IO_NAME_GPIO_10,HI_IO_FUNC_GPIO_10_PWM1_OUT,HI_PWM_PORT_PWM1, 15000);
}

void car_forward(void)
{
    pwm_control(HI_IO_NAME_GPIO_13,HI_IO_FUNC_GPIO_13_PWM4_OUT,HI_PWM_PORT_PWM4, 15000); 
    gpio_control(HI_IO_NAME_GPIO_14, HI_GPIO_IDX_14, HI_GPIO_DIR_OUT, HI_GPIO_VALUE0, HI_IO_FUNC_GPIO_14_GPIO);
    pwm_control(HI_IO_NAME_GPIO_9,HI_IO_FUNC_GPIO_9_PWM0_OUT,HI_PWM_PORT_PWM0, 15000); 
    gpio_control(HI_IO_NAME_GPIO_10, HI_GPIO_IDX_10, HI_GPIO_DIR_OUT, HI_GPIO_VALUE0, HI_IO_FUNC_GPIO_10_GPIO);
}

void car_left(void)
{

    pwm_control(HI_IO_NAME_GPIO_13,HI_IO_FUNC_GPIO_13_PWM4_OUT,HI_PWM_PORT_PWM4, 10000);   //前左边
    gpio_control(HI_IO_NAME_GPIO_14, HI_GPIO_IDX_14, HI_GPIO_DIR_OUT, HI_GPIO_VALUE0, HI_IO_FUNC_GPIO_14_GPIO);
    //pwm_control(HI_IO_NAME_GPIO_9,HI_IO_FUNC_GPIO_9_PWM0_OUT,HI_PWM_PORT_PWM0, 30000); 
    pwm_control(HI_IO_NAME_GPIO_10,HI_IO_FUNC_GPIO_10_PWM1_OUT,HI_PWM_PORT_PWM1, 15000);//前右边
    gpio_control(HI_IO_NAME_GPIO_9, HI_GPIO_IDX_9, HI_GPIO_DIR_OUT, HI_GPIO_VALUE0, HI_IO_FUNC_GPIO_9_GPIO);

    //gpio_control(HI_IO_NAME_GPIO_10, HI_GPIO_IDX_10, HI_GPIO_DIR_OUT, HI_GPIO_VALUE1, HI_IO_FUNC_GPIO_10_GPIO);
}

void car_right(void)
{
    //pwm_control(HI_IO_NAME_GPIO_0,HI_IO_FUNC_GPIO_0_PWM3_OUT,HI_PWM_PORT_PWM3, 30000); 
    //gpio_control(HI_IO_NAME_GPIO_1, HI_GPIO_IDX_1, HI_GPIO_DIR_OUT, HI_GPIO_VALUE1, HI_IO_FUNC_GPIO_1_GPIO);
    gpio_control(HI_IO_NAME_GPIO_13, HI_GPIO_IDX_13, HI_GPIO_DIR_OUT, HI_GPIO_VALUE0, HI_IO_FUNC_GPIO_13_GPIO);
    pwm_control(HI_IO_NAME_GPIO_14,HI_IO_FUNC_GPIO_14_PWM5_OUT,HI_PWM_PORT_PWM5, 15000);
    pwm_control(HI_IO_NAME_GPIO_9,HI_IO_FUNC_GPIO_9_PWM0_OUT,HI_PWM_PORT_PWM0, 15000); 
    gpio_control(HI_IO_NAME_GPIO_10, HI_GPIO_IDX_10, HI_GPIO_DIR_OUT, HI_GPIO_VALUE0, HI_IO_FUNC_GPIO_10_GPIO);
}


void car_stop(void)
{
    pwm_control(HI_IO_NAME_GPIO_13,HI_IO_FUNC_GPIO_13_PWM4_OUT,HI_PWM_PORT_PWM4, 10);   
    gpio_control(HI_IO_NAME_GPIO_14, HI_GPIO_IDX_14, HI_GPIO_DIR_OUT, HI_GPIO_VALUE0, HI_IO_FUNC_GPIO_14_GPIO);
    pwm_control(HI_IO_NAME_GPIO_9,HI_IO_FUNC_GPIO_9_PWM0_OUT,HI_PWM_PORT_PWM0, 10);
    gpio_control(HI_IO_NAME_GPIO_10, HI_GPIO_IDX_10, HI_GPIO_DIR_OUT, HI_GPIO_VALUE0, HI_IO_FUNC_GPIO_10_GPIO);
}

// void RobotTask(void* parame)
// {
//     (void)parame;
//     printf("start test l9110s\r\n");
//     unsigned int time = 500;
//     // car_forward();
//     // osDelay(time);
//     // car_forward();
//     // osDelay(time);
//     // car_backward();
//     // osDelay(time);
//     // car_left();
//     // osDelay(time);
//     // car_right();
//     // osDelay(time);
//     // car_stop();
//     // osDelay(time);
//     car_direction_control_func();
// }


void car_direction_control_func(char * arg)
{

    char web_control_t = 0;
    char web_control_p = 0;
    while (1)
    {
        web_control_p = web_control_t;
        osDelay(50);
        web_control_t = *arg;
        if (web_control_t != web_control_p)
        {
            printf("web_control cmd = %d", web_control_t);
            switch (web_control_t)
            {
            case '1': //一直往前
                car_forward();
                break;
            case '2': //一直往后
                car_backward();
                break;
            case '3': //一直往左
                car_left();
                break;
            case '4': //一直往右
                car_right();
                break;
            case '5': //
                car_stop();
                break;
            default:
                car_stop();
                break;
            }
        }
        else
        {
            continue;
        }
    }
}


// static void RobotDemo(void)
// {
//     osThreadAttr_t attr;

//     attr.name = "RobotTask";
//     attr.attr_bits = 0U;
//     attr.cb_mem = NULL;
//     attr.cb_size = 0U;
//     attr.stack_mem = NULL;
//     attr.stack_size = TASK_STAK_SIZE;
//     attr.priority = osPriorityNormal;

//     if (osThreadNew(RobotTask, NULL, &attr) == NULL) {
//         printf("[RobotDemo] Falied to create RobotTask!\n");
//     }
// }

// APP_FEATURE_INIT(RobotDemo);
