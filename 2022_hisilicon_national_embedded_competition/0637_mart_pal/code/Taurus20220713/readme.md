### 目录结构
该目录内文件结构按照OpenHarmony_master/device/soc/hisilicon/hi3516dv300/sdk_linux/sample/taurus/目录下的目录结构进行组织，具体如下所示
```
│  readme.md # 本文件
│
├─acc_file  # 语音播报所需的音频文件
│      20.aac  # “请稍等”
│      21.aac  # “扫码成功”
│
└─ai_sample
    │  BUILD.gn # 不知道应该叫啥，编译用的
    │  readme.md
    │  
    ├─ai_infer_process
    │      ai_infer_process.c # 增加了针对条码检测模型的初始化、推理、卸载等函数以及相应的结构体，（人体检测模型则直接用之前的yolov2相关函数及结构体）
    │      ai_infer_process.h
    │      
    ├─denpendency
    │      audio_test.c # 语音播报依赖的代码，只修改了191行的循环播放时间为1.5s
    │      
    ├─interconnection_server
    │      hisignalling.c # 串口通信代码，增加了调用静态函数HisignallingMsgSend的函数my_HisignallingMsgSend以方便在该文件外直接调用HisignallingMsgSend进行串口通信
    │      hisignalling.h
    │      
    ├─scenario
    │  ├─handclassify  # 两个yolov2模型的初始化、模型的回调函数、串口发送、音频播报的实现等
    │  │      hand_classify.c 
    │  │      hand_classify.h
    │  │      yolov2_people_detect.c
    │  │      yolov2_people_detect.h
    │  │      
    │  └─tennis_detect
    │          barcode_wrapper.cpp # 对条形码的解码函数进行包装以便在C代码中调用
    │          barcode_wrapper.h
    │          tennis_detect.cpp # 条形码的解码函数
    │          tennis_detect.h
    │          
    ├─smp
    │      sample_ai_main.cpp # sample的主函数
    │      sample_media_ai.c # 媒体通路的连接和初始化、线程的创建等
    │      sample_media_ai.h
    
```
