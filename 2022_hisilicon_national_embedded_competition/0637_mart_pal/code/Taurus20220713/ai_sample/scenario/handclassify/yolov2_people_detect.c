/*
 * Copyright (c) 2022 HiSilicon (Shanghai) Technologies CO., LIMITED.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>

#include "sample_comm_nnie.h"
#include "ai_infer_process.h"
#include "sample_media_ai.h"

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* End of #ifdef __cplusplus */

#define MODEL_FILE_BARCODE  "/userdata/models/barcode_inst.wk"//"/userdata/models/person_detect_resnet18_inst.wk" // "/userdata/models/hand_classify/hand_detect.wk" // darknet framework wk model
#define MODEL_FILE_PEOPLE   "/userdata/models/person_detect_resnet18_inst.wk" // "/userdata/models/hand_classify/hand_detect.wk" // darknet framework wk model
#define PIRIOD_NUM_MAX      49 // Logs are printed when the number of targets is detected
#define DETECT_OBJ_MAX      32 // detect max obj


static uintptr_t g_peopleModel = 0;
static uintptr_t g_BarcodeModel = 1;



static HI_S32 Yolo2FdLoad(uintptr_t* model,int flag)
{
    SAMPLE_SVP_NNIE_CFG_S *self = NULL;
    HI_S32 ret;
    if(flag==1)
    {
        ret = Yolo2Create(&self, MODEL_FILE_PEOPLE);
        char * path=MODEL_FILE_PEOPLE;

        SAMPLE_PRT("---------- Loaded People Model Successfully ----------\n");
        SAMPLE_PRT("---------- Model Path : %s ----------\n",path);
    }
    else if (flag==2)
    {
        ret = Yolo2CreateforBarcode(&self, MODEL_FILE_BARCODE);
        char * path=MODEL_FILE_BARCODE;
        SAMPLE_PRT("---------- Loaded Barcode Model Successfully ----------\n");
        SAMPLE_PRT("---------- Model Path : %s ----------\n",path);
    }
    *model = ret < 0 ? 0 : (uintptr_t)self;
    SAMPLE_PRT("model:%d\n", *model);
    SAMPLE_PRT("g_peopleModel:%d\n", g_peopleModel);
    SAMPLE_PRT("g_BarcodeModel:%d\n", g_BarcodeModel);

    SAMPLE_PRT("Yolo2FdLoad ret:%d\n", ret);

    return ret;
}

HI_S32 PeopleDetectInit()
{
    return Yolo2FdLoad(&g_peopleModel,1);
}



static HI_S32 Yolo2FdUnload(uintptr_t model)
{
    Yolo2Destory((SAMPLE_SVP_NNIE_CFG_S*)model);
    return 0;
}



HI_S32 PeopleDetectExit()
{
    return Yolo2FdUnload(g_peopleModel);
}


static HI_S32 PeopleDetect(uintptr_t model, IVE_IMAGE_S *srcYuv, DetectObjInfo boxs[])
{
    SAMPLE_SVP_NNIE_CFG_S *self = (SAMPLE_SVP_NNIE_CFG_S*)model;
    int objNum;
    int ret = Yolo2CalImg(self, srcYuv, boxs, DETECT_OBJ_MAX, &objNum);
    if (ret < 0) {
        SAMPLE_PRT("People detect Yolo2CalImg FAIL, for cal FAIL, ret:%d\n", ret);
        return ret;
    }

    return objNum;
}



HI_S32 PeopleDetectCal(IVE_IMAGE_S *srcYuv, DetectObjInfo resArr[])
{
    SAMPLE_PRT("g_peopleModel in Func PeopleDetectCal:%d\n", g_peopleModel);
    int ret = PeopleDetect(g_peopleModel, srcYuv, resArr);
    return ret;
}


HI_S32 BarcodeDetectInit()
{
    return Yolo2FdLoad(&g_BarcodeModel,2);
}

static HI_S32 Yolo2FdUnloadforBarcode(uintptr_t model)
{
    Yolo2DestoryforBarcode((SAMPLE_SVP_NNIE_CFG_S*)model);
    return 0;
}

HI_S32 BarcodeDetectExit()
{
    return Yolo2FdUnloadforBarcode(g_BarcodeModel);
}

static HI_S32 BarcodeDetect(uintptr_t model, IVE_IMAGE_S *srcYuv, DetectObjInfo boxs[])
{
    SAMPLE_SVP_NNIE_CFG_S *self = (SAMPLE_SVP_NNIE_CFG_S*)model;
    int objNum;
    int ret = Yolo2CalImgforBarcode(self, srcYuv, boxs, DETECT_OBJ_MAX, &objNum);
    if (ret < 0) {
        SAMPLE_PRT("People detect Yolo2CalImg FAIL, for cal FAIL, ret:%d\n", ret);
        return ret;
    }

    return objNum;
}


HI_S32 BarcodeDetectCal(IVE_IMAGE_S *srcYuv, DetectObjInfo resArr[])
{
    SAMPLE_PRT("g_BarcodeModel in Func BarcodeDetectCal:%d\n", g_BarcodeModel);
    int ret = BarcodeDetect(g_BarcodeModel, srcYuv, resArr);
    return ret;
}



#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* End of #ifdef __cplusplus */
