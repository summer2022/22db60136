#ifndef ROBOT_MOTOR_H
#define ROBOT_MOTOR_H

#include <stdio.h>
#include <stdlib.h>

/*将4个gpio复用为4路pwm输出*/
void gpioMotorControllerInit(void);
/* 前进（单位m） */
void car_forward(float distance);
/* 后退（单位m） */
void car_backward(float distance);
/* 原地左转（单位°） */
void car_turnLeft(int angle);
/* 原地右转（单位°） */
void car_turnRight(int angle);
/* 制动 */
void car_stop(void);

/*设置电机pwm*/
void setLeftMotorPWM(int);
void setRightMotorPWM(int);
 
/*从buf读取速度*/
int getSpeed (char*);

#endif