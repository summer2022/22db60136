#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <memory.h>
#include <unistd.h>
#include "iot_watchdog.h"
#include "ohos_init.h"
#include "cmsis_os2.h"
#include "iot_gpio.h"
#include "hi_io.h"
#include "hi_time.h"
#include "iot_pwm.h"
#include "hi_pwm.h"
#include "iot_gpio_ex.h"

#define GPIO2 2
#define IOT_PWM_PORT_PWM2 2

/* 蜂鸣器初始化函数 */
hi_void UserBeepInit(void)
{
    hi_io_set_func(GPIO2, 5); 
    IoTGpioSetDir(GPIO2, IOT_GPIO_DIR_OUT);
    IoTPwmInit(IOT_PWM_PORT_PWM2);
}

/* 蜂鸣器调用函数 */
hi_void UserBeep(int duration)
{
    IoTPwmStart(IOT_PWM_PORT_PWM2, 50, 4000);   //GPIO2复用为PWM
    TaskMsleep(duration); //间隔1秒
    IoTPwmStop(IOT_PWM_PORT_PWM2);
}