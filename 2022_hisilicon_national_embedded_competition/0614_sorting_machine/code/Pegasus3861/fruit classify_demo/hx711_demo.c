/*
 * Copyright (c) 2022 HiSilicon (Shanghai) Technologies CO., LIMITED.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <math.h>
#include <hi_early_debug.h>
#include <hi_task.h>
#include <hi_time.h>
#include <hi_adc.h>
#include <hi_stdlib.h>
#include <hi_watchdog.h>
#include <hi_pwm.h>
#include <hi_io.h>
#include <hi_gpio.h>

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include "iot_gpio_ex.h"
#include "ohos_init.h"
#include "cmsis_os2.h"
#include "iot_i2c.h"
#include "iot_errno.h"
#include "iot_pwm.h"
#include "iot_gpio.h"
#include "iot_gpio_ex.h"
#include <hi_types_base.h>
#include <hi_early_debug.h>

#include "oled_ssd1306.h"
//校准参数
//因为不同的传感器特性曲线不是很一致，因此，每一个传感器需要矫正这里这个参数才能使测量值很准确。
//当发现测试出来的重量偏大时，增加该数值。
//如果测试出来的重量偏小时，减小改数值。
//该值可以为小数,可参考误差1g调整10
//#define GapValue 430                           //影响精度
double hx711_weight = 0;
double buf[10] = {0};
//#define GapValue1 530                           //影响精度
//double hx711_weight1 = 0;

unsigned long Sensor_Read(void);
double Get_Sensor_Read(void);
//double base_data = 0;

//unsigned long Sensor_Read1(void);
//double Get_Sensor_Read1(void);

#define LED_TEST_GPIO 9
/*hi_void gpio_init() {
	hi_io_set_func(HI_IO_NAME_GPIO_13, HI_IO_FUNC_GPIO_13_I2C0_SDA);
    hi_io_set_func(HI_IO_NAME_GPIO_14, HI_IO_FUNC_GPIO_14_I2C0_SCL);
}*/

#define AHT20_BAUDRATE (400 * 1000)
#define AHT20_I2C_IDX 0
#define LED_INTERVAL_TIME_US 3000000

/*double hi_hx711_task(hi_void)
{	
    printf("start loading\n");
	/*hi_gpio_init();

	//初始化GPIO7为GPIO输入，为传感器DT引脚
    hi_io_set_func(HI_IO_NAME_GPIO_7, HI_IO_FUNC_GPIO_7_GPIO);
    hi_gpio_set_dir(HI_GPIO_IDX_7,HI_GPIO_DIR_IN);
	//初始化GPIO8为GPIO输出，为传感器SCK引脚
    hi_io_set_func(HI_IO_NAME_GPIO_8, HI_IO_FUNC_GPIO_8_GPIO);
    hi_gpio_set_dir(HI_GPIO_IDX_8,HI_GPIO_DIR_OUT);
    hi_gpio_set_ouput_val(HI_IO_NAME_GPIO_8,0);

	OledInit();
	OledFillScreen(0);
	IoTI2cInit(AHT20_I2C_IDX, AHT20_BAUDRATE);*/

	//初始化i2c,oled屏使用
    //hi_i2c_init(HI_GPIO_IDX_0, HI_I2C_IDX_0); /* baudrate: 400kbps */
    //hi_i2c_set_schmitt(HI_GPIO_IDX_0, HI_I2C_IDX_0);

	//oled初始化并显示对应信息
    //while( HI_ERR_SUCCESS != OledInit()) ;
	//OledFillScreen(OLED_CLEAN_SCREEN);
    //OledShowString(5,1, "HX711  ",3);
	//oled_show_str(0,4, "Weigth : ",1);
    
	/*hi_u8 buf[10] = {0};
    base_data = Get_Sensor_Read(); //获取基准值
    //base_data = Sensor_Read(); //获取基准值
    for(int i=0;i<9;i++) {
        hx711_weight = (Sensor_Read() - base_data) / GapValue; //获取重量
		/*if(hx711_weight > 0) // 大于0时显示
        {
            printf("weight : %.2f\r\n" ,hx711_weight); 
			OledShowString(15,3, " weight :       ",2);
            sprintf_s(buf,10,"%.2f g ",hx711_weight);
            OledShowString(25,5, buf,2);
        }*/        
        /*hi_sleep(10);              							//该延时用于舵机控制与显示重量的联动  
        hi_gpio_set_ouput_val(LED_TEST_GPIO,0);
	}
	return hx711_weight;
}*/


/*void *PWM_Task(const char *arg)
{
    arg = arg;
    hi_gpio_init();
    hi_io_set_func(IOT_IO_NAME_GPIO_9,IOT_IO_FUNC_GPIO_9_PWM0_OUT);  
 
    //hi_pwm_init(HI_PWM_PORT_PWM0);

    for(int i=0;i<9;i++)
    {
//调用app_demo_pwm

        hi_hx711_task();
        usleep(10000);

    

    }
}*/

unsigned long Sensor_Read(void)
{
	unsigned long value = 0;
	unsigned char i = 0;
	hi_gpio_value input = 0;
	hi_udelay(2);
	//时钟线拉低 空闲时时钟线保持低电位
	hi_gpio_set_ouput_val(HI_IO_NAME_GPIO_8,0);
	hi_udelay(2);	
	hi_gpio_get_input_val(HI_IO_NAME_GPIO_7,&input);
	//等待AD转换结束
	while(input)
    {
        hi_gpio_get_input_val(HI_IO_NAME_GPIO_7,&input);
    }
	for(i=0;i<24;i++)
	{
		//时钟线拉高 开始发送时钟脉冲
		hi_gpio_set_ouput_val(HI_IO_NAME_GPIO_8,1);
		hi_udelay(2);
		//左移位 右侧补零 等待接收数据
		value = value << 1;
		//时钟线拉低
		hi_gpio_set_ouput_val(HI_IO_NAME_GPIO_8,0);
		hi_udelay(2);
		//读取一位数据
        hi_gpio_get_input_val(HI_IO_NAME_GPIO_7,&input);
		if(input){
			value ++;
        }
	}
	//第25个脉冲
	hi_gpio_set_ouput_val(HI_IO_NAME_GPIO_8,1);
	hi_udelay(2);
	value = value^0x800000;	
	//第25个脉冲结束
	hi_gpio_set_ouput_val(HI_IO_NAME_GPIO_8,0);	
	hi_udelay(2);	
	return value;
}

double Get_Sensor_Read(void)
{
  	double sum = 0;    // 为了减小误差，一次取出10个值后求平均值。
  	for (int i = 0; i < 10; i++) // 循环的越多精度越高，当然耗费的时间也越多
    	sum += Sensor_Read();  // 累加
  	return (sum/10); // 求平均值进行均差

}
