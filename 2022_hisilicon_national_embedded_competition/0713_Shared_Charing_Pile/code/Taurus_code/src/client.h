#ifndef Client_H
#define Client_H

#include <cstring>

//**********************************************************
//the function to send plate using tcp/ip
//input @ip: char* 
//		the ipv4 of the server
//	@port_:char*
//		the port of the server
//	@filename:char*
//		the file which contains what you want to send
//output @ret:int
//		if success:0
//		if failure:-1
//**********************************************************

int client(char* ip,char* port_,std::string strSent);

#endif
