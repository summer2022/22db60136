#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdlib.h>
#include <string>

#define N 128

using namespace std;

int client(char* ip,char* port_,string strSent)
{
	//argv[1] the ipv4 of the server
	//argv[2] the port of the server
	//argv[3] the file point you want to send 
	/*
	 if(argc!=4)
	{
		fprintf(stderr,"Usage :%s hostname portnumber\a\n",argv[0]);
		exit(1);
	}
	 if((argv[1] && argv[2] && argv[3]) == NULL)
	{
		herror("get para error\n");
		exit(1);
	}
	*/
	char buf[N];//字节流要一样大，服务器端设置为1024，这里也要是1024
 	char ch;
	int i = 0;

	//printf("three arg in is \narg1:%s\t\narg2:%s\t\narg3:%s\t\n",argv[1],argv[2],argv[3]);

	//FILE *fp = NULL ;
	//fp = fopen(argv[3],"r");
	/*fp = fopen(filename,"r");
	if(fp == NULL){
		printf("cannot open file!\n");
		return -1;	
	}*/

	char ch_temp;
	int port = atoi(port_);

	//create socket
	//int sockFd = socket(PF_INET,SOCK_STREAM,0);
	int sockFd = socket(AF_INET,SOCK_STREAM,0);
	if(sockFd < 0)
	{
		perror("create socket error!");
		printf("create socket error!\n");
		return -1;
	}
	printf("create socket OK!\n");
	
/*	char buf[1024*8];
	char ch;
	int i = 0;
        while((ch=fgetc(fp)) != EOF)
        {
          // putchar(ch);
	  buf[i] = ch;
	  i++;
	}
	printf("buf is:%s\n",buf);
	int s=strlen(buf);
	printf("the length of buf is:%d\n",s);
*/	
	//请求连接
	struct sockaddr_in servAddr = {0};
	//servAddr.sin_family = PF_INET;
	servAddr.sin_family = AF_INET;
	servAddr.sin_port = htons( (short unsigned int)port );
	

	printf("short unsigned int port = %d \n",(short unsigned int)port);
	//printf("servAddr.sin_port = %d \n",servAddr.sin_port);

	//servAddr.sin_addr.s_addr = inet_addr(argv[1]);
	//printf("test the function inet_addr\ninput 127.0.0.1\toutput:%x\n",inet_addr("127.0.0.1"));
	//if( !inet_aton(argv[1], &servAddr.sin_addr.s_addr)){
	if( !inet_aton(ip,(in_addr*)&servAddr.sin_addr.s_addr)){
		printf("wrong ip!\n");
	}
	
	printf("servAddr.s_addr = %x \n",servAddr.sin_addr.s_addr);
	int ret = connect(sockFd,(struct sockaddr *)&servAddr,sizeof(servAddr));
	if(ret < 0)
	{
		perror("connect error!");
		printf("connect error!\n");
		close(sockFd);
		return -1;
	}
	printf("connect OK!\n");
	//write
	printf("write to server:\n");

	//while(1)
	//for(int i=0; i<2; i++)
	{
		//fread(buf, strlen(buf)+1 , 1 , fp);
		//memset(buf,0,sizeof(buf));
		//printf("assert i am here!\n");
		/*while((ch=fgetc(fp)) != EOF)
        	{
		  buf[i] = ch;
		  i++;
		}*/

		memset(buf,0,N*sizeof(bool));
		strcpy(buf,strSent.c_str());

		//fgets(buf, N, fp);

		//i=0;
		int s=strlen(buf);
		//printf("the length of buf is:%d\n",s);
				
		//printf("write will do\n\n");
		ret = write(sockFd,buf,sizeof(buf));
		printf("write data have done!\n\n");
		if(ret < 0)
		{
			perror("write error!");
			printf("write error!\n");
			close(sockFd);
			return -1;
		}
		
		printf("write OK!\n");
		usleep(10000);
		//if(strncmp(buf,"quit",4) == 0)
		//	break;
	}
	//close

	//fclose(fp);
	close(sockFd);

	return 0;
}


