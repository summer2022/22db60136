/*
 * Copyright (c) 2022 HiSilicon (Shanghai) Technologies CO., LIMITED.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>
#include <stdlib.h>

#include "ohos_init.h"
#include "cmsis_os2.h"
#include "iot_i2c.h"
#include "iot_gpio.h"
#include "iot_errno.h"

#include "oled_ssd1306.h"

#include "my_request.h"
#include "plate.h"
#include "flag.h"

#define AHT20_BAUDRATE (400 * 1000)
#define AHT20_I2C_IDX 0 

int flag=0;
int gpio_flag[6]={0,0,0,0,0,0};
struct VLP vlp1;

void cb_timeout_periodic(int *arg)
{
    (int)arg;
    vlp1.parktime++;
}

static void OledmentTask(const char *arg)
{
    (void)arg;
    int left=0;
    //int Station;
    //int cari=0;
    char mytype[32]={0};
    //char my_request[32]={"AD06755"};
    OledInit();
    OledFillScreen(0);
    IoTI2cInit(AHT20_I2C_IDX, AHT20_BAUDRATE);

    osTimerId_t periodic_tid = osTimerNew(cb_timeout_periodic, osTimerPeriodic, NULL, NULL);
        if (periodic_tid == NULL) {
        printf("[Timer Test] osTimerNew(periodic timer) failed.\r\n");
        return;
        } 
        else {
        printf("[Timer Test] osTimerNew(periodic timer) success, tid: %p.\r\n", periodic_tid);
        }

    osStatus_t status;

while(1){
    printf("my_request:%s\n",my_request);
    //int temp = strcmp(vlp1.plate, my_request);
    int temp = strcmp(mytype, my_request);
    //int temp = strcmp(PLATE1[0], my_request)||strcmp(PLATE1[1], my_request);
    printf("1111vlp1.plate:%s\n",vlp1.plate);
    printf("++++%d\n",temp);
    printf("mytype:%s\n",mytype);
    printf("[Timer Test] times:%d.\r\n", vlp1.parktime);
    
    if(temp!=0){
        //strcpy(vlp1.plate, my_request);
        strcpy(mytype, my_request);
        //判断是否可以使用充电桩
        int i;
        if(strcmp(mytype, PLATE1[0])==0){i=0;flag=1; strcpy(vlp1.plate, PLATE2[0]);vlp1.parktime = 0;left=1;}
        //if(strcmp(mytype, PLATE1[0])||strcmp(mytype, PLATE1[1])==0){i=0;flag=1; strcpy(vlp1.plate, PLATE2[0]);vlp1.parktime = 0;left=1;}
        //else if(strcmp(mytype, PLATE1[2])||strcmp(mytype, PLATE1[3])==0){i=1;flag=1; strcpy(vlp1.plate, PLATE2[1]);vlp1.parktime = 0;left=1;}
        else if(strcmp(mytype, PLATE1[1])==0){i=1;flag=1; strcpy(vlp1.plate, PLATE2[1]);vlp1.parktime = 0;left=1;}
        else if(strcmp(mytype, PLATE1[2])==0){i=2;flag=1; strcpy(vlp1.plate, PLATE2[2]);vlp1.parktime = 0;left=1;}
        else if(strcmp(mytype, PLATE1[3])==0){i=3;flag=1; strcpy(vlp1.plate, PLATE2[3]);vlp1.parktime = 0;left=1;}
        else if(strcmp(mytype, PLATE1[4])==0){i=4;flag=1; strcpy(vlp1.plate, PLATE2[4]);vlp1.parktime = 0;left=1;}
        else if(strcmp(mytype, PLATE1[5])==0){i=5;flag=1; strcpy(vlp1.plate, PLATE2[5]);vlp1.parktime = 0;left=1;}
        //else{flag=0;gpio_flag[0]=1;gpio_flag[1]=1;gpio_flag[2]=1;gpio_flag[3]=1;gpio_flag[4]=1;gpio_flag[5]=1;}
        else{flag=0;gpio_flag[0]=0;gpio_flag[1]=0;gpio_flag[2]=0;gpio_flag[3]=0;gpio_flag[4]=0;gpio_flag[5]=0;}
        //if(strcmp(vlp1.plate, PLATE[3])==0){i=3;flag=1; }
        
        //if(strcmp(mytype, PLATE[i])==0){
        
                        //flag==1表明该车可以使用充电桩
            if(flag){
                //strcpy(vlp1.plate, PLATE[i]);
                vlp1.gpio = i+1;
                gpio_flag[i]=1;
                printf("2222%s\n",vlp1.plate);
            }
        //for(int i=0;i<plate_size||flag==1;i++){
        //    if(strcmp(mytype, PLATE[i])==0){
        //        flag=1;         //flag==1表明该车可以使用充电桩
                //建立车牌信息
        //        strcpy(vlp1.plate, PLATE[i]);
        //        printf("vvvvvvvvv%s\n",vlp1.plate);
                //vlp1.gpio = i+1;
                //gpio_flag[i]=1;
                //Station = i+1;
                //分配充电口
                /*
                for (int j = 0; j < gpio_size; ){
                    if (gpio_flag[j]==1){j++; }
                    else{
                        switch(j)
                            case 0:
                                strcpy(vlp1.plate, PLATE[j]);
                                vlp1.gpio = j;
                                gpio_flag[j]=1;
                            case 1:
                                strcpy(vlp2.plate, PLATE[j]);
                                vlp2.gpio = j;
                                gpio_flag[j]=1;
                            case 2:
                                strcpy(vlp3.plate, PLATE[j]);
                                vlp3.gpio = j;
                                gpio_flag[j]=1;
                            case 3:
                                strcpy(vlp4.plate, PLATE[j]);
                                vlp4.gpio = j;
                                gpio_flag[j]=1;
                            default:
                                printf("error\n");
                    }
                } 
                
            }
        }
        */
        //可以使用充电桩
        if(flag){
            //屏幕显示
            //strcpy(mytype,vlp1.plate);
            printf("3333%s\n",vlp1.plate);
            
            OledFillScreen(0);
            IoTI2cInit(AHT20_I2C_IDX, AHT20_BAUDRATE);
            OledShowString(20, 1, vlp1.plate, 2);        //屏幕20列1行显示8*16(2行),显示车牌号
            printf("44444444444444\n");
            char stion[10]="Station:";
            //stion[8] = 1+'0';
            stion[8] = vlp1.gpio+'0';
            OledShowString(20, 6, stion, 1);  // 屏幕第20列6行显示6*8(1行),显示充电口号码
            printf("55555555555555555\n");

            status = osTimerStart(periodic_tid, 100);
                if (status != osOK) {
                    printf("[Timer Test] osTimerStart(periodic timer) failed.\r\n");
                    return;
                } else {
                    printf("[Timer Test] osTimerStart(periodic timer) success, wait a while and stop.\r\n");
                }

        }
        //不可以使用充电桩，flag==0
        else{
            
            //OledFillScreen(0);
            IoTI2cInit(AHT20_I2C_IDX, AHT20_BAUDRATE);
            
            if(left){
                OledShowString(20, 1, vlp1.plate, 2);        //屏幕20列1行显示8*16(2行),显示车牌号
                char pt[]="Time:";
                char pt1[15];
                sprintf(pt1, "%d" , vlp1.parktime);
                strcat(pt,pt1);
                OledShowString(20, 4, pt, 1);        //屏幕20列4行显示6*8(1行) 
                vlp1.cost=vlp1.parktime/60;
                char co[]="Cost:";
                char co1[15];
                sprintf(co1, "%6.2f" , vlp1.cost);
                strcat(co,co1);
                OledShowString(20, 6, co, 1);        //屏幕20列6行显示6*8(1行) 
                left=0;
            }else{
                OledShowString(20, 2, mytype, 1);        //屏幕20列2行显示6*8(1行) 
                OledShowString(0, 4, "Wrong parking space", 1);        //屏幕0列4行显示6*8(1行) 
            }
            printf("[Timer Test] times:%d.\r\n", vlp1.parktime);
            //osDelay(OS_DELAY);
            status = osTimerStop(periodic_tid);
            printf("[Timer Test] stop periodic timer, status :%d.\r\n", status);
            status = osTimerDelete(periodic_tid);
            printf("[Timer Test] kill periodic timer, status :%d.\r\n", status);
            periodic_tid = osTimerNew(cb_timeout_periodic, osTimerPeriodic, NULL, NULL);
                if (periodic_tid == NULL) {
                printf("[Timer Test] osTimerNew(periodic timer) failed.\r\n");
                return;
                } 
                else {
                printf("[Timer Test] osTimerNew(periodic timer) success, tid: %p.\r\n", periodic_tid);
                }
        }
    }
    usleep(100001);
    //OledShowString(20, 1, "efA.D19402", 2); // 屏幕20列1行显示8*16(2行) 
    //OledShowString(20, 1, "!\"A.D19402", 2); // 屏幕20列1行显示8*16(2行) 
    //OledShowString(20, 4, "TIME:60min", 1);  // 屏幕第20列4行显示6*8(1行) 
    //OledShowString(20, 5, "COST:5yuan", 1);  // 屏幕第20列5行显示6*8(1行) 
    }
    
    
}

static void OledDemo(void)
{
    osThreadAttr_t attr;
    attr.name = "OledmentTask";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = 40960; /* 任务大小4096 */
    attr.priority = osPriorityNormal;

    if (osThreadNew(OledmentTask, NULL, &attr) == NULL) {
        printf("[OledDemo] Falied to create OledmentTask!\n");
    }
}

APP_FEATURE_INIT(OledDemo);