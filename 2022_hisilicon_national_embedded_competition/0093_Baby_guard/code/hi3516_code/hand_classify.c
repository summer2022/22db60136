/*
 * Copyright (c) 2022 HiSilicon (Shanghai) Technologies CO., LIMITED.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>

#include "sample_comm_nnie.h"
#include "sample_media_ai.h"
#include "ai_infer_process.h"
#include "yolov2_hand_detect.h"
#include "vgs_img.h"
#include "ive_img.h"
#include "misc_util.h"
#include "hisignalling.h"
#include "cJSON.h"
#include "electronic_fence.h"

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* End of #ifdef __cplusplus */

#define HAND_FRM_WIDTH     640
#define HAND_FRM_HEIGHT    384
#define DETECT_OBJ_MAX     32
#define RET_NUM_MAX        3
#define DRAW_RETC_THICK    2    // Draw the width of the line
#define DRAW_RETC_MID      4    // Draw the width of the line
#define WIDTH_LIMIT        32
#define HEIGHT_LIMIT       32
#define IMAGE_WIDTH        224  // The resolution of the model IMAGE sent to the classification is 224*224
#define IMAGE_HEIGHT       224
#define MODEL_FILE_GESTURE    "/userdata/models/hand_classify/fenlei2.wk" // darknet framework wk model

static int biggestBoxIndex;
static IVE_IMAGE_S img;
static DetectObjInfo objs[DETECT_OBJ_MAX] = {0};
//static RectBox boxs[DETECT_OBJ_MAX] = {0};
static RectBox objBoxs[DETECT_OBJ_MAX] = {0};
static RectBox filteredBoxs[DETECT_OBJ_MAX] = {0};
static DetectObjInfo filteredHead[5] = {0};
static DetectObjInfo filteredBody[5] = {0};
static RectBox remainingBoxs[DETECT_OBJ_MAX] = {0};
static RectBox testBox;
static RectBox fenceBox;
static RectBox cnnBoxs[DETECT_OBJ_MAX] = {0}; // Store the results of the classification network
static RecogNumInfo numInfo[RET_NUM_MAX] = {0};
static HI_S32 filteredObjNum[2] = {0};//筛选后的obj数量
static IVE_IMAGE_S imgIn;
static IVE_IMAGE_S imgDst;
static VIDEO_FRAME_INFO_S frmIn;
static VIDEO_FRAME_INFO_S frmDst;
static char test_string[20]={'i','a','m','i','r','o','n','m','a','n'};
//static char json_data[150]={0};
int uartFd = 0;
static int fps = 0;
static int tick = 0;
char temp_[40];

//将整型数字转为字符串后直接存入上面的temp_全局变量中，第一个参数为整型数字，第二个为进制基数
void itoa(int num, int radix)
{
	 /* 索引表 */
    char index[]="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    unsigned unum; /* 中间变量 */
    int i=0,j,k;
    /* 确定unum的值 */
    if(radix==10&&num<0) /* 十进制负数 */
    {
        unum=(unsigned)-num;
        temp_[i++]='-';
    }
    else unum=(unsigned)num; /* 其它情况 */
    /* 逆序 */
    do
    {
        temp_[i++]=index[unum%(unsigned)radix];
        unum/=radix;
    }while(unum);
    temp_[i]='\0';
    /* 转换 */
    if(temp_[0]=='-') k=1; /* 十进制负数 */
    else k=0;
    /* 将原来的“/2”改为“/2.0”，保证当num在16~255之间，radix等于16时，也能得到正确结果 */
    char temp;
    for(j=k;j<=(i-k-1)/2.0;j++)
    {
        temp=temp_[j];
        temp_[j]=temp_[i-j-1];
        temp_[i-j-1]=temp;
    }
}

HI_S32 Yolo2HandDetectResnetClassifyLoad(uintptr_t* model)
{
    SAMPLE_SVP_NNIE_CFG_S *self = NULL;
    HI_S32 ret;

    ret = CnnCreate(&self, MODEL_FILE_GESTURE);
    *model = ret < 0 ? 0 : (uintptr_t)self;
    HandDetectInit(); // Initialize the hand detection model
    SAMPLE_PRT("Load hand detect claasify model success\n");
    /* uart open init */
    uartFd = UartOpenInit();
    if (uartFd < 0) {
        printf("uart1 open failed\r\n");
    } else {
        printf("uart1 open successed\r\n");
    }
    return ret;
}

HI_S32 Yolo2HandDetectResnetClassifyUnload(uintptr_t model)
{
    CnnDestroy((SAMPLE_SVP_NNIE_CFG_S*)model);
    HandDetectExit(); // Uninitialize the hand detection model
    SAMPLE_PRT("Unload hand detect claasify model success\n");

    return 0;
}

/* Get the maximum hand */
static HI_S32 GetBiggestHandIndex(RectBox boxs[], int detectNum)
{
    HI_S32 handIndex = 0;
    HI_S32 biggestBoxIndex = handIndex;
    HI_S32 biggestBoxWidth = boxs[handIndex].xmax - boxs[handIndex].xmin + 1;
    HI_S32 biggestBoxHeight = boxs[handIndex].ymax - boxs[handIndex].ymin + 1;
    HI_S32 biggestBoxArea = biggestBoxWidth * biggestBoxHeight;

    for (handIndex = 1; handIndex < detectNum; handIndex++) {
        HI_S32 boxWidth = boxs[handIndex].xmax - boxs[handIndex].xmin + 1;
        HI_S32 boxHeight = boxs[handIndex].ymax - boxs[handIndex].ymin + 1;
        HI_S32 boxArea = boxWidth * boxHeight;
        if (biggestBoxArea < boxArea) {
            biggestBoxArea = boxArea;
            biggestBoxIndex = handIndex;
        }
        biggestBoxWidth = boxs[biggestBoxIndex].xmax - boxs[biggestBoxIndex].xmin + 1;
        biggestBoxHeight = boxs[biggestBoxIndex].ymax - boxs[biggestBoxIndex].ymin + 1;
    }

    if ((biggestBoxWidth == 1) || (biggestBoxHeight == 1) || (detectNum == 0)) {
        biggestBoxIndex = -1;
    }

    return biggestBoxIndex;
}
/*拿到单目标筛选后的盒子*/
static  Filter(DetectObjInfo boxsWithClassAndScore[], int detectNum)
{
    HI_S32 i = 0;
    HI_S32 baby_headNum = 0;
    HI_S32 baby_bodyNum = 0;
    float lastScore = 0;
    float lastScore2 = 0;
    for(i = 0; i < detectNum; i++){
        if(boxsWithClassAndScore[i].cls == 1){
            baby_headNum++;//class标签为1.则baby_head的框数量+1         
            if(baby_headNum >= 2){
                if(boxsWithClassAndScore[i].score > lastScore){
                    lastScore = boxsWithClassAndScore[i].score;
                    filteredBoxs[0] = boxsWithClassAndScore[i].box;//如果不是第一次，比较概率值后决定是否覆盖
                }
            }
            else{
                filteredBoxs[0] = boxsWithClassAndScore[i].box;//如果是第一次，直接赋值，存储概率
                lastScore = boxsWithClassAndScore[i].score;
            }
        }
        if(boxsWithClassAndScore[i].cls == 2){
            baby_bodyNum++;//class标签为1.则baby_head的框数量+1         
            if(baby_bodyNum >= 2){
                if(boxsWithClassAndScore[i].score > lastScore2){
                    lastScore2 = boxsWithClassAndScore[i].score;
                    filteredBoxs[1] = boxsWithClassAndScore[i].box;//如果不是第一次，比较概率值后决定是否覆盖
                }
            }
            else{
                filteredBoxs[1] = boxsWithClassAndScore[i].box;//如果是第一次，直接赋值，存储概率
                lastScore2 = boxsWithClassAndScore[i].score;
            }
        }
    }
    if(lastScore != 0)
        filteredObjNum[0] = 1;
    else
        filteredObjNum[0] = 0;
    if(lastScore2 != 0)   
        filteredObjNum[1] = 1;
    else
        filteredObjNum[1] = 0;
}
/****update****/
/*将boxsWithClassAndScore排序，从小到大*/
static sort(DetectObjInfo *boxsWithClassAndScore, int babies)
{
    int i = 0;
    int j = 0;
    DetectObjInfo temp;
    for(i = 0; i < babies-1; i++){
        for(j = 0; j < babies-1-i; j++){
            if(boxsWithClassAndScore[j].score > boxsWithClassAndScore[j+1].score){
                temp = boxsWithClassAndScore[j];
                boxsWithClassAndScore[j] = boxsWithClassAndScore[j+1];
                boxsWithClassAndScore[j+1] = temp;
            }
        }
    }
}

static FilterV2(DetectObjInfo boxsWithClassAndScore[], int detectNum, int babies)
{
    HI_S32 i = 0;
    HI_S32 baby_headNum = 0;
    HI_S32 baby_bodyNum = 0;
    for(i = 0; i < detectNum; i++){
        if(boxsWithClassAndScore[i].cls == 1){
            baby_headNum++;//class标签为1.则baby_head的框数量+1         
            if(baby_headNum > babies){//当头部数量大于需求值时，进行筛选
                if(boxsWithClassAndScore[i].score > filteredHead[0].score){//比最小值大，满足插入条件
                    filteredHead[0] = boxsWithClassAndScore[i];//替换最小值
                    sort(filteredHead,babies);//重新排序
                }
            }
            else{//当前检测数量未到需求，记当前已有baby_headNum
                filteredHead[baby_headNum-1] = boxsWithClassAndScore[i];//直接赋值，存储概率,box,cls,存储在0,1,2...中
                if(baby_headNum==babies){//验证一下此时是否已存满，若存满则执行排序,从小到大
                    sort(filteredHead,babies);
                }
            }
        }
        if(boxsWithClassAndScore[i].cls == 2){
            baby_bodyNum++;//class标签为1.则baby_head的框数量+1         
            if(baby_bodyNum > babies){
                if(boxsWithClassAndScore[i].score > filteredBody[0].score){
                    filteredBody[0] = boxsWithClassAndScore[i];//替换最小值
                    sort(filteredBody,babies);//重新排序
                }
            }
            else{
                filteredBody[baby_bodyNum-1] = boxsWithClassAndScore[i];//直接赋值，存储概率,box,cls,存储在0,1,2...中
                if(baby_bodyNum==babies){//验证一下此时是否已存满，若存满则执行排序,从小到大
                    sort(filteredBody,babies);
                }
            }
        }
     
    }
    //标记规则 如果计数值大于应有的婴儿数，则进行了筛选，返回婴儿数，否则未进行筛选，返回计数值
    filteredObjNum[0] = baby_headNum > babies ? babies :  baby_headNum;
    filteredObjNum[1] = baby_bodyNum > babies ? babies :  baby_bodyNum;
}

/*数据处理逻辑函数，得到三个预测值，顺序为 awake,asleep,background
返回最可能的类型*/
int getBestPred(int awake, int asleep, int background){
    return 0;
}
extern int uartFd;
extern int borderFrame[4];
extern int babyNumber;
HI_S32 Yolo2HandDetectResnetClassifyCal(uintptr_t model, VIDEO_FRAME_INFO_S *srcFrm, VIDEO_FRAME_INFO_S *dstFrm)
{
    SAMPLE_SVP_NNIE_CFG_S *self = (SAMPLE_SVP_NNIE_CFG_S*)model;
    HI_S32 resLen = 0;
    int objNum,i,j,i_back;
    int temp[3]={0};
    int defaultNum = -1;
    int ret;
    int classCase = 0;
    char catReuslt[350]="";
    
    ret = FrmToOrigImg((VIDEO_FRAME_INFO_S*)srcFrm, &img);
    SAMPLE_CHECK_EXPR_RET(ret != HI_SUCCESS, ret, "hand detect for YUV Frm to Img FAIL, ret=%#x\n", ret);

    objNum = HandDetectCal(&img, objs); // Send IMG to the detection net for reasoning
    //Filter(objs, objNum);//把objs里的信息筛选出两个boxs，每次会刷新filteredObjNum的值
    /******update*****/
    
    FilterV2(objs, objNum,babyNumber);
    
    /*if(filteredObjNum[0]){
        cnnBoxs[0] = filteredBoxs[0];
        RectBoxTran(&filteredBoxs[0], HAND_FRM_WIDTH, HAND_FRM_HEIGHT,
            dstFrm->stVFrame.u32Width, dstFrm->stVFrame.u32Height);
        //cJSON_AddNumberToObject(pred,"head_left_x",filteredBoxs[0].xmin);
        itoa(filteredBoxs[0].xmin, 10);
        strcat(catReuslt, temp_);
        strcat(catReuslt, ",");
        //cJSON_AddNumberToObject(pred,"head_left_y",filteredBoxs[0].ymin);
        itoa(filteredBoxs[0].ymin, 10);
        strcat(catReuslt, temp_);
        strcat(catReuslt, ",");
        //cJSON_AddNumberToObject(pred,"head_right_x",filteredBoxs[0].xmax);
        itoa(filteredBoxs[0].xmax, 10);
        strcat(catReuslt, temp_);
        strcat(catReuslt, ",");
        //cJSON_AddNumberToObject(pred,"head_right_y",filteredBoxs[0].ymax);
        itoa(filteredBoxs[0].ymax, 10);
        strcat(catReuslt, temp_);
        strcat(catReuslt, ",");
    }//如果有head，那么box0一定有head
    /*************update V2*********************/

    for(i = 0; i < filteredObjNum[0]; i++){//循环遍历每个检测结果,注意在filteredObjNum内才为有效值
        cnnBoxs[i] = filteredHead[i].box;
        RectBoxTran(&filteredHead[i].box, HAND_FRM_WIDTH, HAND_FRM_HEIGHT,
            dstFrm->stVFrame.u32Width, dstFrm->stVFrame.u32Height);
        itoa(filteredHead[i].box.xmin, 10);
        strcat(catReuslt, temp_);
        strcat(catReuslt, ",");
        itoa(filteredHead[i].box.ymin, 10);
        strcat(catReuslt, temp_);
        strcat(catReuslt, ",");
        itoa(filteredHead[i].box.xmax, 10);
        strcat(catReuslt, temp_);
        strcat(catReuslt, ",");
        itoa(filteredHead[i].box.ymax, 10);
        strcat(catReuslt, temp_);
        strcat(catReuslt, ",");
        //SAMPLE_PRT("head:%d,%d\n",(filteredHead[i].box.xmin + filteredHead[i].box.xmax)/2, (filteredHead[i].box.ymin + filteredHead[i].box.ymax)/2);
    }
    for(i = 0; i < babyNumber - filteredObjNum[0]; i++)
        for(int cnt = 0; cnt < 4; cnt++)
            strcat(catReuslt, "-1,");
    strcat(catReuslt, ";");//数据类型结束
     
    
    /*else{
        for(int cnt = 0; cnt < 4; cnt++)strcat(catReuslt, "-1,");
    }*/
    /*if(filteredObjNum[1]){
        cnnBoxs[1] = objs[1].box;
        RectBoxTran(&filteredBoxs[1], HAND_FRM_WIDTH, HAND_FRM_HEIGHT,
            dstFrm->stVFrame.u32Width, dstFrm->stVFrame.u32Height);
        itoa(filteredBoxs[1].xmin, 10);
        strcat(catReuslt, temp_);
        strcat(catReuslt, ",");
        itoa(filteredBoxs[1].ymin, 10);
        strcat(catReuslt, temp_);
        strcat(catReuslt, ",");
        itoa(filteredBoxs[1].xmax, 10);
        strcat(catReuslt, temp_);
        strcat(catReuslt, ",");
        itoa(filteredBoxs[1].ymax, 10);
        strcat(catReuslt, temp_);
        strcat(catReuslt, ",");
        //pred = strcat(pred, str2[3]);
    }*///如果有body

    for(i = 0; i < filteredObjNum[1]; i++){//循环遍历每个检测结果,注意在filteredObjNum内才为有效值
        RectBoxTran(&filteredBody[i].box, HAND_FRM_WIDTH, HAND_FRM_HEIGHT,
            dstFrm->stVFrame.u32Width, dstFrm->stVFrame.u32Height);
        itoa(filteredBody[i].box.xmin, 10);
        strcat(catReuslt, temp_);
        strcat(catReuslt, ",");
        itoa(filteredBody[i].box.ymin, 10);
        strcat(catReuslt, temp_);
        strcat(catReuslt, ",");
        itoa(filteredBody[i].box.xmax, 10);
        strcat(catReuslt, temp_);
        strcat(catReuslt, ",");
        itoa(filteredBody[i].box.ymax, 10);
        strcat(catReuslt, temp_);
        strcat(catReuslt, ",");
        //SAMPLE_PRT("body:%d,%d\n",(filteredBody[i].box.xmin + filteredBody[i].box.xmax)/2, (filteredBody[i].box.ymin + filteredBody[i].box.ymax)/2);
    }
    for(i = 0; i < babyNumber - filteredObjNum[1]; i++)
        for(int cnt = 0; cnt < 4; cnt++)
            strcat(catReuslt, "-1,");
    strcat(catReuslt, ";");//数据类型结束
  
    
    
    // When an object is detected, a rectangle is drawn in the DSTFRM
    if (filteredObjNum[0] + filteredObjNum[1] >= 0) {
        //SAMPLE_PRT("filteredObjNum:%d\n",filteredObjNum);//在这里测试一下检测出来的物体值
        
        //char *json_data = cJSON_Print(pred);	//JSON数据结构转换为JSON字符串
        /*间隔发送区代码*/
        /*帧率及其他测试*/
        /*testBox.xmin = 100;
        testBox.xmax = 800;
        testBox.ymin = 100;
        testBox.ymax = 800;
        SAMPLE_PRT("center_x:%d,center_y:%d\n",(filteredBoxs[1].xmin+filteredBoxs[1].xmax)/2, (filteredBoxs[1].ymin+filteredBoxs[1].ymax)/2);
        if(fenceJudge(100, 100, 800, 800, (filteredBoxs[1].xmin+filteredBoxs[1].xmax)/2, (filteredBoxs[1].ymin+filteredBoxs[1].ymax)/2))
            SAMPLE_PRT("out of line!!!\n");
        //test
        //SAMPLE_PRT("string:%s\n",json_data);
        //fps++;
        //SAMPLE_PRT("fps:%d\n",fps);
        MppFrmDrawRects(dstFrm, &testBox, 1, RGB888_RED, DRAW_RETC_MID);*/
        fenceBox.xmin = borderFrame[0];
        fenceBox.ymin = borderFrame[1];
        fenceBox.xmax = borderFrame[2];
        fenceBox.ymax = borderFrame[3];
      

        // Crop the image to classification network
        /*********update V2************/

        for(i = 0; i < filteredObjNum[0]; i++)
        {
            ret = ImgYuvCrop(&img, &imgIn, &cnnBoxs[i]);
            SAMPLE_CHECK_EXPR_RET(ret < 0, ret, "ImgYuvCrop FAIL, ret=%#x\n", ret);
            if ((imgIn.u32Width >= WIDTH_LIMIT) && (imgIn.u32Height >= HEIGHT_LIMIT)) {
                COMPRESS_MODE_E enCompressMode = srcFrm->stVFrame.enCompressMode;
                ret = OrigImgToFrm(&imgIn, &frmIn);
                frmIn.stVFrame.enCompressMode = enCompressMode;
                SAMPLE_PRT("crop u32Width = %d, img.u32Height = %d\n", imgIn.u32Width, imgIn.u32Height);
                ret = MppFrmResize(&frmIn, &frmDst, IMAGE_WIDTH, IMAGE_HEIGHT);
                ret = FrmToOrigImg(&frmDst, &imgDst);
                ret = CnnCalU8c1Img(self,  &imgDst, numInfo, sizeof(numInfo) / sizeof((numInfo)[0]), &resLen);
                SAMPLE_CHECK_EXPR_RET(ret < 0, ret, "CnnCalU8c1Img FAIL, ret=%#x\n", ret);
                SAMPLE_PRT("resLen:%d\n",resLen);
                HI_ASSERT(resLen <= sizeof(numInfo) / sizeof(numInfo[0]));
                SAMPLE_PRT("class:%d,score:%d\n", numInfo[0].num, numInfo[0].score);
                SAMPLE_PRT("class:%d,score:%d\n",numInfo[1].num, numInfo[1].score);
                SAMPLE_PRT("class:%d,score:%d\n",numInfo[2].num, numInfo[2].score);
                for(i_back = 0;i_back < 3;i_back++){
                    for(j = 0;j < 3;j++){
                        if(numInfo[j].num==i_back){
                            temp[i_back]=numInfo[j].score;
                            break;
                        }
                    }
                }
                for(i_back = 0;i_back < 3;i_back++){
                    itoa(temp[i_back], 10);
                    strcat(catReuslt, temp_);
                    strcat(catReuslt, ",");
                }
                    
                MppFrmDestroy(&frmDst);
            }
            else{
                for(int cnt = 0; cnt < 3; cnt++)
                    strcat(catReuslt, "-1,");
            }
            IveImgDestroy(&imgIn);

            /*ret = ImgYuvCrop(&img, &imgIn, &cnnBoxs[0]);
            SAMPLE_CHECK_EXPR_RET(ret < 0, ret, "ImgYuvCrop FAIL, ret=%#x\n", ret);
            if ((imgIn.u32Width >= WIDTH_LIMIT) && (imgIn.u32Height >= HEIGHT_LIMIT)) {
            COMPRESS_MODE_E enCompressMode = srcFrm->stVFrame.enCompressMode;
            ret = OrigImgToFrm(&imgIn, &frmIn);
            frmIn.stVFrame.enCompressMode = enCompressMode;
            SAMPLE_PRT("crop u32Width = %d, img.u32Height = %d\n", imgIn.u32Width, imgIn.u32Height);
            ret = MppFrmResize(&frmIn, &frmDst, IMAGE_WIDTH, IMAGE_HEIGHT);
            ret = FrmToOrigImg(&frmDst, &imgDst);
            ret = CnnCalU8c1Img(self,  &imgDst, numInfo, sizeof(numInfo) / sizeof((numInfo)[0]), &resLen);
            SAMPLE_CHECK_EXPR_RET(ret < 0, ret, "CnnCalU8c1Img FAIL, ret=%#x\n", ret);
            SAMPLE_PRT("resLen:%d\n",resLen);
            HI_ASSERT(resLen <= sizeof(numInfo) / sizeof(numInfo[0]));
            SAMPLE_PRT("class:%d,score:%d\n", numInfo[0].num, numInfo[0].score);
            SAMPLE_PRT("class:%d,score:%d\n",numInfo[1].num, numInfo[1].score);
            SAMPLE_PRT("class:%d,score:%d\n",numInfo[2].num, numInfo[2].score);
            for(i = 0;i < 3;i++){
                for(j = 0;j < 3;j++){
                    if(numInfo[j].num==i){
                        temp[i]=numInfo[j].score;
                        break;
                    }
                }
            }
            for(i = 0;i < 3;i++){
                itoa(temp[i], 10);
                strcat(catReuslt, temp_);
                strcat(catReuslt, ",");
            }
            
            MppFrmDestroy(&frmDst);
            }
            IveImgDestroy(&imgIn);*/
            

        }
        for(i = 0; i < babyNumber - filteredObjNum[0]; i++)
            for(int cnt = 0; cnt < 3; cnt++)
                strcat(catReuslt, "-1,");
    //strcat(catReuslt, ";");//数据类型结束
//没有检测到头部 八个坐标数据+class0、1、2
    UartSendRead(uartFd, 0, catReuslt, strlen(catReuslt));
    SAMPLE_PRT("len:%d\n",strlen(catReuslt));
    //SAMPLE_PRT("babynum:%d\n",babyNumber);
    sprintf(catReuslt, "");
    for(i = 0; i < filteredObjNum[1]; i++){
        MppFrmDrawRects(dstFrm, &filteredBody[i].box, 1, RGB888_RED, DRAW_RETC_MID);
    }
    for(i = 0; i < filteredObjNum[0]; i++){
        MppFrmDrawRects(dstFrm, &filteredHead[i].box, 1, RGB888_GREEN, DRAW_RETC_MID);
    }
    MppFrmDrawRects(dstFrm, &fenceBox, 1, RGB888_BLUE, DRAW_RETC_MID);
        //MppFrmDrawRects(dstFrm, &filteredBoxs[0], filteredObjNum[0], RGB888_GREEN, DRAW_RETC_MID); // Target hand objnum is equal to 1，只检测一类
        //MppFrmDrawRects(dstFrm, &filteredBoxs[1], filteredObjNum[1], RGB888_RED, DRAW_RETC_MID);
        
    }

    return ret;
}

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* End of #ifdef __cplusplus */
