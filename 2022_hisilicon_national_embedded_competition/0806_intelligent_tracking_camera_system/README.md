作品简介：
在会议、演讲拍摄、网上直播和课堂录播时，由于被摄主体会无规则移动，通常需要人工控制摄像机追踪目标对象进行拍摄，摄像人员负担较重且对中要求较高，存在着对中误差、延迟等问题，而且很可能由于人工疏忽造成被摄主体丢失等故障。
为此，本项目旨在开发一套具有“自动对中跟踪、目标丢失找回”功能的智能追踪摄像系统，这不仅可以高效地达到对中跟踪的目的，减轻拍摄人员的压力，而且可以智能找回丢失的目标，优化拍摄效果。


code目录下包括了如下的内容：

其中ai_sample是Hi3516DV300端的运行程序，包含了串口通信和yolov2检测模型，以及语音报警提示等内容，这几个内容有逻辑有组织的进行线程式的工作


Newproject：是我们在Hi3861V100下的工程目录，其功能为接收Hi3516DV300端传来的信息，并进行处理对相应的GPIO口产生PWM信号



主要修理的模块是：
Hi3516DV300端将原来ai_sample目录下的手势检测和手势识别，修改成了仅有检测功能，掩盖了识别模型。串口通信新增了消息类型和内。


Hi3861V100端将串口通信模块代码和智能小车模块合成一个Newproject，修改配置文件，串口通信的代码，让其处理接收到的消息，然后能够调用舵机控制代码

