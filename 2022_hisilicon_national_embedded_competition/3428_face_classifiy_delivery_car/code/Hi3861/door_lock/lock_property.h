#ifndef LOCK_PROPERTY_H
#define LOCK_PROPERTY_H
typedef enum
{
    LOCK_STATUS_LOCKED,
    LOCK_STATUS_UNLOCK
}lock_status_t;

typedef enum
{
    LOCK_OPERATION_UNLOCK,
    LOCK_OPERATION_LOCK,
    LOCK_OPERATION_NO
}lock_operation_t;

typedef uint32_t lock_io_id;

typedef struct lock_class
{
    lock_status_t status;
    lock_operation_t operation;
    lock_io_id check;
    lock_io_id control;
    uint32_t (*lock)(struct lock_class *);
    uint32_t (*unlock)(struct lock_class *);
}lock_class_t;

#endif