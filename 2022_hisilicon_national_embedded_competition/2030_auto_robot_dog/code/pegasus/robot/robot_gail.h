/*
 * @Author: liu Shihao
 * @Date: 2022-06-12 20:54:47
 * @LastEditors: liu Shihao
 * @LastEditTime: 2022-06-13 19:22:38
 * @FilePath: \bearpi-hm_nano\applications\BearPi\BearPi-HM_Nano\sample\robot\include\scs15_control.h
 * @Description: 空闲中断
 * Copyright (c) 2022 by ${fzu} email: logic_fzu@outlook.com, All Rights Reserved.
 */

#ifndef __ROBOT_GAIL_H
#define __ROBOT_GAIL_H
#include <stdio.h>
#include <string.h>
#include "stdint.h"
extern struct Body_t body;
extern struct Leg_t FR_Leg;
extern struct Leg_t FL_Leg;
extern struct Leg_t BL_Leg;
extern struct Leg_t BR_Leg;


void AttitudeParse(void);
/*六轴移动测试，循环改变六个轴的位置*/
void Attitudestyle(void);//六自由度移动测试 3
/*基于IMU模块的自稳，实现自身姿态调节*/
void AttitudeIMU(void);  //自稳测试        4
/*足端XY轴轨迹规划*/

/*采用摆线进行规划
**普通摆线与改进摆线在XY方向函数相同
**主要改进在于消除Z方向的速度突变
**cycle为摆动相周期，Tm为支撑相周期，小跑（tort）步态两者相同*/

void Trot_Move(struct Leg_t * leg);   //全向移动 
void Trot_Gyrate(struct Leg_t * leg);  //原地旋转 

/*摆线轨迹规划*/
void Cycloid_Trot_Base(void);
void Cycloid_Trot_Base_Wide(void);
/*改进摆线轨迹规划*/
void Cycloid_Trot_Improve(void);
void Cycloid_Trot_Improve_Wide(void);



/*线性足端轨迹规划*/
void Linear_Trot_Move(struct Leg_t * leg);
void Linear_Trot(void);

void Linear_Trot_Gyrate(struct Leg_t * leg);
void Linear_Trot_Wide(void);

void Linear_Walk_Move(struct Leg_t * leg);
void Linear_Walk(void);


/*调整宽度侧向补偿函数*/

float  AD_Wide(float cycle);


#endif  //__SCS15_CONTROL_H



