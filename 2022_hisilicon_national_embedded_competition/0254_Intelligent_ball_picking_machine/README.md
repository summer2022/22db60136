# 基于视觉感知的智能捡球机

## 1.1设计目的

羽毛球运动人员经常需要聘请教练或使用发球机来接受指导训练。在上私教课或进行训练的过程中，对于羽毛球的拾取和整理花费了大量时间。对花费数十元的场地费和数百元的教练费，消耗大量时间完成捡球及整理工作对于消费者来说是低效的、奢侈的。针对此痛点，我们设计了一种智能捡球机。能够在人们进行训练的同时，自动拾取掉落在场地上的羽毛球并将其整理好。无论是对于训练者还是教练而言都节省了大量时间，使得训练更加高效。

## 1.2应用领域

本产品的核心功能是自动识别场地中的羽毛球，并完成对散落在场地上的羽毛球的收集及整理工作。主要面向的应用市场为羽毛球爱好者个人使用，作为羽毛球教练员的辅助工具，羽毛球场馆的设备租赁服务，各类学校羽毛球体育课程的辅助教学，专业训练人员的辅助工具等。通过智能捡球机实现对羽毛球的收集和整理，大大提高了锻炼、训练的效率。具有极高的社会应用价值。



**详情见作品设计报告**

## code 目录组成

Hi3516_code 是Hi3516DV300端的运行程序，包含了串口通信和yolov2检测算法

Hi3861_code 是Hi3861V100端的运行程序，包括TCP Server和串口接收功能

PhoneClient_code 是手机app控制端代码