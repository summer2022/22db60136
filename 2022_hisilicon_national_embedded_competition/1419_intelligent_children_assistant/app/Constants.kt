package com.tangjinghao.monitor.util

/**
 * @Author 唐靖豪
 * @Date 2022/7/12 17:58
 * @Email 762795632@qq.com
 * @Description
 */

object Constants {
    //模式选择
    const val DARK_MODE = 1
    const val LIGHT_MODE = 2
    const val CAR_RIGHT = "CAR_RIGHT"
    const val CAR_LEFT="CAR_LEFT"
    const val CAR_UP="CAR_UP"
    const val CAR_DOWN="CAR_DOWN"
    const val HAND_RIGHT = " HAND_RIGHT"
    const val HAND_LEFT="HAND_LEFT"
    const val HAND_UP="HAND_UP"
    const val HAND_DOWN="HAND_DOWN"
    const val PROGRAM_MOD="PROGRAM_MOD"
    const val MONITOR_MOD="MONITOR_MOD"
    const val X="X"
    const val L="L"
}