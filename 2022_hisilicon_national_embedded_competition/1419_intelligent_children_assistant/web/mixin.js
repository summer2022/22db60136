import { store } from "../../../store";
import { MODEL_ACTION,ELCOUNT_ACTION,CONTAININFO_ACTION,MOVETARGET_ACTION ,MOUSE_ACTION,BASHX_ACTION,BASHY_ACTION, CHOICETARGET_ACTION,FAKETARGET_ACTION} from "../../../store/actionsCreators";
import { svgComponentOption } from "../models/models";
import { getSvgWH,getTransform } from "../shared/svg-utils";
import { getTypeAndId } from "../shared/common-utils";
import { isSvgContainer } from "../shared/svg-utils";
import { findList } from "../shared/vlist-utils";
import { hideGlobalInput } from "../input-operate/change-text-utils";
import { listPush } from "../shared/vlist-utils";
import { renewWhileOption,renewJudgeOption } from "../svg-operate/options";
// import jquery from "jquery";
import * as $ from 'jquery';


/**
 * @description 混入模式，将这个可移动的组件添加一个点击事件,点击将全局的拖拽对象moveTarget赋值为这个组件
 * @param {vueComponent} refDom 组件节点
 */
export function eventMixin(refDom,value) {
  
    refDom.onmousedown = (event) => {
      console.log(7878);
      let cur = refDom;
    
     event.stopPropagation();
     event.preventDefault();
     // this.$store.state.moveTarget = this.$el;

     store.dispatch(MOVETARGET_ACTION(cur))
     

     if (isSvgContainer(cur)) {
       // 容器在点击的时候，会默认选择该容器去作为代码的容器
       store.dispatch(CHOICETARGET_ACTION(cur))
     }
     // initContainInfo.call(this);
     // -------------initContainInfo(refArr);
     // 嵌套情况下保存父容器的信息
     initContainInfo(refDom)
   
     // setMouseDownInit.call(this, event);
     // ---------setMouseDownInit(event);
     //   
     setMouseDownInit(event)

     // choiceUpdate.call(this, this.$el);
     // choiceUpdate(event.target);
     // 隐藏输入框
     hideGlobalInput();
   }

}

/**
 * @version 1.0.0
 * @description 得到父容器的宽高，存到全局store中
 */
function initContainInfo(refDom) {

      let canvasList = store.getState().canvasList;
      let resultList = findList(refDom, canvasList)

      console.log(676776,resultList,'resultList');
      

      // 当前移动的节点父容器不是画布，即存在嵌套情况
      if (resultList !== store.getState().canvasList) {
          const containVal = {
             width: getSvgWH($(refDom).parent()[0]).width,
             height: getSvgWH($(refDom).parent()[0]).height,
             x: getTransform($(refDom).parent()[0]).x,
             y: getTransform($(refDom).parent()[0]).y,
             id: $(refDom).parent()[0].getAttribute('id'),
             isUsed: false
          }
          store.dispatch(CONTAININFO_ACTION(containVal));
      }

}

/**
 * @description 左边的模板初始化
 * @param this 
 */
export function createModelMixin(refDom) {
  
  let target;
  
  // 鼠标按下左边model任意一个，获取点击的dom，向右边操作部分push一个按下的元素

      refDom.onmousedown = (event) => {
        let value = JSON.parse(refDom.getAttribute("value"))

        target = refDom;
          // 获取信息
          // let target = event.target,
          let list = store.getState().canvasList,
            { type } = getTypeAndId(target),
            { width, height } = getSvgWH(target),
            { x: targetX, y: targetY } = getTransform(target);

        
          event.preventDefault();

          // 根据点击的对象，将点击的对象的所有属性都放在$store的model属性中，然后在fake层新添一个svg进行拖拽
          const modelVal = {
              x: targetX,
              y: targetY,
              type: type,
              value: value 
          }
          
          let ID = 'el' + store.getState().elCount;   // 这里只是进行引用id，实际上还没有进行生成组件，只是提前用了，真正生成组件的是调用listPush方法，并且在上面的initMixin方法里面生成并赋值
          
          // 全局改变model的值
          store.dispatch(ELCOUNT_ACTION(store.getState().elCount+1));
          store.dispatch(MODEL_ACTION(modelVal))
       
          // 新节点
          let newItem = {
            x: targetX,
            y: targetY,
            width: width,
            height: height,
            id: ID,
            type:type,
            value: value
          };
      
          switch (type) {
            case 'judge': {
              newItem.svgOptions = Object.assign({}, renewJudgeOption);
              break;
            }
            case 'circle': {
            
              newItem.svgOptions = Object.assign({}, renewWhileOption);
              break;
            }
            case 'inOrder': {
              newItem.svgOptions = Object.assign({}, renewWhileOption);
              break;
            }
          }
      
          // 列表的插入 操作
          listPush(list, type, newItem);
          setTimeout(() => {
        
            // 设置鼠标初始化
            setMouseDownInit(event,type);
            
            // 获取操作面板的所有元素，最后面那个就是正在操作的元素
            let targetList = $('#main-svg-container>.' + type);
            let fakeTarget = $('#fake-' + type)[0];

            store.dispatch(MOVETARGET_ACTION(targetList[targetList.length - 1]));
            store.dispatch(FAKETARGET_ACTION(fakeTarget));   
          }, 0);
        }
  // }
}

/**
 * 鼠标数值(距离页面x/y)的初始化，获取正在移动目标的初始位置，分别存到全局store中
 * @param this 
 * @param event 
 */
function setMouseDownInit(event,type) {

    // let moveTarget = store.getState().moveTarget; // 记录movetarget
    const mouseVal = {
        x: event.clientX,
        y: event.clientY
    }
    store.dispatch(MOUSE_ACTION(mouseVal))
   
 
    setTimeout(() => {
      // 点击时候赋值，这是本次循环中进行赋值，但是这个store真正有赋值到的是下个事件循环的时候才能拿到。
      // 在每个元素都会混入让movetarget指向一个块，但是这个变量是在本次事件循环赋值，并不知道快慢，所以只有等到下个事件循环的时候才进行判断
      // 所以需要用setTimeout等到下个事件循环的时候进行查询
     
      if (!store.getState().moveTarget) {
        return;
      }

      let moveTarget = store.getState().moveTarget;

 
      let transform = getTransform(moveTarget);
 
      const bashXVal = transform.x;
      const bashYVal = transform.y;
      store.dispatch(BASHX_ACTION(bashXVal));
      store.dispatch(BASHY_ACTION(bashYVal));
      
    }, 0);
}

/**
 * 初始化contain含有的值
 * @description 队列表进行插入属性
 * @param {*} conObj 容器的contain属性或者根目录，即VList
 */
export function componentListMixin(conObj) {
  svgComponentOption.forEach((value) => {
      conObj[value] = [];
    });
  }