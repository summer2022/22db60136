/*
 * Copyright (c) 2022 HiSilicon (Shanghai) Technologies CO., LIMITED.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <hi_stdlib.h>
#include <hisignalling_protocol.h>
#include <hi_uart.h>
#include <app_demo_uart.h>
#include <iot_uart.h>
#include <hi_gpio.h>
#include <hi_io.h>
#include "iot_gpio_ex.h"
#include "ohos_init.h"
#include "cmsis_os2.h"
#include "ohos_init.h"
#include "cmsis_os2.h"
#include "iot_errno.h"
#include "iot_gpio.h"
#include "iot_gpio_ex.h"
#include "iot_uart.h"
#include "iot_pwm.h"
#include "string.h"
#include "stdlib.h"
#include "hi_pwm.h"
#define IOT_GPIO_SERVO1 7
#define IOT_GPIO_SERVO2 8
#include <hi_task.h>
#include <string.h>
#include <hi_wifi_api.h>
#include <hi_mux.h>
#include <hi_io.h>
#include <hi_gpio.h>
#include "iot_config.h"
#include "iot_log.h"
#include "iot_main.h"
#include "iot_profile.h"
#include "ohos_init.h"
#include "cmsis_os2.h"

/* attribute initiative to report */
#define TAKE_THE_INITIATIVE_TO_REPORT
#define ONE_SECOND                          (1000)
/* oc request id */
#define CN_COMMADN_INDEX                    "commands/request_id="
#define WECHAT_SUBSCRIBE_LIGHT              "light"
#define WECHAT_SUBSCRIBE_LIGHT_ON_STATE     "1"
#define WECHAT_SUBSCRIBE_LIGHT_OFF_STATE    "0"

#define WECHAT_SUBSCRIBE_LIGHT1              "motor"
#define WECHAT_SUBSCRIBE_LIGHT_ON_STATE1     "1"
#define WECHAT_SUBSCRIBE_LIGHT_OFF_STATE1    "0"

#define WECHAT_SUBSCRIBE_LIGHT2              "led"
#define WECHAT_SUBSCRIBE_LIGHT_ON_STATE2     "1"
#define WECHAT_SUBSCRIBE_LIGHT_OFF_STATE2    "0"

int g_ligthStatus = -1;
int g_ligthStatus1 = -1;
int g_ligthStatus2 = -1;
typedef void (*FnMsgCallBack)(hi_gpio_value val);

typedef struct FunctionCallback {
    hi_bool  stop;
    hi_u32 conLost;
    hi_u32 queueID;
    hi_u32 iotTaskID;
    FnMsgCallBack    msgCallBack;
}FunctionCallback;
FunctionCallback g_functinoCallback;

/* CPU Sleep time Set */
/*unsigned int TaskMsleep(unsigned int ms)
{
    if (ms <= 0) {
        return HI_ERR_FAILURE;
    }
    return hi_sleep((hi_u32)ms);
}
*/

/*电机控制初始化定义GPIO*/
#define GPIO0 0
#define GPIO1 1
#define GPIO3 3
#define GPIO10 10
#define GPIOFUNC 0
/*电机控制程序*/
void gpio_control1 (unsigned int gpio, IotGpioValue value)
{
    hi_io_set_func(gpio, GPIOFUNC);
    IoTGpioSetDir(gpio, IOT_GPIO_DIR_OUT);
    IoTGpioSetOutputVal(gpio, value);
}



void car_forward1(void)
{
    gpio_control1(GPIO0, IOT_GPIO_VALUE1);
    hi_udelay(1800);

    gpio_control1(GPIO0, IOT_GPIO_VALUE0);
    gpio_control1(GPIO1, IOT_GPIO_VALUE1);
    hi_udelay(1800);

    gpio_control1(GPIO1, IOT_GPIO_VALUE0);
    gpio_control1(GPIO3, IOT_GPIO_VALUE1);
    hi_udelay(1800);

    gpio_control1(GPIO3, IOT_GPIO_VALUE0);
    gpio_control1(GPIO10, IOT_GPIO_VALUE1);
    hi_udelay(1800);

    gpio_control1(GPIO10, IOT_GPIO_VALUE0);
}

void car_left1(void)
{
    gpio_control1(GPIO10, IOT_GPIO_VALUE1);
    //osDelay(1);
    hi_udelay(1800);
    gpio_control1(GPIO10, IOT_GPIO_VALUE0);
    gpio_control1(GPIO3, IOT_GPIO_VALUE1);
    //osDelay(1);
    hi_udelay(1800);
    gpio_control1(GPIO3, IOT_GPIO_VALUE0);
    gpio_control1(GPIO1, IOT_GPIO_VALUE1);
    //osDelay(1);
    hi_udelay(1800);
    gpio_control1(GPIO1, IOT_GPIO_VALUE0);
    gpio_control1(GPIO0, IOT_GPIO_VALUE1);
    //osDelay(1);
    hi_udelay(1800);
    gpio_control1(GPIO0, IOT_GPIO_VALUE0);
}

void car_right1(void)
{
    gpio_control1(GPIO0, IOT_GPIO_VALUE1);
    gpio_control1(GPIO1, IOT_GPIO_VALUE0);
    gpio_control1(GPIO3, IOT_GPIO_VALUE0);
    gpio_control1(GPIO10, IOT_GPIO_VALUE0);
}


/*电机反转*/
void Robotfan1(void)
{
    printf("start test l9110s\r\n");
    unsigned int time = 500;
    for(int i=0; i<2000; i++)
    {
        //car_forward();
        car_left1();
    }
}
/*电机正转*/
void Robotzheng1(void)
{
    printf("start test l9110s\r\n");
    unsigned int time = 500;
    for(int i=0;i<2000;i++)
    {
        //car_forward();
        car_forward1();
    }
}





static void DeviceConfigInit(hi_gpio_value val)
{
    hi_io_set_func(HI_IO_NAME_GPIO_9, HI_IO_FUNC_GPIO_9_GPIO);
    hi_gpio_set_dir(HI_GPIO_IDX_9, HI_GPIO_DIR_OUT);
    hi_gpio_set_ouput_val(HI_GPIO_IDX_9, val);
    IoTGpioInit(IOT_GPIO_SERVO1);
    IoSetFunc(IOT_GPIO_SERVO1,0);
    IoTGpioSetDir(IOT_GPIO_SERVO1,IOT_GPIO_DIR_OUT);
    IoTGpioInit(IOT_GPIO_SERVO2);
    IoSetFunc(IOT_GPIO_SERVO2,0);
    IoTGpioSetDir(IOT_GPIO_SERVO2,IOT_GPIO_DIR_OUT);
}

static int  DeviceMsgCallback(FnMsgCallBack msgCallBack)
{
    g_functinoCallback.msgCallBack = msgCallBack;
    return 0;
}

static void wechatControlDeviceMsg(hi_gpio_value val)
{
    DeviceConfigInit(val);
}

// < this is the callback function, set to the mqtt, and if any messages come, it will be called
// < The payload here is the json string
static void DemoMsgRcvCallBack(int qos, const char *topic, const char *payload)
{
    IOT_LOG_DEBUG("RCVMSG:QOS:%d TOPIC:%s PAYLOAD:%s\r\n", qos, topic, payload);
    /* 云端下发命令后，板端的操作处理 */
    if (strstr(payload, WECHAT_SUBSCRIBE_LIGHT) != NULL) {
        if (strstr(payload, WECHAT_SUBSCRIBE_LIGHT_OFF_STATE) != NULL) {
            wechatControlDeviceMsg(HI_GPIO_VALUE1);
            IoTGpioSetOutputVal(IOT_GPIO_SERVO1, IOT_GPIO_VALUE0);
            //printf(1);
            //TaskMsleep(ONE_SECOND);
            g_ligthStatus = HI_FALSE;
        } else {
            wechatControlDeviceMsg(HI_GPIO_VALUE0);
            IoTGpioSetOutputVal(IOT_GPIO_SERVO1, IOT_GPIO_VALUE1);
            //TaskMsleep(ONE_SECOND);
            g_ligthStatus = HI_TRUE;
        }
    }

    if (strstr(payload, WECHAT_SUBSCRIBE_LIGHT1) != NULL) {
        if (strstr(payload, WECHAT_SUBSCRIBE_LIGHT_OFF_STATE1) != NULL) {
            wechatControlDeviceMsg(HI_GPIO_VALUE1);
            //IoTGpioSetOutputVal(IOT_GPIO_SERVO1, IOT_GPIO_VALUE0);
            Robotzheng1();
            //printf(1);
            //TaskMsleep(ONE_SECOND);
            g_ligthStatus1 = HI_FALSE;
        } else {
            wechatControlDeviceMsg(HI_GPIO_VALUE0);
            Robotfan1();
            g_ligthStatus1 = HI_TRUE;
        }
    }

    if (strstr(payload, WECHAT_SUBSCRIBE_LIGHT2) != NULL) {
        if (strstr(payload, WECHAT_SUBSCRIBE_LIGHT_OFF_STATE2) != NULL) {
            wechatControlDeviceMsg(HI_GPIO_VALUE1);
            IoTGpioSetOutputVal(IOT_GPIO_SERVO2, IOT_GPIO_VALUE0);
            g_ligthStatus2 = HI_FALSE;
        } else {
            wechatControlDeviceMsg(HI_GPIO_VALUE0);
            IoTGpioSetOutputVal(IOT_GPIO_SERVO2, IOT_GPIO_VALUE1);
            g_ligthStatus2 = HI_TRUE;
        }
    }


    return HI_NULL;
}

/* publish sample */
hi_void IotPublishSample(void)
{
    /* reported attribute */
    WeChatProfile weChatProfile = {
        .subscribeType = "type",
        .status.subState = "state",
        .status.subReport = "reported",
        .status.reportVersion = "version",
        .status.Token = "clientToken",
        /* report motor */
        .reportAction.subDeviceActionMotor = "motor",
        .reportAction.motorActionStatus = 0, /* 0 : motor off */


        /*666666666666666666666666666666666666666666666666666666666666666666666*/
        /*report led*/
        .reportAction.subDeviceActionLed = "led",
        .reportAction.ledActionStatus = 0, /* 0 : led off */

        /* report temperature */
        .reportAction.subDeviceActionTemperature = "temperature",
        .reportAction.temperatureData = 30, /* 30 :temperature data */
        /* report humidity */
        .reportAction.subDeviceActionHumidity = "humidity",
        .reportAction.humidityActionData = 70, /* humidity data */
        /* report light_intensity */
        .reportAction.subDeviceActionLightIntensity = "light_intensity",
        .reportAction.lightIntensityActionData = 60, /* 60 : light_intensity */
    };

    /* report motor */
    if (g_ligthStatus1 == HI_TRUE) {
        weChatProfile.reportAction.subDeviceActionMotor = "motor";
        weChatProfile.reportAction.motorActionStatus = 1; /* 1: light on */
    } else if (g_ligthStatus1 == HI_FALSE) {
        weChatProfile.reportAction.subDeviceActionMotor = "motor";
        weChatProfile.reportAction.motorActionStatus = 0; /* 0: light off */
    } else {
        weChatProfile.reportAction.subDeviceActionMotor = "motor";
        weChatProfile.reportAction.motorActionStatus = 0; /* 0: light off */
    }


    /* report light */
    if (g_ligthStatus == HI_TRUE) {
        weChatProfile.reportAction.subDeviceActionLight = "light";
        weChatProfile.reportAction.lightActionStatus = 1; /* 1: light on */
    } else if (g_ligthStatus == HI_FALSE) {
        weChatProfile.reportAction.subDeviceActionLight = "light";
        weChatProfile.reportAction.lightActionStatus = 0; /* 0: light off */
    } else {
        weChatProfile.reportAction.subDeviceActionLight = "light";
        weChatProfile.reportAction.lightActionStatus = 0; /* 0: light off */
    }

    /* report led */
    if (g_ligthStatus2 == HI_TRUE) {
        weChatProfile.reportAction.subDeviceActionLed = "led";
        weChatProfile.reportAction.ledActionStatus = 1; /* 1: light on */
    } else if (g_ligthStatus2 == HI_FALSE) {
        weChatProfile.reportAction.subDeviceActionLed = "led";
        weChatProfile.reportAction.ledActionStatus = 0; /* 0: light off */
    } else {
        weChatProfile.reportAction.subDeviceActionLed = "led";
        weChatProfile.reportAction.ledActionStatus = 0; /* 0: light off */
    }


    /* profile report */
    IoTProfilePropertyReport(CONFIG_USER_ID, &weChatProfile);
}

// < this is the demo main task entry,here we will set the wifi/cjson/mqtt ready and
// < wait if any work to do in the while
static hi_void *DemoEntry(const char *arg)
{
    WifiStaReadyWait();
    cJsonInit();
    IoTMain();
    IoTGpioInit(IOT_GPIO_SERVO1);
    IoSetFunc(IOT_GPIO_SERVO1,0);
    IoTGpioSetDir(IOT_GPIO_SERVO1,IOT_GPIO_DIR_OUT);



    /* 云端下发回调 */
    IoTSetMsgCallback(DemoMsgRcvCallBack);
    /* 主动上报 */
#ifdef TAKE_THE_INITIATIVE_TO_REPORT
    while (1) {
        /* 用户可以在这调用发布函数进行发布，需要用户自己写调用函数 */
        IotPublishSample(); // 发布例程
#endif
        TaskMsleep(ONE_SECOND);
    }
    return NULL;
}

// < This is the demo entry, we create a task here,
// and all the works has been done in the demo_entry
#define CN_IOT_TASK_STACKSIZE  0x1000
#define CN_IOT_TASK_PRIOR 25
#define CN_IOT_TASK_NAME "IOTDEMO"

static void AppDemoIot(void)
{
    osThreadAttr_t attr;
    IoTWatchDogDisable();

    attr.name = "IOTDEMO";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = CN_IOT_TASK_STACKSIZE;
    attr.priority = CN_IOT_TASK_PRIOR;

    if (osThreadNew((osThreadFunc_t)DemoEntry, NULL, &attr) == NULL) {
        printf("[mqtt] Falied to create IOTDEMO!\n");
    }
}

SYS_RUN(AppDemoIot);