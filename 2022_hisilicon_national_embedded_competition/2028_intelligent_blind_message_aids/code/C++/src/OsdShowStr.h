#ifndef OSD_SHOW_STR_H_
#define OSD_SHOW_STR_H_

#include "osd_img.h"
#include "DefaultDefine.h"

namespace hiych {

//OSD字符串显示
class OSD
{
public:
    OSD(): osd(nullptr), id(-1) {}
    ~OSD() {}
    void showStr(std::string str, int x,int y, uint32_t color = RGB888_YELLOW)
    {
        HI_OSD_ATTR_S rgn;
        memset_s(&rgn, sizeof(HI_OSD_ATTR_S), 0 ,sizeof(HI_OSD_ATTR_S));
        if(osd == nullptr)
        {
            OsdLibInit();
            osd = OsdsCreate(HI_OSD_BINDMOD_VPSS, 0, 1);
            id = OsdsCreateRgn(osd);
        }
        if(x <= 0) x = 10;
        if(y <= 0) y = 10;
        TxtRgnInit(&rgn, str.c_str(), (x), (y), color); // font width and heigt use default 40
        OsdsSetRgn(osd, id, &rgn);
    }

    void clearStr()
    {
        if(osd == nullptr)
            return;
        OsdsClear(osd);
    }

private:
    OsdSet* osd;
    int id;
};

}

#endif