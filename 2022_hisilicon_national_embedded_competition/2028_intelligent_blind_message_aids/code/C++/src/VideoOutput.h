#ifndef VIDEO_OUTPUT_H_
#define VIDEO_OUTPUT_H_

#include "DefaultDefine.h"

namespace hiych {

//vo类
class VideoOutput
{
public:
    using DATA_VO_TYPE = SAMPLE_VO_CONFIG_S;
private:
    DATA_VO_TYPE* voParam;
    PIC_SIZE_E picSize;

    bool configFlag;

    void initVoParam();
public:
    DATA_VO_TYPE* getParam() const;
    bool isConfigSetComplete() const;


    VideoOutput(PIC_SIZE_E picSize);
    ~VideoOutput();
};

VideoOutput::VideoOutput(PIC_SIZE_E _picSize):
    voParam(nullptr),
    picSize(_picSize),
    configFlag(false)
{
    try
    {
        voParam = new DATA_VO_TYPE;
    }
    catch(const std::bad_alloc& e)
    {
        std::cerr << e.what() << '\n';
    }

    memset_s(this->voParam, sizeof(DATA_VO_TYPE), 0, sizeof(DATA_VO_TYPE));

    initVoParam();
}

VideoOutput::~VideoOutput()
{
    delete voParam;
}

VideoOutput::DATA_VO_TYPE* VideoOutput::getParam() const
{
    return this->voParam;
}

void VideoOutput::initVoParam()
{
    SAMPLE_COMM_VO_GetDefConfig(this->voParam);

    this->voParam->enDstDynamicRange = DYNAMIC_RANGE_SDR8;
    this->voParam->enIntfSync = VO_OUTPUT_USER;
    this->voParam->enVoIntfType = VO_INTF_MIPI;
    this->voParam->enPicSize = picSize;
    this->voParam->voChn = 0;
    this->voParam->voLayer = 0;

    configFlag = true;
}

bool VideoOutput::isConfigSetComplete() const
{
    return configFlag;
}

}

#endif