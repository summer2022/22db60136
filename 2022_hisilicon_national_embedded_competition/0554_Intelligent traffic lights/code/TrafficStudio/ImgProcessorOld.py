import cv2


class ImgProcessorOld():
    def __init__(self):
        self.pic_0 = cv2.imread("sources/detail/pic_0.png")
        self.pic_1 = cv2.imread("sources/detail/pic_1.png")
        self.pic_2 = cv2.imread("sources/detail/pic_2.png")
        self.img_background = cv2.imread("sources/detail/img_background.png")
        self.picList = [self.pic_0, self.pic_1, self.pic_2]
        self.positionList = [[0, 400], [500, 400], [400, 500], [400, 0]] # 先y再x

    def makeimg(self, congessionList):
        for i in range(4):
            position = self.positionList[i]
            congession = 0
            for j in range(2):
                congession_temp = congessionList[i*2+j]
                if congession_temp > congession:
                    congession = congession_temp
            pic = self.picList[congession]
            if i == 0:
                pic = cv2.transpose(pic)
            elif i == 1:
                pic = cv2.transpose(cv2.flip(pic, 1))
            elif i == 2:
                pic = cv2.flip(pic, 1)
            self.img_background = self.imgandpic(self.img_background, pic, position)
        return self.img_background

    def imgandpic(self, img_background, pic, position):
        shape = pic.shape
        roi = img_background[position[0]:(position[0] + shape[0]), position[1]:(position[1] + shape[1])]
        pic_gray = cv2.cvtColor(pic, cv2.COLOR_BGR2GRAY)
        _, mask = cv2.threshold(pic_gray, 200, 255, cv2.THRESH_BINARY)
        mask_inv = cv2.bitwise_not(mask)
        img_bg = cv2.bitwise_and(roi, roi, mask=mask)
        img_fg = cv2.bitwise_and(pic, pic, mask=mask_inv)
        dst = cv2.add(img_bg, img_fg)
        img_background[position[0]:(position[0] + shape[0]), position[1]:(position[1] + shape[1])] = dst
        return img_background