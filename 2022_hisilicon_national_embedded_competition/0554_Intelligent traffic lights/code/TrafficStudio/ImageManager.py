from MainWindow import Ui_MainWindow
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
import PyQt5.QtCore as QtCore
import sys
from ConfigFile import point_list, reference_size

class ImageManager(object):
    '''
    ImageManager用来管理图片绘制过程中所用到的图片，以及图片绘制的大小
    '''
    def __init__(self):
        self.__backgroundPix = QPixmap("sources/GreenBackground")  # 载入图片
        self.__littlePointPix = QPixmap("sources/littlePoint.png")
        self.__littlePointTouchPix = QPixmap("sources/littlePoint_touch.png")
        self.__littlePointClickPix = QPixmap("sources/littlePoint_click.png")
        self.__littlePointOnlinePix = QPixmap("sources/littlePoint_online.png")
        self.__littlePointTouchOnlinePix = QPixmap("sources/littlePoint_touch_online.png")

        self.__middlePointPix = QPixmap("sources/middlePoint.png")
        self.__middlePointTouchPix = QPixmap("sources/middlePoint_touch.png")
        self.__middlePointClickPix = QPixmap("sources/middlePoint_click.png")
        self.__middlePointOnlinePix = QPixmap("sources/middlePoint_online.png")
        self.__middlePointTouchOnlinePix = QPixmap("sources/middlePoint_touch_online.png")

        self.__largePointPix = QPixmap("sources/largePoint.png")
        self.__largePointTouchPix = QPixmap("sources/largePoint_touch.png")
        self.__largePointClickPix = QPixmap("sources/largePoint_click.png")
        self.__largePointOnlinePix = QPixmap("sources/largePoint_online.png")
        self.__largePointTouchOnlinePix = QPixmap("sources/largePoint_touch_online.png")

        self.__backgroundScaled = self.__backgroundPix.scaled(2650, 2650)  # 初始化缩放图
        self.size = 0
        self.iconScale()

        self.__clickFlag = [False for _ in range(len(point_list))]
        self.__touchFlag = [False for _ in range(len(point_list))]

        # __pointStatus 存储点的触碰状态：normal(0)， touch(1)，click(2)
        self.__pointStatus = [0 for _ in range(len(point_list))]
        # __pointDistance 存储点的大小信息：little(0), middle(1), large(2)
        self.__pointDistance = [0 for _ in range(len(point_list))]
        # __pointOnline 存储点的在线信息：离线(0)，在线(1)
        self.__pointOnline = [0 for _ in range(len(point_list))]
        self.rangeIndex = 0
        self.__range1LIst = [0, 0, 0, 0, 500, 900, 800, 2000, 2000, 2000, 2000, 2000]
        self.__range1 = 0
        self.__range2 = 3000

    def iconScale(self):
        self.__littlePointScaled = self.__littlePointPix.scaled(50+self.size, 50+self.size)
        self.__littlePointTouchScaled = self.__littlePointTouchPix.scaled(90+self.size, 90+self.size)
        self.__littlePointClickScaled = self.__littlePointClickPix.scaled(50+self.size, 50+self.size)
        self.__littlePointOnlineScaled = self.__littlePointOnlinePix.scaled(50+self.size, 50+self.size)
        self.__littlePointTouchOnlineScaled = self.__littlePointTouchOnlinePix.scaled(90+self.size, 90+self.size)

        self.__middlePointScaled = self.__middlePointPix.scaled(50+self.size, 50+self.size)
        self.__middlePointTouchScaled = self.__middlePointTouchPix.scaled(90+self.size, 90+self.size)
        self.__middlePointClickScaled = self.__middlePointClickPix.scaled(50+self.size, 50+self.size)
        self.__middlePointOnlineScaled = self.__middlePointOnlinePix.scaled(50+self.size, 50+self.size)
        self.__middlePointTouchOnlineScaled = self.__middlePointTouchOnlinePix.scaled(90+self.size, 90+self.size)

        self.__largePointScaled = self.__largePointPix.scaled(120+self.size, 120+self.size)
        self.__largePointTouchScaled = self.__largePointTouchPix.scaled(160+self.size, 160+self.size)
        self.__largePointClickScaled = self.__largePointClickPix.scaled(120+self.size, 120+self.size)
        self.__largePointOnlineScaled = self.__largePointOnlinePix.scaled(120+self.size, 120+self.size)
        self.__largePointTouchOnlineScaled = self.__largePointTouchOnlinePix.scaled(160+self.size, 160+self.size)

    def rescaleRange(self, direction):
        if direction == 1: # 放大
            self.rangeIndex = self.rangeIndex + 1
            self.size = self.size + 10
            self.iconScale()
        else: # 缩小
            self.rangeIndex = self.rangeIndex - 1
            self.size = self.size - 10
            self.iconScale()


    def getPointIcon(self, i):
        iconList = [[[self.__littlePointScaled, self.__littlePointTouchScaled, self.__littlePointClickScaled],
                   [self.__middlePointScaled, self.__middlePointTouchScaled, self.__middlePointClickScaled],
                   [self.__largePointScaled, self.__largePointTouchScaled, self.__largePointClickScaled]],

                    [[self.__littlePointOnlineScaled, self.__littlePointTouchOnlineScaled, self.__littlePointClickScaled],
                     [self.__middlePointOnlineScaled, self.__middlePointTouchOnlineScaled, self.__middlePointClickScaled],
                     [self.__largePointOnlineScaled, self.__largePointTouchOnlineScaled, self.__largePointClickScaled]]]

        return iconList[self.__pointOnline[i]][self.__pointDistance[i]][self.__pointStatus[i]]

    def getBackground(self):
        return self.__backgroundScaled

    def getBackgroundSize(self):
        return self.__backgroundScaled.size()

    def backgroundScale(self, multiplier, divisor):
        newWidth = int(self.__backgroundScaled.width() * multiplier / divisor)
        newHeight = int(self.__backgroundScaled.height() * multiplier / divisor)
        if (newHeight <= 9000 and newHeight > 2640) or (newWidth <= 9000 and newWidth > 2640):
            self.__backgroundScaled = self.__backgroundPix.scaled(newWidth, newHeight)
            return True
        else:
            return False

    def clickFlagFlip(self, i):
        self.__clickFlag[i] = not self.__clickFlag[i]

    def setClickFlag(self, i, boolFlag):
        self.__clickFlag[i] = boolFlag

    def checkClicked(self):
        if self.__clickFlag.count(True) == 1:
            return True
        else:
            return False

    def getClickIndex(self):
        return self.__clickFlag.index(True)

    def setTouchFlag(self, i, boolFlag):
        self.__touchFlag[i] = boolFlag

    def touchFlagNum(self):
        return self.__touchFlag.count(True)

    def touchFlagIndex(self):
        return self.__touchFlag.index(True)

    def pointOnlineUpdate(self, equipOnlineList):
        for i in equipOnlineList:
            self.__pointOnline[i] = 1

    def pointStatusUpdate(self):
        for i in range(len(self.__pointStatus)):
            if self.__touchFlag[i]:
                self.__pointStatus[i] = 1
            else:
                self.__pointStatus[i] = 0
            if self.__clickFlag[i]:
                self.__pointStatus[i] = 2

    def pointDistanceUpdate(self, viewCenter):
        viewAbstractCenter = (viewCenter[0]* reference_size/self.__backgroundScaled.width(), viewCenter[1]*reference_size/self.__backgroundScaled.height())
        for i in range(len(point_list)):
            distance = (point_list[i][0]-viewAbstractCenter[0])**2 + (point_list[i][1]-viewAbstractCenter[1])**2
            if distance < self.__range1LIst[self.rangeIndex]**2: # 近点：large
                self.__pointDistance[i] = 2
            # elif distance < self.__range2**2: # 中点，middle
            #     self.__pointDistance[i] = 1
            else: # 中点：middle
                self.__pointDistance[i] = 1


if __name__ == '__main__':
    pass