
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include "ohos_init.h"
#include "cmsis_os2.h"
#include "iot_gpio.h"
#include "hi_io.h"
#include "hi_time.h"

//#define GPIO14 14
#define GPIO2 2
//#define GPIO3 3
//#define COUNT 40
#define COUNT 40
/*
void set_angle(unsigned int duty)   //180舵机占空比
{
    unsigned int time = 20000;
    IoTGpioInit(GPIO3);
    IoTGpioSetDir(GPIO3, IOT_GPIO_DIR_OUT);
    IoTGpioSetOutputVal(GPIO3, IOT_GPIO_VALUE1);    //GPIO3高电平
    hi_udelay(duty);
    IoTGpioSetOutputVal(GPIO3, IOT_GPIO_VALUE0);
    hi_udelay(time-duty);
}

void set_rotation_speed(unsigned int duty)    //360舵机
{
    unsigned int time = 20000;
    IoTGpioInit(GPIO2);
    IoTGpioSetDir(GPIO2, IOT_GPIO_DIR_OUT);
    IoTGpioSetOutputVal(GPIO2, IOT_GPIO_VALUE1);    //GPIO2高电平
    hi_udelay(duty);
    IoTGpioSetOutputVal(GPIO2, IOT_GPIO_VALUE0);
    hi_udelay(time);
}
*/

void set_angle(unsigned int duty)   //180舵机占空比,暂用GPIO2
{
    unsigned int time = 20000;
    IoTGpioInit(GPIO2);
    IoTGpioSetDir(GPIO2, IOT_GPIO_DIR_OUT);
    IoTGpioSetOutputVal(GPIO2, IOT_GPIO_VALUE1);    //GPIO2高电平
    hi_udelay(duty);
    IoTGpioSetOutputVal(GPIO2, IOT_GPIO_VALUE0);
    hi_udelay(time-duty);
}
/*
void engine_turn_forward(void)  //自定义360度舵机正向转动，0.5-1.4ms为正向转动，其中0.5为最大转速，1.4转速为0
{
    unsigned int angle = 1380;
    printf("engine turn forward\n");
    for (int i = 0; i < COUNT; i++) {
        set_rotation_speed(angle);
    }
}

void engine_turn_reverse(void)  //自定义360度舵机反向转动,1.5以上为反向转动
{
    unsigned int angle = 1500;
    printf("engine turn reverse\n");
    for (int i = 0; i < COUNT; i++) {
        set_rotation_speed(angle);
    }
}

void engine_stop(void)  //自定义360度舵机停止转动
{
    unsigned int angle = 1400;
    printf(" engine stop\n");
    for (int i = 0; i < COUNT; i++) {
        set_rotation_speed(angle);
    }
}
*/
void engine_turn_left(void)   //自定义180度舵机左转
{
    unsigned int angle = 1300;
    printf("engine turn left\n");
    for (int i = 0; i < COUNT; i++) {
        set_angle(angle);
    }
}


void engine_middle(void)   //自定义180度舵机居中
{
    unsigned int angle = 1500;
    printf("engine middle\n");
    for (int i = 0; i < COUNT; i++) {
        set_angle(angle);
    }
}

void engine_turn_right(void)   //自定义180度舵机右转
{
    unsigned int angle = 1700;
    printf("engine turn right\n");
    for (int i = 0; i < COUNT; i++) {
        set_angle(angle);
    }
}


void engine_reco_one(void)   //自定义180度舵机
{
    unsigned int angle = 1500;
    printf("engine turn reconnoitre one\n");
    for (int i = 0; i < COUNT; i++) {
        set_angle(angle);
    }
}

void engine_reco_two(void)   //自定义180度舵机
{
    unsigned int angle = 1400;
    printf("engine turn reconnoitre two\n");
    for (int i = 0; i < COUNT; i++) {
        set_angle(angle);
    }
}

void engine_reco_three(void)   //自定义180度舵机
{
    unsigned int angle = 1300;
    printf("engine turn reconnoitre three\n");
    for (int i = 0; i < COUNT; i++) {
        set_angle(angle);
    }
}

void engine_reco_four(void)   //自定义180度舵机
{
    unsigned int angle = 1300;
    printf("engine turn reconnoitre four\n");
    for (int i = 0; i < COUNT; i++) {
        set_angle(angle);
    }
}
void engine_reco_five(void)   //自定义180度舵机
{
    unsigned int angle = 1600;
    printf("engine turn reconnoitre five\n");
    for (int i = 0; i < COUNT; i++) {
        set_angle(angle);
    }
}

void engine_reco_six(void)   //自定义180度舵机
{
    unsigned int angle = 1700;
    printf("engine turn reconnoitre six\n");
    for (int i = 0; i < COUNT; i++) {
        set_angle(angle);
    }
}
