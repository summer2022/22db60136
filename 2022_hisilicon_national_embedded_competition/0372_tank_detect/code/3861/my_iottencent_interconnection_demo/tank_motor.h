#ifndef __TANK_L9110S_H__
#define __TANK_L9110S_H__


void tank_backward(void);
void tank_forward(void);
void tank_left(void);
void tank_right(void);
void tank_stop(void);
#endif