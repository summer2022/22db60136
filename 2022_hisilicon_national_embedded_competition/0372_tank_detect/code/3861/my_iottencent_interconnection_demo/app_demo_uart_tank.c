/*
 * Copyright (c) 2022 HiSilicon (Shanghai) Technologies CO., LIMITED.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
#include <hi_stdlib.h>
#include <hisignalling_protocol.h>
#include <hi_uart.h>
#include <app_demo_uart_tank.h>
#include <iot_uart.h>
#include <hi_gpio.h>
#include <hi_io.h>
#include "iot_gpio_ex_tank.h"
#include "ohos_init.h"
#include "cmsis_os2.h"
#include "app_demo_iot.h"

UartDefConfig uartDefConfig = {0};

static void Uart1GpioCOnfig(void)
{
//#ifdef ROBOT_BOARD
    IoSetFunc(HI_IO_NAME_GPIO_5, IOT_IO_FUNC_GPIO_5_UART1_RXD);
    //IoTGpioSetDir(HI_IO_NAME_GPIO_5, IOT_GPIO_DIR_OUT);
    //IoTGpioSetPull(HI_IO_NAME_GPIO_5, IOT_GPIO_PULL_UP);
    IoSetFunc(HI_IO_NAME_GPIO_6, IOT_IO_FUNC_GPIO_6_UART1_TXD);
    //IoTGpioSetDir(HI_IO_NAME_GPIO_6, IOT_GPIO_DIR_OUT);
    //IoTGpioSetPull(HI_IO_NAME_GPIO_6, IOT_GPIO_PULL_UP);
    hi_udelay(20);
    /* IOT_BOARD */
//#elif defined (EXPANSION_BOARD)
    //IoSetFunc(HI_IO_NAME_GPIO_0, IOT_IO_FUNC_GPIO_0_UART1_TXD);

    //IoSetFunc(HI_IO_NAME_GPIO_1, IOT_IO_FUNC_GPIO_1_UART1_RXD);

//#endif


// #ifdef EXPANSION_BOARD
//     IoSetFunc(HI_IO_NAME_GPIO_0, IOT_IO_FUNC_GPIO_0_UART1_TXD);
//     IoSetFunc(HI_IO_NAME_GPIO_1, IOT_IO_FUNC_GPIO_1_UART1_RXD);
//     /* IOT_BOARD */
// #elif defined (ROBOT_BOARD)
//     IoSetFunc(HI_IO_NAME_GPIO_5, IOT_IO_FUNC_GPIO_5_UART1_RXD);
//     IoSetFunc(HI_IO_NAME_GPIO_6, IOT_IO_FUNC_GPIO_6_UART1_TXD);
// #endif
}

// unsigned int TaskMsleep(unsigned int ms)
// {
//     if (ms <= 0) {
//         return HI_ERR_FAILURE;
//     }
//     return hi_sleep((hi_u32)ms);
// }

int SetUartRecvFlag(UartRecvDef def)
{
    if (def == UART_RECV_TRUE) {
        uartDefConfig.g_uartReceiveFlag = HI_TRUE;
    } else {
        uartDefConfig.g_uartReceiveFlag = HI_FALSE;
    }
    
    return uartDefConfig.g_uartReceiveFlag;
}

int GetUartConfig(UartDefType type)
{
    int receive = 0;

    switch (type) {
        case UART_RECEIVE_FLAG:
            receive = uartDefConfig.g_uartReceiveFlag;
            break;
        case UART_RECVIVE_LEN:
            receive = uartDefConfig.g_uartLen;
            break;
        default:
            break;
    }
    return receive;
}

void ResetUartReceiveMsg(void)
{
    (void)memset_s(uartDefConfig.g_receiveUartBuff, sizeof(uartDefConfig.g_receiveUartBuff),
        0x0, sizeof(uartDefConfig.g_receiveUartBuff));
}

unsigned char *GetUartReceiveMsg(void)
{
    return uartDefConfig.g_receiveUartBuff;
}

//接收3516发来的消息
int tankflag; //定义全局变量来接收tank检测状态
int tankControlflag; //定义全局变量来接收tank检测状态 用于外设的控制
int x_min; //定义全局变量来接收tank坐标1
int y_min; //定义全局变量来接收tank坐标2
int x_max; //定义全局变量来接收tank坐标3
int y_max; //定义全局变量来接收tank坐标4
static hi_void *TankUartDemoTask(char *param)
{
    hi_u8 uartBuff[UART_BUFF_SIZE] = {0};//这里指定一个Buffer来接收3516发来的消息
    hi_unref_param(param);
    printf("Initialize uart my_iottencent_interconnection_demo successfully, please enter some datas via DEMO_UART_NUM port...\n");
    Uart1GpioCOnfig();//使用GPIO,都需要调用该接口 
    for (;;) {
        //IoTUartRead:这里就相当于去接收3516通过串口发送来的信息。然后进行一个判断
        uartDefConfig.g_uartLen = IoTUartRead(DEMO_UART_NUM, uartBuff, UART_BUFF_SIZE);
        // printf("uartBuff is :%s",uartBuff); //没有传递信息时,这里的消息是乱码。uartBuff_size=1
        if ((uartDefConfig.g_uartLen > 0) && (uartBuff[0] == 170) && (uartBuff[1] == 85)) {
            printf(" ====uart read data=== :%s \n", uartBuff); //这里直接读取的时候是一个乱码
            // printf("uartBuff[0] is :%d\n",uartBuff[0]);//自定义协议头
            // printf("uartBuff[1] is :%d\n",uartBuff[1]);//自定义协议头
            // printf("uartBuff[2] is :%d\n",uartBuff[2]);//自定义tank检测状态
            // printf("uartBuff[3] is :%d\n",uartBuff[3]);//x_min 商
            // printf("uartBuff[4] is :%d\n",uartBuff[4]);//x_min 余
            // printf("uartBuff[5] is :%d\n",uartBuff[5]);//y_min 商
            // printf("uartBuff[6] is :%d\n",uartBuff[6]);//y_min 余
            // printf("uartBuff[7] is :%d\n",uartBuff[7]);//x_max 商
            // printf("uartBuff[8] is :%d\n",uartBuff[8]);//x_max 余
            // printf("uartBuff[9] is :%d\n",uartBuff[9]);//y_max 商
            // printf("uartBuff[10] is :%d\n",uartBuff[10]);//y_max 余
            // printf("uartBuff[11] is :%d\n",uartBuff[11]);//自定义协议尾
            //坐标数据
            printf("x_min is :%d\n",uartBuff[3]*256+uartBuff[4]);
            printf("y_min is :%d\n",uartBuff[5]*256+uartBuff[6]);
            printf("x_max is :%d\n",uartBuff[7]*256+uartBuff[8]);
            printf("y_max is :%d\n",uartBuff[9]*256+uartBuff[10]);
            tankflag = uartBuff[2];//tank检测状态放到全局变量tankflag中
            printf("====app_demo_uart_tankflag=== %d \n",tankflag);
            tankControlflag = uartBuff[2];//tank检测状态放到全局变量tankControlflag中
            printf("====app_demo_uart_tankControlflag=== %d \n",tankControlflag);
            x_min = uartBuff[3]*256+uartBuff[4];
            y_min = uartBuff[5]*256+uartBuff[6];
            x_max = uartBuff[7]*256+uartBuff[8];
            y_max = uartBuff[9]*256+uartBuff[10];
            printf("===== x_min= %d,y_min= %d,x_max= %d,y_max= %d ==== \n",x_min,y_min,x_max,y_max);
        }
        tankTaskMsleep(20); /* 20:sleep 20ms */
    }
    return NULL;
}
/*
 * This demo simply shows how to read datas from UART2 port and then echo back.
 */
hi_void Tank_UartTransmit(hi_void)
{
    hi_u32 ret = 0;

    IotUartAttribute uartAttr = {
        .baudRate = 115200, /* baudRate: 115200 */
        // .baudRate = 9600, /* baudRate: 9600 */
        .dataBits = 8, /* dataBits: 8bits */
        .stopBits = 1, /* stop bit */
        .parity = 0,
    };
    /* Initialize uart driver */
    ret = IoTUartInit(DEMO_UART_NUM, &uartAttr);
    if (ret != HI_ERR_SUCCESS) {
        printf("Failed to init tankuart! Err code = %d\n", ret);
        return;
    }
    // if (ret == HI_ERR_SUCCESS) {
    //     printf("success to init tankuart! Err code = %d\n", ret);
    //     return;
    // }
    /* Create a task to handle uart communication */
    osThreadAttr_t attr = {0};
    attr.stack_size = UART_DEMO_TASK_STAK_SIZE;
    attr.priority = UART_DEMO_TASK_PRIORITY;
    // attr.priority = 25; //这里的优先级是数字越大则优先级越
    attr.name = (hi_char*)"uart demo";
    if (osThreadNew((osThreadFunc_t)TankUartDemoTask, NULL, &attr) == NULL) {
        printf("Falied to create tankuart demo task!\n");
    }
}
SYS_RUN(Tank_UartTransmit);
// APP_FEATURE_INIT(Tank_UartTransmit);