#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include "ohos_init.h"
#include "cmsis_os2.h"
#include "iot_gpio.h"
#include "hi_io.h"
#include "hi_time.h"
#include "iot_pwm.h"
#include "hi_pwm.h"

#define GPIO0 0
#define GPIO1 1
#define GPIO9 9
#define GPIO10 10
#define GPIO7 7
#define GPIO8 8
#define GPIO12 12
#define GPIO11 11
#define GPIOFUNC 0
#define PWM_FREQ_FREQUENCY  (60000)

void gpio_control_1(unsigned int  gpio, IotGpioValue value)
{
    // unsigned int duty = 19500;
    // unsigned int time = 20000;
    hi_io_set_func(gpio, GPIOFUNC);
    IoTGpioSetDir(gpio, IOT_GPIO_DIR_OUT);
    IoTGpioSetOutputVal(gpio, value);
    // hi_udelay(duty);
    // IoTGpioSetOutputVal(gpio, 0);
    // hi_udelay(time-duty);
}


void gpio_control_2(unsigned int  gpio, IotGpioValue value)
{
    // unsigned int duty = 19500;
    // unsigned int time = 20000;
    hi_io_set_func(gpio, GPIOFUNC);
    IoTGpioSetDir(gpio, IOT_GPIO_DIR_OUT);
    IoTGpioSetOutputVal(gpio, value);
    // hi_udelay(duty);
    // IoTGpioSetOutputVal(gpio, 0);
    // hi_udelay(time-duty);
}

void tank_forward(void)
{
    printf("tank forward\n");
    gpio_control_1(GPIO0, IOT_GPIO_VALUE0);
    gpio_control_1(GPIO1, IOT_GPIO_VALUE1);
    gpio_control_1(GPIO9, IOT_GPIO_VALUE1);
    gpio_control_1(GPIO10, IOT_GPIO_VALUE0);
    gpio_control_2(GPIO7, IOT_GPIO_VALUE0);
    gpio_control_2(GPIO8, IOT_GPIO_VALUE1);
    gpio_control_2(GPIO12, IOT_GPIO_VALUE1);
    gpio_control_2(GPIO11, IOT_GPIO_VALUE0);
}

// void tank_backward(void)
// {
//     printf("tank backward\n");
//     gpio_control_1(GPIO0, IOT_GPIO_VALUE1);
//     gpio_control_1(GPIO1, IOT_GPIO_VALUE0);
//     gpio_control_1(GPIO9, IOT_GPIO_VALUE0);
//     gpio_control_1(GPIO10, IOT_GPIO_VALUE1);

//     gpio_control_2(GPIO7, IOT_GPIO_VALUE1);
//     gpio_control_2(GPIO8, IOT_GPIO_VALUE0);
//     gpio_control_2(GPIO12, IOT_GPIO_VALUE0);
//     gpio_control_2(GPIO11, IOT_GPIO_VALUE1);
// }

void tank_left(void)
{
    printf("tank turn left left left left\n");
    gpio_control_1(GPIO0, IOT_GPIO_VALUE0);
    gpio_control_1(GPIO1, IOT_GPIO_VALUE0);
    gpio_control_1(GPIO9, IOT_GPIO_VALUE1);
    gpio_control_1(GPIO10, IOT_GPIO_VALUE0);

    gpio_control_2(GPIO8, IOT_GPIO_VALUE1);
    gpio_control_2(GPIO7, IOT_GPIO_VALUE0);
    gpio_control_2(GPIO12, IOT_GPIO_VALUE0);
    gpio_control_2(GPIO11, IOT_GPIO_VALUE0);
}

void tank_right(void)
{
    printf("tank turn right\n");
    gpio_control_1(GPIO0, IOT_GPIO_VALUE0);
    gpio_control_1(GPIO1, IOT_GPIO_VALUE1);
    gpio_control_1(GPIO9, IOT_GPIO_VALUE0);
    gpio_control_1(GPIO10, IOT_GPIO_VALUE0);

    gpio_control_2(GPIO7, IOT_GPIO_VALUE0);
    gpio_control_2(GPIO8, IOT_GPIO_VALUE0);
    gpio_control_2(GPIO12, IOT_GPIO_VALUE1);
    gpio_control_2(GPIO11, IOT_GPIO_VALUE0);
}

void tank_stop(void)
{
    printf("tank stop\n");
    gpio_control_1(GPIO0, IOT_GPIO_VALUE0);
    gpio_control_1(GPIO1, IOT_GPIO_VALUE0);
    gpio_control_1(GPIO9, IOT_GPIO_VALUE0);
    gpio_control_1(GPIO10, IOT_GPIO_VALUE0);

    gpio_control_2(GPIO7, IOT_GPIO_VALUE0);
    gpio_control_2(GPIO8, IOT_GPIO_VALUE0);
    gpio_control_2(GPIO12, IOT_GPIO_VALUE0);
    gpio_control_2(GPIO11, IOT_GPIO_VALUE0);
}

// void tank_lf(void)
// {
//     printf("tank turn left front\n");
//     gpio_control_1(GPIO0, IOT_GPIO_VALUE0);
//     gpio_control_1(GPIO1, IOT_GPIO_VALUE1);
//     gpio_control_1(GPIO9, IOT_GPIO_VALUE0);
//     gpio_control_1(GPIO10, IOT_GPIO_VALUE0);

//     gpio_control_2(GPIO8, IOT_GPIO_VALUE1);
//     gpio_control_2(GPIO7, IOT_GPIO_VALUE0);
//     gpio_control_2(GPIO12, IOT_GPIO_VALUE0);
//     gpio_control_2(GPIO11, IOT_GPIO_VALUE0);
// }

// void tank_rf(void)
// {
//     printf("tank turn right front\n");
//     gpio_control_1(GPIO0, IOT_GPIO_VALUE0);
//     gpio_control_1(GPIO1, IOT_GPIO_VALUE0);
//     gpio_control_1(GPIO9, IOT_GPIO_VALUE1);
//     gpio_control_1(GPIO10, IOT_GPIO_VALUE0);

//     gpio_control_2(GPIO7, IOT_GPIO_VALUE0);
//     gpio_control_2(GPIO8, IOT_GPIO_VALUE0);
//     gpio_control_2(GPIO12, IOT_GPIO_VALUE1);
//     gpio_control_2(GPIO11, IOT_GPIO_VALUE0);
// }