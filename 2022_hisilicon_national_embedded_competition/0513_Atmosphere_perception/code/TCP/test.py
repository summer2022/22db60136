#coding=utf-8
#  tcp服务端程序开发

import socket



# 1.创建服务端套接字
    # socket.AF_INET表示IPv4类型
    # SOCK_STREAM表示tcp
tcp_server_socket_ESP32=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
tcp_server_socket_ESP32.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,True)
    #端口复用 服务端退出端口立即释放
    #socket.SOL_SOCKET 表示当前套接字
    #socket.SO_REUSEADDR 复用选项
    #True 确定复用
# 2.绑定端口号
    # 第一个参数表示ip地址，一般不用指定 表示本机的任何一个ip
    #第二个参数表示端口号
tcp_server_socket_ESP32.bind(('',32))
# 3.设置监听
    # 128:表示最大等待建立链接的个数
tcp_server_socket_ESP32.listen(128)
# 4.等待客户端的连接请求
    #每次客户端和服务器建立连接成功都会返回一个新的套接字
new_client_ESP32 , ip_port_ESP32 = tcp_server_socket_ESP32.accept()   

print("ESP32 ip PORT:" , ip_port_ESP32)

# 1.创建服务端套接字
    # socket.AF_INET表示IPv4类型
    # SOCK_STREAM表示tcp
tcp_server_socket_wx=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
tcp_server_socket_wx.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,True)
    #端口复用 服务端退出端口立即释放
    #socket.SOL_SOCKET 表示当前套接字
    #socket.SO_REUSEADDR 复用选项
    #True 确定复用
# 2.绑定端口号
    # 第一个参数表示ip地址，一般不用指定 表示本机的任何一个ip
    #第二个参数表示端口号
tcp_server_socket_wx.bind(('',16))
# 3.设置监听
    # 128:表示最大等待建立链接的个数
tcp_server_socket_wx.listen(128)
# 4.等待客户端的连接请求
    #每次客户端和服务器建立连接成功都会返回一个新的套接字
new_client_wx , ip_port_wx = tcp_server_socket_wx.accept()

print("wx ip PORT:" , ip_port_wx)
    
while True:  
    # 5.接收数据
        #收发消息使用新返回的套接字
    rece_data = new_client_ESP32.recv(1024)
    #rece_data = rece_data.decode('utf-8')
    print("DATA:",rece_data)
    
    # 6.发送数据
    new_client_wx.send(rece_data)
    
# 7.关闭套接字
tcp_server_socket_wx.close()
tcp_server_socket_ESP32.close()