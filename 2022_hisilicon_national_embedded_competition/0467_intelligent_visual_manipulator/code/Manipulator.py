#coding=utf-8
from pyfirmata import Arduino, util

import cv2
import  numpy as np
import serial

import mediapipe as mp
import time

ser = serial.Serial()
ser.baudrate = 115200
print(ser

print(ser.is_open)



def detect_circle_demo ():
    cap = cv2.VideoCapture(0)

    mpHands = mp.solutions.hands
    hands = mpHands.Hands()
    mpDraw = mp.solutions.drawing_utils
    pTime = 0
    cTime = 0

    while True:

        success, img = cap.read()
        imgRGB = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        results = hands.process(imgRGB)
        # print(results.multi_hand_landmarks)
        lmlist = []
        if results.multi_hand_landmarks:
            for handLms in results.multi_hand_landmarks:
                    # print(id, lm)
                    h, w, c = img.shape
                    cx, cy = int(lm.x * w), int(lm.y * h)
                    lmlist.append([id, cx, cy])

                    # print(id, cx, cy)
                    # if id == 4:
                    #     cv2.circle(img, (cx, cy), 15, (255, 0, 255), cv2.FILLED)

                mpDraw.draw_landmarks(img, handLms, mpHands.HAND_CONNECTIONS)

        cTime = time.time()
        fps = 1 / (cTime - pTime)
        pTime = cTime

        cv2.putText(img, str(int(fps)), (10, 70), cv2.FONT_HERSHEY_PLAIN, 3,
                    (255, 0, 255), 3)
        if len(lmlist) != 0:


            x4 = lmlist[4][1]
            y4 = lmlist[4][2]
            x7 = lmlist[7][1]
            y7 = lmlist[7][2]
            r0 = pow(pow(x7 - x4, 2) + pow(y7 - y4, 2), 0.5)
            print('{:.2f}'.format(r0))

            if r0 >= 100 :
                ser.write(b"5")
            elif r0 < 100:
                ser.write(b"0")


            x0 = lmlist[0][1]
            y0 = lmlist[0][2]
            x4 = lmlist[4][1]
            y4 = lmlist[4][2]
            r0 = pow(pow(x4 - x0, 2) + pow(y4 - y0, 2), 0.5)
            print('{:.2f}'.format(r0))


            x5 = lmlist[5][1]
            y5 = lmlist[5][2]
            x8 = lmlist[8][1]
            y8 = lmlist[8][2]
            r1 = pow(pow(x8 - x5, 2) + pow(y8 - y5, 2), 0.5)





            x17 = lmlist[17][1]
            y17 = lmlist[17][2]
            x20 = lmlist[20][1]
            y20 = lmlist[20][2]
            r4 = pow(pow(x20 - x17, 2) + pow(y20 - y17, 2), 0.5)




            if r0 >= 170 and r1 >= 100 and r2 >= 100 and r3 >= 100 and r4 >= 70:



            elif r0 < 170 and r1 < 100 and r2 >= 100 and r3 < 100 and r4 < 70:
                ser.write(b"2")

            elif r0 < 170 and r1 < 100 and r2 < 100 and r3 >= 100 and r4 < 70:
                ser.write(b"3")

            elif r0 < 170 and r1 < 100 and r2 < 100 and r3 < 100 and r4 >= 70:
                ser.write(b"4")

            elif r0 < 170 and r1 >= 100 and r2 >= 100 and r3 < 100 and r4 < 70:
                ser.write(b"b")
            elif r0 >= 170 and r1 >= 100 and r2 < 100 and r3 < 100 and r4 < 70:
                ser.write(b"a")
            elif r0 < 170 and r1 < 100 and r2 >= 100 and r3 >= 100 and r4 >= 70:
                ser.write(b"e")

            elif r0 >= 170 and r1 >= 100 and r2 < 100 and r3 < 100 and r4 >= 70:
                ser.write(b"d")

            elif r0 >= 170 and r1 >= 100 and r2 >= 100 and r3 < 100 and r4 < 70:
                ser.write(b"c")

            elif r0 < 170 and r1 >= 100 and r2 >= 100 and r3 >= 100 and r4 >= 70:
                ser.write(b"f")

            elif r0 < 170 and r1 < 100 and r2 < 100 and r3 < 100 and r4 < 70:
                ser.write(b"h")


        cv2.imshow('Video', img)
        cv2.waitKey(10)
detect_circle_demo()
