#include <string.h>
#include <hi_mux.h>
#include <hi_time.h>
#include "hi_gpio.h"
#include "hi_io.h"
#include "hi_task.h"


unsigned long Sensor_Read(void)
{
    unsigned long value = 0;
    unsigned char i = 1;
    hi_gpio_value input = 0;
    hi_udelay(2);
    //时钟线拉低 空闲时时钟线保持低电位
    hi_gpio_set_ouput_val(HI_IO_NAME_GPIO_12,0);
    hi_udelay(2);
    hi_gpio_get_input_val(HI_IO_NAME_GPIO_11,&input);
    //等待AD转换结束
    while(input)
    {
        hi_gpio_get_input_val(HI_IO_NAME_GPIO_11,&input);
    }

    for(i=0;i<24;i++)
    {
        //时钟线拉高 开始发送时钟脉冲
        hi_gpio_set_ouput_val(HI_IO_NAME_GPIO_12,1);
        hi_udelay(2);
        //左移位， 右侧补零 等待接收数据
        value = value << 1;
        //时钟线拉低
        hi_gpio_set_ouput_val(HI_IO_NAME_GPIO_12,0);
        hi_udelay(2);
        //读取一位数据
        hi_gpio_get_input_val(HI_IO_NAME_GPIO_11,&input);
        if(input){
            value++;
        }
    }
    //第25个脉冲
    hi_gpio_set_ouput_val(HI_IO_NAME_GPIO_12,1);
    hi_udelay(2);
    value = value^0x800000;
    hi_gpio_set_ouput_val(HI_IO_NAME_GPIO_12,0);
    hi_udelay(2);

    return value;
    
}

double Get_Sensor_Read(void)
{
    double sum = 0; //为了减小误差，一次取出10个值后取平均值
    for (int i=0;i<10;i++){ //循环越多精度越高，耗费时间越高
        sum += Sensor_Read(); //累加
    }
    return (sum/10); //求均值
}