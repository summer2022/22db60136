### 本仓库中包含作品Face Dect on NNIE所修改的Hi3516代码以及build.gn文件

### 程序还借鉴使用了开源库与模型

算法库：nniefacelib，海思35xx系列芯片上运行的开源人脸算法库中的RetinaFace模型

<https://github.com/hanson-young/nniefacelib>

模型参考：RetinaFace，2019年最强开源人脸检测

<https://blog.csdn.net/dqcfkyqdxym3f8rb0/article/details/95400941>

目标工程：hi3516dv300\sdk_linux\sample\platform\svp\nnie
一个AI基础的单个应用