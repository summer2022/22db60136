#include "mainwindow.h"
#include <QApplication>
#include "librarydatabase.h"
#include "bookdatabase.h"
#include <QList>
QFont font1,font2,font3,font4;//字体类型使用指针会报错
libraryDataBase* database;//数据库，全局变量
BookDatabase* bookdatabase;
QList<QString> BookBoxList1;
QList<QString> BookBoxList2;
QList<QString> BookBoxList3;
QList<QString> BookBoxList4;
QString ip;
QString port;
int main(int argc, char *argv[])
{
    //以数据库为主体，所有数据从数据库开始，不以学生列表为主体
    //初始化几种字体------------------------------------------------------
    font1.setFamily("宋体");
    font1.setPointSize(15);
    font1.setBold(true);
    font2.setFamily("楷体");
    font2.setPointSize(12);
    font2.setBold(true);
    font3.setFamily("楷体");
    font3.setPointSize(8);
    font3.setBold(false);
    font4.setFamily("楷体");
    font4.setPointSize(9);
    font4.setBold(true);
    //创建数据库
    database=new libraryDataBase();
    bookdatabase=new BookDatabase();
    //tcp参数
    ip="192.168.43.169";
    port="8888";

    //初始化借书的人的数据库
    QList<QString> initbooklist;
    //初始化三个书箱
    for(int i=0;i<3;i++)
    {
        //QList box;
        //BookBoxListList.append(box);
    }

    Student* person1=new Student("20192333001","曾康","./images/20192333001.png","通信工程","2019",initbooklist);
    Student* person2=new Student("20192333002","欧阳俊斌","./images/20192333002.png","通信工程","2019",initbooklist);
    Student* person3=new Student("20192333003","郭先达","./images/20192333003.png","通信工程","2019",initbooklist);

    database->addLine(person1);
    database->addLine(person2);
    database->addLine(person3);

    person3=new Student("20192333004","张三","./images/20192333013.png","通信工程","2019",initbooklist);
    database->addLine(person3);
    person3=new Student("20192333005","李四","./images/20192333023.png","通信工程","2019",initbooklist);
    database->addLine(person3);
    person3=new Student("20192333006","王五","./images/20192333033.png","通信工程","2019",initbooklist);
    database->addLine(person3);
    person3=new Student("20192333007","小明","./images/20192333043.png","通信工程","2019",initbooklist);
    database->addLine(person3);
    person3=new Student("20192333008","大明","./images/20192333053.png","通信工程","2019",initbooklist);
    database->addLine(person3);
    person3=new Student("20192333009","小红","./images/20192333063.png","通信工程","2019",initbooklist);
    database->addLine(person3);
    person3=new Student("20192333015","小张","./images/20192333073.png","通信工程","2019",initbooklist);
    database->addLine(person3);
    person3=new Student("20192333025","小武","./images/20192333083.png","通信工程","2019",initbooklist);
    database->addLine(person3);
    person3=new Student("20192333035","小王","./images/20192333093.png","通信工程","2019",initbooklist);
    database->addLine(person3);
    person3=new Student("20192333045","小李","./images/20192333103.png","通信工程","2019",initbooklist);
    database->addLine(person3);
    person3=new Student("20192333055","小郑","./images/20192333203.png","通信工程","2019",initbooklist);
    database->addLine(person3);
    person3=new Student("20192333065","小关","./images/20192333303.png","通信工程","2019",initbooklist);
    database->addLine(person3);
    person3=new Student("20192333075","小刘","./images/20192333403.png","通信工程","2019",initbooklist);
    database->addLine(person3);


    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
