#ifndef LIBRARYDATABASE_H
#define LIBRARYDATABASE_H
#include <QObject>
#include <QMessageBox>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QDebug>
#include <QSqlQueryModel>
#include "student.h"
#include <QBuffer>
#include "bookdatabase.h"
extern BookDatabase* bookdatabase;
class libraryDataBase
{
public:
    libraryDataBase();
    QSqlDatabase db1;
    QSqlQuery query;
    QSqlQueryModel *model;

    void addLine(Student* sm);//增加一行数据
    void deleteLine(int id);//删除一行数据
    void changeLine(Student* sm);//改变一行数据
    Student* getLine(QString id);//获取一行数据
    QString getBookName(QString t);//根据书籍数据库获得书籍名称
};

#endif // LIBRARYDATABASE_H
