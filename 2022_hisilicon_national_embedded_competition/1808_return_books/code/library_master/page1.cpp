#include "page1.h"
#include "ui_page1.h"

Page1::Page1(Page2 *p) :
    ui(new Ui::Page1)
{
    ui->setupUi(this);
    //界面初始化
    ui->groupBox_6->setFont(font1);
    ui->groupBox_6->setTitle("");//动图的标题
    ui->groupBox_7->setFont(font1);
    ui->groupBox_7->setTitle("");//书箱的标题
    ui->groupBox_1->setFont(font2);
    ui->groupBox_1->setTitle("1号书箱(共0本)");
    ui->groupBox_2->setFont(font2);
    ui->groupBox_2->setTitle("2号书箱(共0本)");
    ui->groupBox_3->setFont(font2);
    ui->groupBox_3->setTitle("3号书箱(共0本)");
//    ui->groupBox_4->setFont(font2);
//    ui->groupBox_4->setTitle("4号书箱(共0本)");

    ui->listWidget_1->setFont(font3);
    ui->listWidget_2->setFont(font3);
    ui->listWidget_3->setFont(font3);
    //ui->listWidget_4->setFont(font3);

    ui->pushButton_connect->setFont(font2);
    ui->pushButton_connect->setText("开始工作");
    ui->pushButton_set->setFont(font2);
    ui->pushButton_set->setText("网络设置");
    ui->label_title->setFont(font1);
    ui->label_title->setAlignment(Qt::AlignCenter);
    ui->label_title->setText("图书馆智能还书系统");
    ui->label_picture->setPixmap(QPixmap("./machine.png"));
    //QMovie* movie=new QMovie("work.gif");
    //movie->resized(QSize(ui->label_picture->width()*9,ui->label_picture->height()*20));
    //ui->label_picture->setAlignment(Qt::AlignCenter);
    //ui->label_picture->setMovie(movie);
    //movie->start();
    socket = new QTcpSocket();
    connect(ui->pushButton_set,&QPushButton::clicked,[=](){
        Page1Dialog* setDialog=new Page1Dialog(this);
        setDialog->show();
        setDialog->setAttribute(Qt::WA_DeleteOnClose);
    });
    //连接Hi3861的槽函数
    connect(ui->pushButton_connect,&QPushButton::clicked,[=](){
        socket->connectToHost(ip,port.toInt());//连接服务器
        //socket->write(QString("$$$$").toUtf8().data());//以$$$$为开始工作的指令
    });

    //接收Hi3861的信息，自动触发，自定义通信协议为：##人的id&书的id##
    connect(socket,&QTcpSocket::readyRead,[=](){
        tcpRevbuf=socket->readAll();//读取全部接受到的数据并存放在tcpRevbuf中
        qDebug()<<"接收到的信息为："<<tcpRevbuf;
        //数据处理，##之间为一次人的标签和书的标签，用&分割人和书
        QString studentAll = QString(tcpRevbuf).section("#",1,1);//切割字符串，起始/结尾
        QString studentBookBox= QString(studentAll).section("&",0,0);//切割字符串，起始/结尾
        QString studentID= QString(studentAll).section("&",1,1);//切割字符串，起始/结尾
        QString studentBook= QString(studentAll).section("&",2,2);//切割字符串，起始/结尾
        qDebug()<<"----studentAll--------"<<studentAll;
        qDebug()<<"----studentBookBox------"<<studentBookBox;
        qDebug()<<"----studentID--------"<<studentID;
        qDebug()<<"----studentBook--------"<<studentBook;

        //新建学生对象
        Student* st=new Student();
        //先读取数据库中该学生的数据
        st=database->getLine(studentID);
        st->borrowBookList.append(studentBook);
        //st->borrowBookList.removeOne(studentBook);//删除studentBook这本书，表示已还
        database->changeLine(st);//更新数据库，有还书那么一定有借书记录，所以不需要判断有无借书

        //日志显示
        QDateTime currentDateTime =QDateTime::currentDateTime();
        QString currentDate =currentDateTime.toString("yyyy.MM.dd hh:mm:ss.zzz ddd");
        QString message=QString("%1同学（学号%2）于 %3 归还%4").arg(st->name).arg(st->id).arg(currentDate).arg(database->getBookName(studentBook));
        p->setListWidget(message);

        //书箱统计
        switch(studentBookBox.toInt())
        {
            case 1:
                BookBoxList1.append(database->getBookName(studentBook));
                ui->listWidget_1->addItem(database->getBookName(studentBook));
                ui->groupBox_1->setTitle(QString("1号书箱(共%1本)").arg(BookBoxList1.length()));
            break;
            case 2:
                BookBoxList2.append(database->getBookName(studentBook));
                ui->listWidget_2->addItem(database->getBookName(studentBook));
                ui->groupBox_2->setTitle(QString("2号书箱(共%1本)").arg(BookBoxList2.length()));
            break;
            case3:
                BookBoxList3.append(database->getBookName(studentBook));
                ui->listWidget_3->addItem(database->getBookName(studentBook));
                ui->groupBox_3->setTitle(QString("3号书箱(共%1本)").arg(BookBoxList3.length()));
            break;
//            case 4:
//                BookBoxList4.append(database->getBookName(studentBook));
//                ui->listWidget_4->addItem(database->getBookName(studentBook));
//                ui->groupBox_4->setTitle(QString("4号书箱(共%1本)").arg(BookBoxList4.length()));
//            break;
        }
    });
}
Page1::~Page1()
{
    delete ui;
}
