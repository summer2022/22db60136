#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    setWindowIcon(QIcon(":/icos/student.ico"));
    setWindowTitle("学生信息");
    //初始化
    ui->label_id->setFont(font2);
    ui->label_id->setText("学号:");
    ui->label_name->setFont(font2);
    ui->label_name->setText("姓名:");
    ui->label_major->setFont(font2);
    ui->label_major->setText("专业:");
    ui->label_grade->setFont(font2);
    ui->label_grade->setText("年级:");
}
//void Dialog::paintEvent(QPaintEvent*)
//{
//    if(paintEnable==1)
//    {
//        int w=ui->label_picture->width();
//        int h=ui->label_picture->height();
//        qDebug()<<this->picturePath;
//        ui->label_picture->setPixmap(QPixmap(this->picturePath).scaled(QSize(h*7,w*2.7),Qt::IgnoreAspectRatio, Qt::SmoothTransformation));
//    }
//}
Dialog::~Dialog()
{
    delete ui;
}
void Dialog::setID(QString id)
{
    ui->label_id_value->setFont(font2);
    ui->label_id_value->setText(id);
}
void Dialog::setName(QString name)
{
    ui->label_name_value->setFont(font2);
    ui->label_name_value->setText(name);
}
void Dialog::setMajor(QString major)
{
    ui->label_major_value->setFont(font2);
    ui->label_major_value->setText(major);
}
void Dialog::setGrade(QString grade)
{
    ui->label_grade_value->setFont(font2);
    ui->label_grade_value->setText(grade);
}
void Dialog::setPicture(QString picturePath)
{
    int w=ui->label_picture->width();
    int h=ui->label_picture->height();
    this->picturePath=picturePath;
    ui->label_picture->setPixmap(QPixmap(picturePath).scaled(QSize(h*7,w*2.7),Qt::IgnoreAspectRatio, Qt::SmoothTransformation));
    //ui->label_picture->setPixmap(QPixmap(picturePath));
    paintEnable=1;
}
void Dialog::setBookList(QList<QString> list)
{
    foreach (QString ll, list) {
        ui->listWidget->addItem(ll);
    }
}
