#include "page4.h"
#include "ui_page4.h"

Page4::Page4(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Page4)
{
    ui->setupUi(this);
    ui->tableView->setFont(font2);
    //ui->tableView->horizontalHeader()->setDefaultAlignment(Qt::AlignCenter);
    //ui->tableView->verticalHeader()->setDefaultAlignment(Qt::AlignCenter);
    //ui->tableView->setAlternatingRowColors(true);
    //ui->tableView->setAutoScroll(true);

    ui->tableView->verticalHeader()->setDefaultSectionSize(240);
    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    QStandardItemModel *model=new QStandardItemModel(bookdatabase->bookmodel->columnCount(),3,this);
    QStringList str;
    str<<"编号"<<"书名"<<"照片";
    model->setHorizontalHeaderLabels(str);
    ui->tableView->setModel(model);
    for(int i=0;i<bookdatabase->bookmodel->columnCount();i++)
    {
        QLabel *label1=new QLabel(this);
        QLabel *label2=new QLabel(this);
        QLabel *label3=new QLabel(this);
        label1->setAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
        label1->setWordWrap(true);
        label1->setText(bookdatabase->bookmodel->data(bookdatabase->bookmodel->index(i,0)).toString());
        ui->tableView->setIndexWidget(model->index(i,0),label1);
        label2->setAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
        label2->setWordWrap(true);
        label2->setText(bookdatabase->bookmodel->data(bookdatabase->bookmodel->index(i,1)).toString());
        ui->tableView->setIndexWidget(model->index(i,1),label2);
        label3->setAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
        label3->setWordWrap(true);
        label3->setPixmap(QPixmap(bookdatabase->bookmodel->data(bookdatabase->bookmodel->index(i,2)).toString()).scaled(QSize(130,190)));
        ui->tableView->setIndexWidget(model->index(i,2),label3);
    }
    ui->tableView->show();
}

void Page4::update()
{
//    ui->tableView->setModel(bookdatabase->bookmodel);
//    for(int i=0;i<bookdatabase->bookmodel->columnCount();i++)
//    {
//        QLabel *bookPictureLabel = new QLabel();
//        // QPushButton *bookPictureLabel = new QPushButton();
//        bookPictureLabel->setPixmap(QPixmap(bookdatabase->bookmodel->data(bookdatabase->bookmodel->index(i,0)).toString()).scaled(QSize(100,30)));
//        ui->tableView->setIndexWidget(bookdatabase->bookmodel->index(i,2),bookPictureLabel);
//    }
//    ui->tableView->show();
}

Page4::~Page4()
{
    delete ui;
}
