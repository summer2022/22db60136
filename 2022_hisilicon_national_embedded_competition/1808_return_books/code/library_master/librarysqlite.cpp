#include "librarysqlite.h"
/*
 * 使用SQlite数据库，没有连接服务器
 * SQlite使用三个类QSqlDatabase、QSqlQuery、QSqlQueryModel
 * QSqlDatabase：创建数据库
 * QSqlQuery：使用sql语句创建表头等等
 * QSqlQueryModel：读取数据库，变为一个model,model在TableView上显示
 */
LibrarySqlite::LibrarySqlite(QObject *parent) : QObject(parent)
{
    //创建数据库并创建表格
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("library.db");
    if(!db.open())
    {
        qDebug("open is false");
        return false;
    }
    else
    {
        qDebug("open is OK");
    }
    QSqlQuery query;
    //创建一个表格，主键值为id,20字符的名字、20字符的学号
    query.exec("create table student (id int primary key, name varchar(20),studentNumber varchar(20))");
    //query.exec("insert into student values(0, '张飞','20192333068')");//字符类型可以使用中文

    //创建model
    QSqlQueryModel *model = new QSqlQueryModel;
    model->setQuery("select * from student");
    model->setHeaderData(0, Qt::Horizontal, tr("id"));
    model->setHeaderData(1, Qt::Horizontal, tr("name"));
    model->setHeaderData(2, Qt::Horizontal, tr("studentNumber"));
}
bool LibrarySqlite::create()
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("library.db");
    if(!db.open())
    {
        qDebug("open is false");
        return false;
    }
    else
    {
        qDebug("open is OK");
    }
    QSqlQuery query;
    //创建一个表格，主键值为id,20字符的名字、20字符的学号
    query.exec("create table student (id int primary key, name varchar(20),studentNumber varchar(20))");
    //query.exec("insert into student values(0, '张飞','20192333068')");//字符类型可以使用中文
    return true;
}
