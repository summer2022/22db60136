

# 路口摄像头

# 介绍

## 设计目的

当今社会交通日益发达，随着汽车数量剧增，交通事故的发生屡见不鲜。据统计，在城市交通中，近八成以上交通事故发生在两路交会路口或环形岛等地。基于此，我们设计一个路口摄像头，让我们能够及时了解情况，完成数据分析，同时对特殊情况作出应对，保护行人安全、避免交通意外、维护社会和谐。

## 应用领域

该作品的核心功能是检测行人并统计数量，借此可应用于路口的行人检测，以及其他需要对人群数量进行检测的地方，如景点的旅客日流量、火车站的预估建设规模等。

## 主要技术特点

- (1) 对摄像头采集的图像逐帧进行处理，使用yolov2构成的检测神经网络对人进行检测并对同一场景下的行人计数，实时返回路口流量。
- (2) 回返数据通过WiFi上传云端，使得用户借助微信小程序完成对数据的管理。
- (3)回传数据通过统计，当路口行人过多，将发布提醒信息。

## 关键性能指标

- 1、神经网络逐帧的识别准确率；
- 2、智能算法推理运行的速率；
- 3、云上和落地的传递速率；

#系统组成

## 整体介绍

本系统的主要硬件部分由 Taurus AI Camera 智能相机与 Pegasus WIFI IOT 开发板组成，两者之间通过串口完成通信。其中 Taurus AI Camera 主要负责搭载神经网络模型算法实现对图像的逐帧推理，完成识别功能。Pegasus 端主要负责对 Taurus 端结果的传递和各功能的管理。

## 模块介绍

- Taurus AI Camera 智能相机负责识别
- Pegasus WIFI IOT 开发板负责管理
- 小程序负责控制

# 完成情况及性能参数


Pegasus WIFI IOT部分完成情况如下：
- oled屏幕上能够显示队伍号和各功能情况。
- 按键一用来切换模式，未按下用户键或未在NFC界面下使用NFC将导致指示灯、云端、串口功能不能解锁。如果功能未解锁，则屏幕上只会出现被锁，功能也无法开启，直到解锁，才显示相应的字样。
- 指示灯模式下，按键二控制主板上指示灯的亮灭以检验主板正常。
- 互联模式用来管理摄像头和控制中心的串口功能是否打开。
- 云端通过MQTT协议实现小程序和控制板的互联。
- 回到菜单完成模式的选择循环。
 
小程序部分完成情况如下：
- 动态启动页面防止出现白屏。
- 之后是wifi热点的连接。
- 然后是用户登录和跳转的界面。
- 跳转云上控制页面，分别展示了设备信息，刷新功能，和具体的功能开关。
- 监控按键，可以跳转到监控页面。这一页面的主要功能是实时视频显示和数据分析。数据分析为弹窗页面，显示当前屏幕上的人数量，当人数过多将提示注意交通安全。左下角的悬浮开关，用于服务器未连接无实时视频时，使用手机摄像头验证流程。
 
Taurus AI Camera 智能相机部分情况如下：
- 平均识别准确率在77%以上，达到预期目标。
- 帧率大致达到25帧/秒，满足需求。

# 总结

## 可扩展之处

目前的人机交互方式限制较大，用户仅能通过微信小程序来获取设定的工作情况，无法自由选择想要获取的数据。同时因为服务器开发问题和模型的突发损坏，导致了无法得到实时的监控画面和人车分类的推理结果。
后期将继续优化人机交互的方式，在小程序端开展实时的视频画面推送，同时进行数据可视化的处理，使得数据和其分析更加清晰。

## 心得体会

此次大赛带给我最大的收获在于知识的丰满和充实的自信。
在参加此次比赛之前，我曾耳闻过、参加过其他有关物联网的学科竞赛，它们让我接触不同的单片机，思考其中运行流淌的各色各异的代码，但是对于嵌入式、单片机、SoC、NPU等，也始终只是停留在深浅不同的看过了解过。
回顾参赛历程，促使我报名的关键在于摄像头的视觉处理，但开发过程中各种各样的疑惑和思索才是我此次经历最宝贵的财富。
这一次的参赛，更像一次有目标的求学经历。最初确立的比赛目标不断让我们走出舒适区，硬着头皮带着茫然去学习更新的知识，当我们沉浸其中、不断刷新又回顾，突然发现事情并没有想象中的困难，我们已经走出了超出预期的路。
数据、算力、算法，人工智能的三大要素。得益于科学家和程序员的共同努力，算法不断发展，又更好地服务于大众，经典例子在于python之父提出了威力巨大的Timsort算法，却通过一个简简单单的list.sort即可完成调用。
借助各地各时人们的无私贡献，各种数据集不断涌现以满足人们的需求，在模型的建立中起着不可或缺的力量。
但是随着人工智能的发展，算力的要求越来越高，和实际部署中的环境也相差越来越远。5G、IOT的概念开始普及到华夏大地的角角落落，各种人脸识别、智能检测的需求场景涌现，但是实际情况中并不存在算力巨大的服务器，无视时空距离的传输，或者是待机服务的维修人员，智能算法不能完成部署和发挥其应用的威力，不免成为遗憾。
这次的比赛提供机会让我能追求底层代码更快地响应速度、智能算法更好地落地呈现，我也在此期间学到了从运行到编译，从训练到部署的流程。这次的经历将在我的成长历程中，不断地指引我的方向，让我能更好地适应环境，贡献力量。


# 贡献者

- 赵春廷
- 王为进
- 蔡瀚霖

# 参考文献

- [1]张烈平,李智浩,唐玉良.基于迁移学习的轻量化YOLOv2口罩佩戴检测方法[J].电子测量技术,2022,45(10):112-117.DOI:10.19651/j.cnki.emt.2108620.
- [2]冉险生,陈卓,张禾.改进YOLOv2算法的道路摩托车头盔检测[J].电子测量技术,2021,44(24):105-115.DOI:10.19651/j.cnki.emt.2107718.

## License

[MIT © Richard McRichface.](../LICENSE)

# 附录

- 推理部分：
HI_S32 Yolo2HandDetectResnetClassifyCal(uintptr_t model, VIDEO_FRAME_INFO_S *srcFrm, VIDEO_FRAME_INFO_S *dstFrm)
{
    //model == MODEL_FILE_GESTURE_CL == self
    SAMPLE_SVP_NNIE_CFG_S *self = (SAMPLE_SVP_NNIE_CFG_S*)model;
    HI_S32 resLen = 0;
    int objNum;
    unsigned char num_obj; 
    int ret;
    int num = 0;

    ret = FrmToOrigImg((VIDEO_FRAME_INFO_S*)srcFrm, &img);          //color_space_translate

    SAMPLE_CHECK_EXPR_RET(ret != HI_SUCCESS, ret, "hand detect for YUV Frm to Img FAIL, ret=%#x\n", ret);

    objNum = HandDetectCal(&img, objs); // Send IMG to the detection net for reasoning                          //

    for (int i = 0; i < objNum; i++) {
        cnnBoxs[i] = objs[i].box;
        RectBox *box = &objs[i].box;
        RectBoxTran(box, HAND_FRM_WIDTH, HAND_FRM_HEIGHT,
            dstFrm->stVFrame.u32Width, dstFrm->stVFrame.u32Height);

        SAMPLE_PRT("yolo2_out: {%d, %d, %d, %d}\n", box->xmin, box->ymin, box->xmax, box->ymax);
        boxs[i] = *box;
    }

    biggestBoxIndex = GetBiggestHandIndex(boxs, objNum);
    SAMPLE_PRT("biggestBoxIndex:%d, objNum:%d\n", biggestBoxIndex, objNum);
- 显示部分：
    OledScreenInitConfig();
    TestGpioInit();
    AppMultiSampleDemo();
    /* display init */
    OledNfcDisplayInit();

    while (1) {
        switch (GetKeyStatus(CURRENT_MODE)) {
            case NFC_TAG_WECHAT_MODE:
                NFCTagWechatMode();
                break;
            case LIGHT_MODE:
                if (nfc_state == 2)
                {
                    light_mode();
                }else
                {
                    lock_mode();
                }
                break;
            case INTER_MODE:
                if (nfc_state == 2)
                {
                    inter_mode();
                }else
                {
                    lock_mode();
                }
                break;
            case CLOUD_MODE:
                if (nfc_state == 2)
                {
                    cloud_mode();
                }else
                {
                    lock_mode();
                }
                break;                                                
            case NFC_RETURN_MODE:
                NfcOledReturnMode();
                break;
            default:
                break;
        }
        TaskMsleep(SLEEP_10_MS); // 10ms
    }
