static const float items_weight[] = {
    0,
    24.1,   //纸巾      1
    300.5,  //牛奶      2
    0,      //威化饼干  3
    9.4,    //笔        4
    43.4,   //口香糖    5
    0
};

static const char *items0_name[]={
    "",
    "清风手帕纸",
    "透明挂钩",
    "菠菠面包",
    "文具剪",
    "厚双面胶带",
    "纸浆模具餐具"
};

static const char *items1_name[]={
    "",
    "糖果",
    "葡萄",
    "yuc",
    "dic",
    "sdc",
    ""
};

static const float items0_price[] = {
    0,
    1,   //     1
    5.5,  //      2
    6,      //  3
    3.5,    //        4
    5,   //    5
    7
};

static const float items1_price[] = {
    0,
    0.08,   //纸巾      1
    0.06,  //牛奶      2
    2,      //威化饼干  3
    2,    //笔        4
    5,   //口香糖    5
    0
};