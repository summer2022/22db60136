/*
 * Copyright (c) 2022 HiSilicon (Shanghai) Technologies CO., LIMITED.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* End of #ifdef __cplusplus */
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "unistd.h"
#include <string.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>

#include "send.h"
#include "sdk.h"
#include <semaphore.h>

#define MM_KEY "/mytmp"
#define SEM_KEY "/mysem"
//自定义标识符，最终是以16进制来显示
// 共享内存访问标识
#define MM_KEY 0x12
// 信号量访问标识
#define SEM_KEY 0x34
// 共享内存指针
char *shm_start = NULL;
int semid;
int shm_id;
#define Length 4096

/*
 * function    : main()
 * Description : main
 */

unsigned int *BoxNum;
struct Bbox
{
    unsigned int ULCX;
    unsigned int ULCY;
    unsigned int LLCX;
    unsigned int LLCY;

} * box1;


//初始化信号量
int init_sem(int sem_id,int init_value)
{
    union semun sem_union;
    sem_union.val = init_value;
    if(semctl(sem_id,0,SETVAL,sem_union) == -1)
    {
        // printf("Initialize semaphore.\n");
        return -1;
    }
    return 0;
}

int sem_p(int semid)
{
    struct sembuf mybuf; // 对信号量做减1操作，即等待P（sv）
    mybuf.sem_num = 0;
    mybuf.sem_op = -1;
    mybuf.sem_flg = SEM_UNDO;
    if (semop(semid, &mybuf, 1) < 0)

    {
        perror("semop");
        exit(1);
    }

    return 0;
}
int sem_v(int semid)
{
    struct sembuf mybuf;
    mybuf.sem_num = 0;
    mybuf.sem_op = 1;
    mybuf.sem_flg = SEM_UNDO;
    if (semop(semid, &mybuf, 1) < 0)

    {
        perror("semop");
        exit(1);
    }

    return 0;
}
int del_sem(int sem_id)
{
    union semun sem_union;
    if(semctl(sem_id,0,IPC_RMID,sem_union) == -1)
    {
        perror("Delete semahore.\n");
        return -1;
    }
}
void sendInit()
{
    shm_id = shmget(MM_KEY, 4096, IPC_CREAT | 0664);
    if (shm_id < 0)
    {
        perror("shmget error");
        // return -1;
    }

    // 2.建立映射关系
    shm_start = shmat(shm_id, NULL, 0);
    if (shm_start == (char *)-1)
    {
        perror("shmat error");
        // return -1;
    }
    // 3.创建信号量
    semid = semget(SEM_KEY, 1, IPC_CREAT | 0666); //创建一个信号量
    init_sem(semid, 1);

    BoxNum = (unsigned int *)shm_start;
    box1 = (struct Bbox *)(shm_start + 4);
}
void sendBox(unsigned int Num,unsigned int x1,unsigned int y1, unsigned int x2,unsigned int y2)
{
    sem_p(semid); //获取信号量
    *BoxNum = Num;
    box1->ULCX = x1;
    box1->ULCY = y1;
    box1->LLCX = x2;
    box1->LLCY = y2;
    sem_v(semid); //获取信号量
}
void sendDeinit()
{
    shmdt(shm_start);
    shmctl(MM_KEY,  IPC_RMID, NULL);
    del_sem(semid);
}
/********************************************************/

//     //****************************************************************************/
    
// }
// int main(void)
// {
//     sem_t mySem;
//     HI_S32 s32Ret;
//     sem_init(&mySem, 0, 1);
//     sem_destroy(&mySem);
//     sdk_init();
//     /* MIPI is GPIO55, Turn on the backlight of the LCD screen */
//     system("cd /sys/class/gpio/;echo 55 > export;echo out > gpio55/direction;echo 1 > gpio55/value");
//     // s32Ret = SampleVioVpssVoMipi();
//     sdk_exit();
//     SAMPLE_PRT("\nsdk exit success\n");
//     return s32Ret;
// }

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* End of #ifdef __cplusplus */