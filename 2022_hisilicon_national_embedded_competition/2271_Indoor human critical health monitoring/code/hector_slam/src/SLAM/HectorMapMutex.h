#ifndef hectormapmutex_h__
#define hectormapmutex_h__

#include <pthread.h>
#include "util/MapLockerInterface.h"

// #include <boost/thread/mutex.hpp>

class HectorMapMutex : public MapLockerInterface
{
public:
  virtual void lockMap()
  {
    //mapModifyMutex_.lock();
    pthread_mutex_lock(&mapModifyMutex_);
  }

  virtual void unlockMap()
  {
    //mapModifyMutex_.unlock();
    pthread_mutex_unlock(&mapModifyMutex_);
  }

  HectorMapMutex()
  {
      pthread_mutex_init(&mapModifyMutex_,NULL);
  }
  ~HectorMapMutex()
  {
      pthread_mutex_destroy(&mapModifyMutex_);
  }

  //boost::mutex mapModifyMutex_;
  pthread_mutex_t mapModifyMutex_;
};

#endif
