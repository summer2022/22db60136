#ifndef _SOCKET_H
#define _SOCKET_H

#include <pthread.h>

void* socketSendThread(void* IP_Add);
int socketSendData(int8_t* data,int dataLen);

#endif
