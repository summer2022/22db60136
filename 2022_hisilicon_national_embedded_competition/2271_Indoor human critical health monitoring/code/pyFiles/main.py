# This is a sample Python script.
#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.


img_PicDstTraPath = 'tra'
img_PicDstValPath = 'val'
img_PicDstTstPath = 'tst'
test_dir = 'test'

# os 用于操作文件夹
import os

from os import getcwd

# def save_file(SrcDir,DstDir):
#     shutil.copy(SrcDir, DstDir)

if __name__ == '__main__':
    # 获取当前路径
    wd = getcwd()
    txtpath = img_PicDstTstPath
    # 遍历所有的txt文件
    list = os.listdir(txtpath)
    for i in range(0, len(list)):
        path = txtpath + '/' + str(list[i])
        if ('.txt' in path) :
            # 对每个txt文件都遍历每一行 先读 读后关闭 然后写
            file1 = open(path,'r')
            lines = file1.readlines()
            file1.close()
            file2 = open(path,'w')
            for line in lines:
                # 保留种类为 0 的标签 （人）
               if line.split(' ')[0] == '0':
                   file2.write(line)
            file2.close()




