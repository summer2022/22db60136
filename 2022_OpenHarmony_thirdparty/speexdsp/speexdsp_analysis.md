# speexdsp核心库分析

## 一、库实现方式

- 编程语言：C
- 原声平台：linux

## 二、依赖分析

- 除C标准库外，无其他第三方库依赖

## 三、 license以及版权

Copyright 2002-2008 	Xiph.org Foundation
Copyright 2002-2008 	Jean-Marc Valin
Copyright 2005-2007	Analog Devices Inc.
Copyright 2005-2008	Commonwealth Scientific and Industrial Research
                        Organisation (CSIRO)
Copyright 1993, 2002, 2006 David Rowe
Copyright 2003 		EpicGames
Copyright 1992-1994	Jutta Degener, Carsten Bormann

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

- Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.

- Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

- Neither the name of the Xiph.org Foundation nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

## 四、 最新一次版本

- 2022年6月16日,版本号1.2.1

## 五、 功能点分析

### 预处理器

预处理器被设计为在运行编码器之前在音频上使用。预处理器提供三个主要功能:
- 噪声抑制
- 自动增益控制(AGC)
- 语音活动检测(VAD)

### 自适应抖动缓冲区

- 当通过UDP或RTP传输语音(或任何相关内容)时，包可能会丢失，以不同的延迟到达，甚至乱序。
- 抖动缓冲区的目的是重新排序数据包，并缓冲足够长的时间，以便它们可以被发送以进行解码。

### 声学回声消除器

- 回声消除是为了提高远端质量,在任何免提通信系统中，远端语音通过本地扬声器播放。音频在房间内传播，并被麦克风捕获。如果从麦克风捕获的音频被直接发送到远程端，那么用户就会听到远端语音的回声。声学回声消除器设计用于在声学回声发送到远端之前消除它。

- [speexdsp功能测试文档](./speexdsp_Functional_test.md)中测试回声消除输入的两个音频，一个是包含底噪音的正常音频文件，一个就是底噪音文件，通过回声消除功能，可以将正常音频中的底噪音消除掉。

### 重采样器

这个重采样器可以用于在任意两个速率之间进行转换(比率必须是有理数)，并且可以控制质量/复杂性的权衡。
- 重采样器在某些情况下将音频从一个采样率转换到另一个采样率。
- 它可以用于混合具有不同采样率的流、用于支持声卡不支持的采样率、用于转码等。

## 六、 代码规模
|文件名|代码行数|
|--|--|
arch.h|232 
bfin.h|15 
buffer.c|   176 
fftwrap.c|   448 
fftwrap.h|    58 
filterbank.c|   227 
filterbank.h|    66 
fixed_arm4.h|   135 
fixed_arm5e.h|   160 
fixed_bfin.h|   141 
fixed_debug.h|   497 
fixed_generic.h|   106 
jitter.c|   839 
kiss_fft.c|   523 
_kiss_fft_guts.h|   160 
kiss_fft.h|   108 
kiss_fftr.c|   297 
kiss_fftr.h|    51 
math_approx.h|   332 
mdf.c|  1279 
misc_bfin.h|    56 
os_support.h|   169 
preprocess.c|  1215 
pseudofloat.h|   379 
resample.c|  1239 
resample_neon.h|   339 
resample_sse.h|   128 
scal.c|   293 
smallft.c|  1261 
smallft.h|    46 
vorbis_psy.h|    97 
speex_buffer.h|   68 
speexdsp_config_types.h|   12 
speexdsp_types.h|  126 
speex_echo.h|  170 
speex_jitter.h|  197 
speex_preprocess.h|  219 
speex_resampler.h|  343 
总行数|12207



































