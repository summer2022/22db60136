NAME
        quirc
SYNOPSIS
        #include "quirc.h"
TYPE
        struct
DESCRIPTION

NAME
        quirc_point
SYNOPSIS
        #include "quirc.h"
TYPE
        struct
DESCRIPTION
        This structure describes a location in the input image buffer.

NAME
        quirc_decode_error_t
SYNOPSIS
        #include "quirc.h"
TYPE
        enum
DESCRIPTION
        This enum describes the various decoder errors which may occur.

NAME
        quirc_code
SYNOPSIS
        #include "quirc.h"
TYPE
        struct
DESCRIPTION
        This structure is used to return information about detected QR codes in the input image.

NAME
        quirc_data
SYNOPSIS
        #include "quirc.h"
TYPE
        struct
DESCRIPTION
        This structure holds the decoded QR-code data


NAME
        quirc_version
SYNOPSIS
        #include "quirc.h"
INPUT
        void
OUTPUT
        const char*
DESCRIPTION
        Obtain the library version string.

NAME
        quirc_new
SYNOPSIS
        #include "quirc.h"
INPUT
        void
OUTPUT
        struct quirc*     
DESCRIPTION
        Construct a new QR-code recognizer. This function will return NULL if sufficient memory could not be allocated.

NAME
        quirc_destory
SYNOPSIS
        #include "quirc.h"
INPUT
        struct quirc* q
OUTPUT
        void
DESCRIPTION
        Destroy a QR-code recognizer.

NAME
        quirc_resize
SYNOPSIS
        #include "quirc.h"
INPUT
        struct quirc* q, int w, int h
OUTPUT
        int       
DESCRIPTION
        Resize the QR-code recognizer. The size of an image must be specified before codes can be analyzed.
        This function returns 0 on success, or -1 if sufficient memory could not be allocated.

NAME
        quirc_begin
SYNOPSIS
        #include "quirc.h"
INPUT
        struct quirc* q, int* w, int* h
OUTPUT
        uint8_t       
DESCRIPTION
        quirc_begin() must first be called to obtain access to a buffer into which the input image should be placed. Optionally, the current width and height may be returned.

NAME
        quirc_end
SYNOPSIS
        #include "quirc.h"
INPUT
        struct quirc* q
OUTPUT
        void   
DESCRIPTION
        After filling the buffer, quirc_end() should be called to process the image for QR-code recognition. The locations and content of each code may be obtained using accessor functions described below.

NAME
        quirc_strerror
SYNOPSIS
        #include "quirc.h"
INPUT
        quirc_decode_error_t err
OUTPUT
        const char*       
DESCRIPTION
        Return a string error message for an error code. 

NAME
        quirc_count
SYNOPSIS
        #include "quirc.h"
INPUT
        const struct quirc* q
OUTPUT
        int
DESCRIPTION
        Return the number of QR-codes identified in the last processed image.

NAME
        quirc_extract
SYNOPSIS
        #include "quirc.h"
INPUT
        const struct quirc* q, int index, struct quirc_code* code
OUTPUT
        void
DESCRIPTION
        Extract the QR-code specified by the given index.        

NAME
        quirc_flip
SYNOPSIS
        #include "quirc.h"
INPUT
        struct quirc_code* code
OUTPUT
        void        
DESCRIPTION
        Flip a QR-code according to optional mirror feature of ISO 18004:2015

NAME
        quirc_decode
SYNOPSIS
        #include "quirc.h"
INPUT
        const struct quirc_code* code, struct quirc_data* data
OUTPUT
        quirc_decode_error_t
DESCRIPTION
        Decode a QR-code, returning the payload data.