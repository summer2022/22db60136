# quirc 功能测试文档
## 功能
|接口名|功能|
|--|--|
quirc_count | 统计图片中二维码数目
quirc_strerror|打印错误信息
quirc_flip|按照国际标准翻转二维码
quirc_decode|解码二维码
quirc_extract|提取二维码
quirc_resize|分配特定大小的quirc数据结构
quirc_version|提取二维码的版本
## 已测试功能
|接口名|功能|
|--|--|
quirc_count | 统计图片中二维码数目
quirc_strerror|打印错误信息
quirc_flip|按照国际标准翻转二维码
quirc_decode|解码二维码
quirc_extract|提取二维码
quirc_resize|分配特定大小的quirc数据结构
quirc_version|提取二维码的版本
## 原生库的功能逻辑
### qrtest测试逻辑
#### 对目录下图片文件处理
1. 读取图片文件
3. 检查图片中二维码数目n
4. 重复执行#5-19 n次
5. 提取二维码
6. 二维码解码
7. 如果解码失败进行#8-9
8. 翻转二维码
9. 再次解码
10. 存在二维码或者可以解码则进行#11-19
11. 提取信息
12. 打印字符形式二维码
13. 如果可以解码进行#14-19
14. 二维码解码
15. 如果错误进行#16-17
16. 翻转二维码
17. 二维码解码
18. 如果解码错误则打印错误信息
19. 否则打印解码数据
#### 对目录处理
1. 读取根目录
2. 对根目录下的所有项目执行测试#3-4
3. 如果是文件执行`对目录下图片文件处理`过程
4. 否则递归扫描目录
### inspect测试逻辑
1. 创建二维码数据结构
2. 检查二维码图片的格式
3. 如果格式不正确则退出并销毁数据结构
4. 否则结束创建二维码数据结构
5. 调用#6-14 检查二维码信息
6. 统计二维码数目n
7. 重复执行#8-14 n次
8. 二维码解码
9. 如果发生数据错误执行#10-11
10. 翻转二维码
11. 二维码解码
12. 打印出字符形式二维码
13. 如果解码错误则输出错误信息
14. 否则打印出解码数据
