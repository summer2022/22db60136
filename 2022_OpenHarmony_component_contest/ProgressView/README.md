# ProgressView

#### 项目介绍

    该组件是一个显示加载进度的OpenHarmony组件库，通常用于图片或文件加载进度显示

#### 项目演示

 ![输入图片说明](./screenshot/demo.gif)

##### 基础用法

   ```typescript
             //正常样式
             ProgressView({
                  progressStyle:new ProgressNormalStyle().setWidth(100).setHeight(100).setRoundCap(true),
                  makeProgressView:this.makeProgressView as Function,
                  onInflateComplete:(progressView:ProgressView)=>{
                  this.progressView1=progressView
                  }
              })

              //扇形样式
              ProgressView({
                  progressStyle:new ProgressFanStyle().setWidth(100)
                  .setHeight(100)
                  .setBackgroundColor('#AABBCC')
                  .setProgressColor('#AABBCC'),
                  makeProgressView:this.makeProgressView as Function,
                  onInflateComplete:(progressView:ProgressView)=>{
                  this.progressView2=progressView
                  }
              })

              //时钟样式
              ProgressView({
                  progressStyle:new ProgressClockStyle().setWidth(100).setHeight(100).setStrokeWidth(20),
                  makeProgressView:this.makeProgressView as Function,
                  onInflateComplete:(progressView:ProgressView)=>{
                  this.progressView3=progressView
                  }
              })

              //自定义loading样式
              ProgressView({
                  progressStyle:new ProgressRotateStyle().setWidth(100).setHeight(100).setRotateRes($r('app.media.loading')),
                  makeProgressView:this.makeProgressView as Function,
                  onInflateComplete:(progressView:ProgressView)=>{
                  this.progressView4=progressView
                  }
              })

              //水位样式
              ProgressView({
                  progressStyle:new ProgressLevelStyle().setWidth(100).setHeight(100).setProgressColor('#224466')
                  .setImageSrc('pages/qq.png'),
                  makeProgressView:this.makeProgressView2 as Function,
                  onInflateComplete:(progressView:ProgressView)=>{
                  this.progressView5=progressView
                  }
              })

   ```   

#### 接口及属性

- ProgressView属性

| **属性**                             | **必填** | **说明**                                                   |
| ------------------------------------ | :------: | --------------------------------------------------------- |
| makeProgressView: Function           |    是    | 构建进度显示组件                                     |
| onInflateComplete: Function          |    否    | 为回调给调用者一个ProgressView对象，方便做后续操作, 函数结构:(obj: ProgressView) => void      |
| progressStyle: BaseProgressStyle     |    是    | 进度样式                  |

- BaseProgressStyle属性

| **属性**                             | **必填** | **说明**                                                   |
| ------------------------------------ | :------: | --------------------------------------------------------- |
| width: number           |    否    | 宽度，默认96                                    |
| height: number          |    否    | 高度，默认96                                    |
| max: number             |    否    | 进度最大值，默认100                              |
| min: number             |    否    | 进度最小值，默认0                                |

#### 版权和许可信息

```
Copyright (c) 2022 gaojianming

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```


    