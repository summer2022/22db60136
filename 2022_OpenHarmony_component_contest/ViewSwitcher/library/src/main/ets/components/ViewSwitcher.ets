import {DefaultConfig} from './DefaultConfig'

/**
 * 视图切换动画
 */
@Component
export struct ViewSwitcher {
  public playIntervalTime: number = DefaultConfig.DEFAULT_INTERVAL_TIME;
  public duration: number = DefaultConfig.DEFAULT_ANIM_DURATION;
  public curve: Curve = DefaultConfig.DEFAULT_CURVE;
  public mData: Array<any>;
  public makeView: Function
  public width: Length = '100%';
  public height: Length = '100%';
  public isShowNextAnim: boolean = true;
  //此函数是为了回调给调用者一个ViewSwitcher，方便做后续操作
  public onInflateComplete: (obj: ViewSwitcher) => void
  //此函数是为了响应ViewSwitcher点击事件
  public onClickListener: (index: number) => void

  private translateHeight: number = 0;

  @State private index1: number = 0;
  @State private index2: number = 1;
  @State private translateY1: number = 0;
  @State private translateY2: number = 0;
  private intervalId: number;
  private sourceNextAnim: boolean;

  build() {
    List() {
      ListItem() {
        Stack() {
          //此处需要构建两个视图做切换动画
          Column() {
            ForEach([this.index1], this.makeView.bind(null, this.mData))
          }
          .translate({ y: this.translateY1 })
          .width('100%')
          .height('100%')
          .onClick(() => {
            if (typeof this.onClickListener == 'function') {
              this.onClickListener.apply(null, [this.index1])
            }
          })

          Column() {
            ForEach([this.index2], this.makeView.bind(null, this.mData))
          }
          .translate({ y: this.translateY2 })
          .width('100%')
          .height('100%')
          .onClick(() => {
            if (typeof this.onClickListener == 'function') {
              this.onClickListener.apply(null, [this.index2])
            }
          })

        }
      }
    }
    .onAreaChange((oldValue: Area, newValue: Area) => {
      if (typeof newValue.height == 'number') {
        this.translateHeight = newValue.height
      }
    })
    .edgeEffect(EdgeEffect.None)
    .enabled(false)
    .width(this.width)
    .height(this.height)
    .onAppear(() => {
      if (typeof this.onInflateComplete == 'function') {
        this.onInflateComplete.apply(null, [this])
      }
      if (this.isShowNextAnim) {
        this.translateY1 = 0
        this.translateY2 = this.translateHeight
      } else {
        this.translateY1 = 0
        this.translateY2 = -this.translateHeight
      }

    })
    .onDisAppear(() => {
      if (this.intervalId) {
        clearInterval(this.intervalId);
      }
    })

  }

  /**
   * 开始自动播放切换动画
   * @param isShowNextAnim 是否是向下做切换动画 true表示向下做切换动画，false表示向上做切换动画
   */
  public startAutoPlay(isShowNextAnim?: boolean) {
    if (typeof isShowNextAnim == 'boolean') {
      this.isShowNextAnim = isShowNextAnim
    }
    if (this.intervalId) {
      clearInterval(this.intervalId);
    }

    var _this = this;
    this.intervalId = setInterval(() => {
      this.startPlayAnim(_this);
    }, this.playIntervalTime)
  }

  /**
   * 停止自动播放
   */
  public stopAutoPlay() {
    console.log('stopAutoPlay')
    if (this.intervalId) {
      clearInterval(this.intervalId);
    }
  }

  /**
   * 开始播放动画切换效果
   */
  private startPlayAnim(_this) {
    animateTo({
      duration: this.duration,
      curve: this.curve,
      iterations: 1,
      playMode: PlayMode.Normal,
      onFinish: function () {
        if (_this.sourceNextAnim) {
          _this.isShowNextAnim = _this.sourceNextAnim
        }

        if (_this.isShowNextAnim) {
          if (_this.translateY1 != 0) {
            _this.translateY1 = _this.translateHeight
          }
          if (_this.translateY2 != 0) {
            _this.translateY2 = _this.translateHeight
          }
        } else {
          if (_this.translateY1 != 0) {
            _this.translateY1 = -_this.translateHeight
          }
          if (_this.translateY2 != 0) {
            _this.translateY2 = -_this.translateHeight
          }
        }

      }
    }, () => {

      if (this.isShowNextAnim) {
        this.translateY1 -= this.translateHeight
        this.translateY2 -= this.translateHeight
      } else {
        this.translateY1 += this.translateHeight
        this.translateY2 += this.translateHeight
      }

      if (this.translateY1 != 0) {
        let index = (this.isShowNextAnim ? this.index1 + 1 : this.index1 - 1) % this.mData.length
        if (index < 0) {
          index = this.mData.length - 1
        }
        this.index2 = index
      }
      if (this.translateY2 != 0) {
        let index = (this.isShowNextAnim ? this.index2 + 1 : this.index2 - 1) % this.mData.length
        if (index < 0) {
          index = this.mData.length - 1
        }
        this.index1 = index
      }

    })
  }

  /**
   * 显示上一个视图
   */
  public showPrevious() {
    this.sourceNextAnim = this.isShowNextAnim
    this.isShowNextAnim = false
    console.log('showPreview')
    if (this.translateY1 != 0) {
      this.translateY1 = -this.translateHeight
    }
    if (this.translateY2 != 0) {
      this.translateY2 = -this.translateHeight
    }
    this.startPlayAnim(this)
  }

  /**
   * 显示下一个视图
   */
  public showNext() {
    this.sourceNextAnim = this.isShowNextAnim
    this.isShowNextAnim = true
    console.log('showNext')
    if (this.translateY1 != 0) {
      this.translateY1 = this.translateHeight
    }
    if (this.translateY2 != 0) {
      this.translateY2 = this.translateHeight
    }
    this.startPlayAnim(this)
  }

}