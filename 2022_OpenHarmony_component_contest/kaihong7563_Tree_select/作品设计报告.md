# tree-select 分类选择组件（JS版本）

## 1、引入

```html
<element name='tree-select' src='../../common/components/treeSelect/treeSelect.hml'></element>
```

## 2、代码演示

- 组件支持显示的数据结构

```
items 整体为一个数组，数组内包含一系列描述分类的对象，每个分类里，text表示当前分类的名称，children表示分类里的可选项。数据格式如下。
```

```js
const items = [
    {
        text: '浙江',
        children: [
            {id: 1,text: '温州'},
            {id: 2,text: '杭州'}
        ]
    },
    {
        text: '江苏',
        children: [
            {id: 3,text: '南京'},
            {id: 4,text: '苏州'},
            {id: 5,text: '扬州'},
            {id: 6,text: '淮安'}
        ]
    },
    {
        text: '安徽',
        children: [
            {id: 7,text: '合肥'},
            {id: 8,text: '芜湖'},
            {id: 9,text: '淮南'}
        ]
    }
];
```

- 支持单选

```
main-active-index表示左侧高亮选项的索引，active-id表示右侧高亮选项的 id
```

```html
<!--  组件左侧导航 start  -->
<div class="tree-nav">
    <div for="{{ (index, item) in items }}"
         @click="clickNav(index)"
         class="tree-nav-item {{ index == mainActiveIndex ? 'tree-nav-active' : '' }}">
        <text>{{ item.text }}</text>
        <text class="dot" if="{{ item.dot }}"></text>
        <text class="info" if="{{ ! item.dot && item.info }}">{{ item.info }}</text>
    </div>
</div>
<!--  组件左侧导航 end  -->
```

```js
export default {
    data: {
        // 支持单选关联的数据
        single: {
            items, // item为分类显示所需的数据
            mainActiveIndex: 0, // main-active-index表示左侧高亮选项的索引
            activeId: 1 // active-id表示右侧高亮选项的 id
        },
    },

    /**
     * 点击左侧导航时触发
     * @param index - 被点击的导航的索引
     */
    changeNav1(index) {
        this.single.mainActiveIndex = index.detail;
        console.log(JSON.stringify(index.detail));
    },

    /**
     * 点击右侧选择项时触发
     * @param index - 该点击项的数据
     */
    getChecked1(data) {
        this.single.activeId = data.detail;
        prompt.showToast({
            message: JSON.stringify(data.detail)
        });
        console.log(JSON.stringify(data.detail));
    }
}
```

- 支持多选

```
active-id为数组格式时，可以选中多个右侧选项
```

```html
<!--  tree-select组件调用：支持多选 start  -->
<text class="txt">支持多选</text>
<tree-select items="{{ multiple.items }}"
             main-active-index="{{ multiple.mainActiveIndex }}"
             active-id="{{ multiple.activeId }}"
             @click-nav="changeNav2"
             @click-item="getChecked2">
</tree-select>
<!--  tree-select组件调用：支持多选 end  -->
```

```js
export default {
    data: {
        // 支持单选关联的数据
        multiple: {
            items, // item为分类显示所需的数据
            mainActiveIndex: 1, // main-active-index表示左侧高亮选项的索引
            activeId: [4, 5] // active-id为数组格式时，可以选中多个右侧选项
        },
    },

    /**
     * 点击左侧导航时触发
     * @param index - 被点击的导航的索引
     */
    changeNav2(index) {
        this.multiple.mainActiveIndex = index.detail;
        console.log(JSON.stringify(index.detail));
    },

    /**
     * 点击右侧选择项时触发
     * @param index - 该点击项的数据
     */
    getChecked2(data) {
        this.multiple.activeId = data.detail;
        prompt.showToast({
            message: JSON.stringify(data.detail)
        });
        console.log(JSON.stringify(data.detail));
    }
}
```

- 提示信息

```
设置dot属性后，会在图标右上角展示一个小红点
设置info属性后，会在图标右上角展示相应的徽标
```

```js
const dotItems = [
    {
        text: '江苏',
        children: [
            {id: 3,text: '南京'},
            {id: 4,text: '苏州'},
            {id: 5,text: '扬州'}
        ],
        info: "1"
    },
    {
        text: '安徽',
        children: [
            {id: 7,text: '合肥'},
            {id: 8,text: '芜湖'}
        ],
        dot: true
    }
];
```

## 3、API

- 属性

| 参数              | 说明                          | 类型                                  | 默认值 |
| ----------------- | ----------------------------- | ------------------------------------- | ------ |
| items             | 分类显示所需的数据            | item[]                                | []     |
| main-active-index | 左侧选中项的索引              | number \|string                       | 0      |
| active-id         | 右侧选中项的 id，支持传入数组 | number \|string \|(number \|string)[] | ""     |

- 事件

| 事件名     | 说明                 | 回调函数的参数                   |
| ---------- | -------------------- | -------------------------------- |
| click-item | 点击右侧选择项时触发 | data.detail: 该点击对象的id数据  |
| click-nav  | 点击左侧导航时触发   | index.detail：被点击的导航的索引 |

## 4、实例演示



![输入图片说明](https://images.gitee.com/uploads/images/2021/0917/102105_0aaaec29_9209365.gif "tree-select.gif")



# tree-select 分类选择组件（ETS版本）

## 5、引入

```ts
import TreeSelect from '../components/treeSelect'
```

## 6、代码演示

- 组件支持显示的数据类型

```ts
export interface itemProp {
  text: string,
  children: itemChildrenProp[],
  info?: string,
  dot?: boolean
}

export interface itemChildrenProp {
  id: string,
  text: string
}
```

- 组件支持显示的数据结构

```
items 整体为一个数组，数组内包含一系列描述分类的对象，每个分类里，text表示当前分类的名称，children表示分类里的可选项。数据格式如下。
```

```js
// 支持基本功能的数据
export const items: itemProp[] = [
  {
    text: '浙江',
    children: [
      { id: '1', text: '温州' },
      { id: '2', text: '杭州' }
    ]
  },
  {
    text: '江苏',
    children: [
      { id: '3', text: '南京' },
      { id: '4', text: '苏州' },
      { id: '5', text: '扬州' },
      { id: '6', text: '淮安' }
    ]
  },
  {
    text: '安徽',
    children: [
      { id: '7', text: '合肥' },
      { id: '8', text: '芜湖' },
      { id: '9', text: '淮南' }
    ]
  }
]
```

- 支持单选

```
main-active-index表示左侧高亮选项的索引，active-id表示右侧高亮选项的 id
```

```ts
import prompt from '@system.prompt'
import TreeSelect from '../components/treeSelect'
import { itemProp, items, dotItems } from '../utils/consts'

@Entry
@Component
struct Index {
  private items: itemProp [] = items // item为分类显示所需的数据（单选、多选）
  @State mainActiveIndex1: number = 0 // main-active-index表示左侧高亮选项的索引
  @State activeId1: string [] = ['1'] // activeId1 表示右侧高亮选项的 id

  /**
   * 点击右侧选择项时触发
   */
  private itemClick1 = () => {
    prompt.showToast({
      message: JSON.stringify(this.activeId1)
    })
  }

  build() {
    Column() {
      // tree-select组件调用：支持单选
      Text('支持单选').fontSize(24).width('100%').backgroundColor('#ccc').height(50)
      TreeSelect({
        items: this.items,
        mainActiveIndex: $mainActiveIndex1,
        activeId: $activeId1,
        itemClick: this.itemClick1
      })
    }
  }
}
```

- 支持多选

```
active-id为数组格式时，可以选中多个右侧选项
```

```ts
import prompt from '@system.prompt'
import TreeSelect from '../components/treeSelect'
import { itemProp, items, dotItems } from '../utils/consts'

@Entry
@Component
struct Index {
  @State mainActiveIndex2: number = 1 // main-active-index表示左侧高亮选项的索引
  @State activeId2: string [] = ['4', '5'] // activeId2 表示右侧高亮选项的 id

  /**
   * 点击右侧选择项时触发
   */
  private itemClick2 = () => {
    prompt.showToast({
      message: JSON.stringify(this.activeId2)
    })
  }

  build() {
    Column() {
      // tree-select组件调用：支持多选
      Text('支持多选').fontSize(24).width('100%').backgroundColor('#ccc').height(50)
      TreeSelect({
        items: this.items,
        mainActiveIndex: $mainActiveIndex2,
        activeId: $activeId2,
        multiple: true,
        itemClick: this.itemClick2
      })
    }
  }
}
```

- 提示信息

```
设置dot属性后，会在图标右上角展示一个小红点
设置info属性后，会在图标右上角展示相应的徽标
```

```ts
export const dotItems: itemProp[] = [
  {
    text: '江苏',
    children: [
      { id: '3', text: '南京' },
      { id: '4', text: '苏州' },
      { id: '5', text: '扬州' }
    ],
    info: '1'
  },
  {
    text: '安徽',
    children: [
      { id: '7', text: '合肥' },
      { id: '8', text: '芜湖' }
    ],
    dot: true
  }
]
```

## 7、API

- 属性

| 参数              | 说明                          | 类型       | 默认值 |
| ----------------- | ----------------------------- | ---------- | ------ |
| items             | 分类显示所需的数据            | item[]     | []     |
| main-active-index | 左侧选中项的索引              | string     | 0      |
| active-id         | 右侧选中项的 id，支持传入数组 | (string)[] | []     |
| multiple          | 是否支持多选                  | boolean    | false  |

- 事件

| 事件名     | 说明                 | 回调函数的参数 |
| ---------- | -------------------- | -------------- |
| click-item | 点击右侧选择项时触发 | 无             |

## 8、实例演示



![输入图片说明](https://images.gitee.com/uploads/images/2021/0917/102105_0aaaec29_9209365.gif "tree-select.gif")