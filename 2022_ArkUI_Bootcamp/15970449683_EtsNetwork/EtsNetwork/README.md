APP绝大多数都是需要网络请求来加载数据的，本项目案例利用鸿蒙ArkUI开发框架中的@ohos.net.http模块提供的网络请求相关API，
实现了查询输入城市的天气预报、查询新闻列表及新闻详情浏览两个案例应用，
同时本应用利用了ETS声明式开发范式的语法特点，实现了页面请求传参、组件循环渲染、网页加载、页面路由跳转等基础功能，
对于学习鸿蒙ETS的网络应用开发有一定的借鉴价值。

开发工具版本：DevEco Studio 3.0 Beta4 Build Version 3.0.0.991
项目配置：Compile SDK: 8     Language: eTS

src/main/ets/MainAbility下新建model文件夹,增加了两个数据模型文件NewsModel.ets  weatherModel.ets
src/main/resources/media下增加了两个图片,back.png和refresh.png