/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import router from '@ohos.router'
import {
  FoodInfo,
  MenuInfo,
  Category,
  MealCategory,
  MealCategoryId,
  CategoryId,
  MealTimeId,
  DietRecord
} from './DataModels'
import { mockMenus, mockFoods, mockDietRecords } from '../mock/MockData'

const DEBUG_PREVIEW = false
const MOCK_API = true

export function getMenus(): Array<MenuInfo> {
  return mockMenus
}

export function getFoods(): Array<FoodInfo> {
  return mockFoods
}

//export function getFoodInfo(): FoodInfo {
//  return DEBUG_PREVIEW ? mockFoodInfo : router.getParams()['foodId']
//}

// make records ordered by meal time
export let initDietRecords: Array<DietRecord> = [
  {
    id: -1,
    foodId: 0,
    mealTime: { name: $r('app.string.meal_time_breakfast'), id: MealTimeId.Breakfast },
    weight: 0
  },
  {
    id: -1,
    foodId: 0,
    mealTime: { name: $r('app.string.meal_time_lunch'), id: MealTimeId.Lunch },
    weight: 0
  },
  {
    id: -1,
    foodId: 0,
    mealTime: { name: $r('app.string.meal_time_dinner'), id: MealTimeId.Dinner },
    weight: 0
  },
  {
    id: -1,
    foodId: 0,
    mealTime: { name: $r('app.string.meal_time_supper'), id: MealTimeId.Supper },
    weight: 0
  }]

export function getDietRecords(): Array<DietRecord> {
  return DEBUG_PREVIEW ? initDietRecords.concat(mockDietRecords) :
  AppStorage.Get<Array<DietRecord>>('dietRecords')
}

export function getFoodCategories(): Category[] {
  return [{ id: CategoryId.Vegetable, name: $r('app.string.category_vegetable') },
          { id: CategoryId.Meat, name: $r('app.string.category_meat') },
          { id: CategoryId.Seafood, name: $r('app.string.category_seafood') },
          { id: CategoryId.Others, name: $r('app.string.category_others') }]
}

export function getMealCategories(): MealCategory[] {
  return [{ id: MealCategoryId.Breakfast, name: $r('app.string.category_breakfast') },
          { id: MealCategoryId.Lunch, name: $r('app.string.category_lunch') },
          { id: MealCategoryId.Dinner, name: $r('app.string.category_dinner') }]

}

export function getMileTimes(): string[] {
  if (MOCK_API) {
    return ['早餐', '午餐', '晚餐', '夜宵']
  } else {
    let mealTimeResources: Resource[] = [$r("app.string.meal_time_breakfast"), $r('app.string.meal_time_lunch'), $r('app.string.meal_time_dinner'), $r('app.string.meal_time_supper'), $r('app.string.category_dessert')]
    let mealTimes = []
    mealTimeResources.forEach(item => {
      let mealTime = this.context.resourceManager.getStringSync(item.id)
      if (mealTime !== '') {
        mealTimes.push(mealTime)
      }
    })
    return mealTimes
  }
}

export function getSortedFoodData(): Array<FoodInfo> {

  var foods = getFoods()

  var resultList = []
  for (var i = 0;i < foods.length; i++) {
    resultList.push(foods[i])
  }

  return resultList
}


export function getMenuData(): Array<MenuInfo> {

  var foods = getMenus()

  var resultList = []
  for (var i = 0;i < foods.length; i++) {
    resultList.push(foods[i])
  }

  return resultList
}

export function getMenuByFood(food : string): Array<MenuInfo> {

  var foods = getMenus()

  var resultList = []
  for (var i = 0;i < foods.length; i++) {
    if(foods[i].material.indexOf(food) > 0)
    resultList.push(foods[i])
  }

  return resultList
}

//
//export function statistics(): Array<OneMealStatisticsInfo> {
//  console.info('meal statistics')
//  let dietRecords = getDietRecords()
//  const mealMap = new Map()
//  dietRecords.forEach((item) => {
//    let oneMealStatisticsInfo: OneMealStatisticsInfo = mealMap.get(item.mealTime.id)
//    if (oneMealStatisticsInfo === undefined) {
//      oneMealStatisticsInfo = new OneMealStatisticsInfo(item.mealTime)
//    }
//    var foodInfo = getFoods().find((food) => {
//      return food.id === item.foodId
//    })
//    var calories = foodInfo.calories * item.weight
//    var protein = foodInfo.protein * item.weight
//    var fat = foodInfo.fat * item.weight
//    var carbohydrates = foodInfo.carbohydrates * item.weight
//    oneMealStatisticsInfo.mealFoods.push(new MealFoodInfo(item.id, foodInfo.name, foodInfo.image, calories, protein, fat, carbohydrates, item.weight))
//    oneMealStatisticsInfo.totalFat += fat
//    oneMealStatisticsInfo.totalCalories += calories
//    oneMealStatisticsInfo.totalCarbohydrates += carbohydrates
//    oneMealStatisticsInfo.totalProtein += protein
//    mealMap.set(item.mealTime.id, oneMealStatisticsInfo)
//  })
//
//  return Array.from(mealMap.values())
//}

export function updateDietWeight(recordId: number, weight: number) {
  let dietRecords = getDietRecords()
  let index = dietRecords.findIndex((record) => {
    return record.id === recordId
  })
  dietRecords[index].weight = weight
  AppStorage.SetOrCreate<Array<DietRecord>>('dietRecords', dietRecords)
}