import { OneMealStatisticsInfo } from '../../../model/DataModels'
import { BreakPointType } from '../../../utils/BreakpointSystem'

import { HeatHistogramContent } from './detail/heat_histogram_content'
import { NutritionHistogramContent } from './detail/nutrition_histogram_content'
import { MealCard } from './detail/meal_card'

@Component
export struct DietDetails {
    // 关联父组件Provide提供的数据
    @Consume("dietData") dietData: Array<OneMealStatisticsInfo>
    // 当前设备尺寸模式
    @StorageProp('currentBreakpoint') currentBreakpoint: string = 'sm'

    build() {
        Scroll() {
            Column() {
                Swiper() {
                    // 热量图表
                    HeatHistogramContent()
                    // 营养元素图表
                    NutritionHistogramContent()
                }
                .itemSpace(12)
                .height(400)
                .width('100%')
                .indicatorStyle({ selectedColor: $r('app.color.theme_color_green') })
                // 是否启用导航点指示器
                .indicator(new BreakPointType({ sm: true, md: false, lg: false }).getValue(this.currentBreakpoint))
                // 设置导航点样式
                .displayCount(new BreakPointType({ sm: 1, md: 2, lg: 2 }).getValue(this.currentBreakpoint))

                ForEach(this.dietData, (item) => {
                    // 饮食卡片
                    MealCard({ mealInfo: item })
                })
            }
        }
        .backgroundColor('#EDF2F5')
    }
}
