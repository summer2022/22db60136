import { HistogramLegend } from '../../../../model/DataModels'
import { Histogram } from './histogram'

@Component
export struct HeatHistogramContent {
    private legend: HistogramLegend[] = [
        new HistogramLegend('#FD9A42', $r("app.string.high_calorie")),
        new HistogramLegend('#73CD57', $r("app.string.medium_low_calories")),
    ]

    @Builder legendComponent(item) {
        Text(item.value).fontSize(12).fontColor('#18181A').fontFamily('HarmonyHeTi')
    }

    @Builder content(item) {
        Column() {
            Rect({ width: 14, height: item.totalCalories / 1000 + 14, radius: 14 })
                .fill(GetColor(item.totalCalories))
                .padding({ top: 7 })
                .margin({ bottom: -7 })
        }
        .clip(true)
    }

    build() {
        Row() {
            Histogram({
                title: $r("app.string.diet_record_calorie"),
                content: this.content,
                legend: this.legend,
                legendComponent: this.legendComponent
            })
        }
    }
}

function GetColor(value: number): Resource {
    if (value / 1000 > 100) {
        return $r("app.color.theme_color_orange")
    } else {
        return $r("app.color.theme_color_green")
    }
}
