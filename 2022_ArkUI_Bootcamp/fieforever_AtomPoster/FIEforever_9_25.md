# #HarmonyOS ArkUI入门训练营—健康生活实战#邮印象ArkUI版

## 一、背景
### 1、项目简介
邮印象是一个为用户提供照片冲印、相册、照片书、同学录、文化衫、马克杯、个性邮册等几十款产品个性化定制服务的C2B平台,是一个基于传统函件特有产品及寄递优势，利用互联网手段，满足用户通过实物介质实现情感纪念与传递的线上平台。
### 2、程序介绍
邮印象ArkUI版是基于邮印象项目，使用HarmonyOS ArkUI开发的HarmonyOS应用程序。在学习了《HarmonyOS ArkUI入门训练营之健康生活实战》后，结合邮印象本身的个性化定制功能进行ArkUI的开发。目前版本的功能还比较初步，后续有机会会继续完善开发。
## 二、功能
### 1、启动动画页面
演示：

![](https://alliance-communityfile-drcn.dbankcdn.com/FileServer/getFile/cmtybbs/227/622/407/0260086000227622407.20220925031126.80091287830406090156390497430866:50530924015210:2800:DC39F352FA327366C3F10088199C4D9E35E31260E5D44C913CEAD94D8495978C.gif)

核心代码：

```javascript
Shape() {
        Path()
          .commands(this.leftPath)
          .fill('none')
          .linearGradient({ angle: 90, colors: [['#fd0c20', 0], ['#ffffff', 1]] })
          .clip(new Path().commands(this.leftPath))

        Path()
          .commands(this.leftBottomPath)
          .fill('none')
          .linearGradient({ angle: 50, colors: [['#8d0c20', 0.1], ['#fd0c20', 0.4], ['#ffffff', 0.7]] })
          .clip(new Path().commands(this.leftBottomPath))

      }
      .height(210)
      .width(210)
      .scale({ x: this.scaleValue, y: this.scaleValue })
      .opacity(this.opacityValue)
      .onAppear(() => {
        animateTo({
          duration: 1000,
          curve: curves.cubicBezier(0.4, 0, 1, 1),
          delay: 100,
          onFinish: () => {
            setTimeout(() => {
              router.replace({ uri: 'pages/index' })
            }, 1000)
          }
        }, () => {
          this.opacityValue = 1
          this.scaleValue = 1
        })
      })

      Text($r("app.string.entry_MainAbility"))
        .fontSize(26)
        .fontColor(Color.White)
        .margin({ top: 300 })

      Text($r("app.string.mainability_description"))
        .fontSize(17)
        .fontColor(Color.White)
        .margin({ top: 4 })
```

### 2、首页-产品列表-产品详情
演示：

![](https://alliance-communityfile-drcn.dbankcdn.com/FileServer/getFile/cmtybbs/227/622/407/0260086000227622407.20220925031302.10247039748999009878206240634798:50530924015210:2800:9DD319F0CBD1B9E0B8CF5A2F44A3284A326F521806C8AE1725F2F30C94D54E49.gif)

核心代码：


```javascript
轮播组件Swiper
@Component
export struct SubscribeSwiper {
  private index: number = 0

  build() {
    Swiper() {
      ForEach(subscribeItems, item => {
        SwiperItem({ index: item.id })
      }, item => item.title)
    }
    .width('100%')
    .height('203')
    .indicator(false)
    .index(this.index)
    .autoPlay(true)
    .itemSpace(15)
    .displayMode(SwiperDisplayMode.AutoLinear)
  }
}
```

```javascript
底部导航栏
@Component
export struct BottomTabs {
  private tabSrc: number[] = [0, 1, 2]
  private backgroundColor: string = '#F1F3F5'
  private controller: TabsController = new TabsController()
  private tips: string = 'Tab'
  @Link bottomTabIndex: number

  build() {
    Flex({ direction: FlexDirection.Row, alignItems: ItemAlign.Center, justifyContent: FlexAlign.SpaceEvenly }) {
      ForEach(this.tabSrc, item => {
        Column() {
          Image(getTabSrc(this.bottomTabIndex, item))
            .objectFit(ImageFit.Contain)
            .width('60%').height('60%')

          Text(getTabStrSrc(item))
            .fontSize(14)
            .fontColor(getTabTextColor(this.bottomTabIndex, item))
        }
        .onClick(() => {

          this.controller.changeIndex(item)
        })
      }, item => item.toString())
    }
    .width('100%').height('8%')
    .backgroundColor(this.backgroundColor)
  }
}
```

### 3、照片书模板
演示：

![](https://alliance-communityfile-drcn.dbankcdn.com/FileServer/getFile/cmtybbs/227/622/407/0260086000227622407.20220925032049.08111407815789733742843360580399:50530924015210:2800:4B6AFF20197D3668A4B52B4561D30D7F5DB247118F0DF89579BE869B58E01D3A.gif)

核心代码：


```javascript
GridView布局
@Component
struct GridItemView {
  private gridItem: SubscribeData;

  build() {
    Column({space: 10})  {
      Image(this.gridItem.image)
        .objectFit(ImageFit.Auto)
        .height(152)
        .width('100%')
      Text(this.gridItem.title)
        .fontSize(15)
        .fontColor(Color.Black)
        .margin(20)
        .textAlign(TextAlign.Center)
    }
    .onClick(() => {
      router.push({
        uri: 'pages/homePicture',
        params: { imageUrl: this.gridItem.image, imageId: this.gridItem.image.id.toString() }
      })
    })
    .backgroundColor(Color.White)
  }
}
```

### 4、产品列表样式切换
演示：

![](https://alliance-communityfile-drcn.dbankcdn.com/FileServer/getFile/cmtybbs/227/622/407/0260086000227622407.20220925032749.02258440510493413576074916562969:50530924015210:2800:184329BA5147C02CAD59570507558CB112C2F03F5BC7B60D81F8676831926F1B.gif)

核心代码：

```javascript
懒加载
Scroll() {
      Grid() {
        LazyForEach(new MyDataSource(this.productItems), (item: ProductData, index) => {
          GridItem() {
            ProductGridItem({ productItem: item })
          }
//          .key('productGridItem' + (index + 1))
        }, (item: ProductData) => item.id.toString())
      }
      .rowsTemplate(this.gridRowTemplate)
      .columnsTemplate('1fr 1fr')
      .columnsGap(8)
      .rowsGap(8)
      .height(this.heightValue)
    }
    .scrollBar(BarState.Off)
    .padding({ left: 16, right: 16 })
```


```javascript
切换样式
Stack({ alignContent: Alignment.TopEnd }) {
        if (this.showList) {
          ProductList({ productItems: this.productItems })
        } else {
          ProductCategory({ productItems: this.productItems })
        }
        Image($r('app.media.Switch'))
          .height(40)
          .width(24)
          .objectFit(ImageFit.Contain)
//          .key('switch')
          .margin({ top: 15, right: 10 })
          .onClick(() => {
            this.showList = !this.showList
          })
      }
```

# 三、总结
## 【开发日记】
在学习开发的过程中，也在论坛同步记录了一下【开发日记】系列，方便自己以后回看和学习记录，有兴趣可以也看看：
- [【开发日记】-ArkUI实现沉浸式体验的状态栏（全屏）](https://developer.huawei.com/consumer/cn/forum/topic/0202991641782410291?fid=0101587866109860105)

## 代码仓
本工程以放在我的gitee仓里[【邮印象ArkUI代码仓】](https://gitee.com/FIEforever/contest/tree/master/2022_ArkUI_Bootcamp/fieforever_my_ArkUI_project)，欢迎来查看指导。

## 小结
经过实战课程：《HarmonyOS ArkUI入门训练营-健康生活实战》的学习，已经开始初步上手ArkUI的开发，其跨平台适配和简洁的编程语言给我留下了深刻的印象，感谢华为终端BG高级开发工程师的细心讲解。希望以后有机会继续学习ArkUI的课程，来慢慢完善邮印象ArkUI版。