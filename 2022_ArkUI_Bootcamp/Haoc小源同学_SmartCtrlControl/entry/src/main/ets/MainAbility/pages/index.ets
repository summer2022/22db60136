import router from '@ohos.router';
import { cardItem, TextRow } from '../utils/componentUtil'
import { getFullYear, getDate, getTime, getSeconds } from '../utils/functionUtil'

@Entry
@Component
struct Index {
  @State currentDate: string = getFullYear() + getDate()
  @State currentTime: string = getTime() + getSeconds()
  @State temp: number = 0
  @State humidity: number = 0
  @State isNight: boolean = false
  quality: string = '优'

  onPageShow() {
    setInterval(() => {
      let date = new Date
      // 判断是否为夜晚
      this.isNight = date.getHours() >= 22 ? true : false
      // 获取当前时间
      this.currentTime = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':' + (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ':' + (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds())
      // 判断是否为新的一天
      if (date.getHours() == 0) {
        this.currentDate = date.getFullYear() + '年' + (date.getMonth() + 1) + '月' + date.getDate() + '日'
      }
    }, 1000)
  }

  build() {
    Row() {
      Flex({ direction: FlexDirection.Column, justifyContent: FlexAlign.SpaceEvenly, alignItems: ItemAlign.Center }) {
        Flex({ justifyContent: FlexAlign.SpaceAround }) {
          if (this.isNight) {
            Image($r('app.media.moon'))
              .width(100)
              .height(100)
              .objectFit(ImageFit.None)
          } else {
            Image($r('app.media.sun'))
              .width(100)
              .height(100)
              .objectFit(ImageFit.None)
          }
          Text(this.currentDate + '\n' + this.currentTime)
            .fontSize(36)
            .fontColor(Color.White)
        }
        .width('100%')

        TextRow({ text: '温度', value: this.temp.toFixed(0) + '℃' })
        TextRow({ text: '空气质量', value: this.quality })
        TextRow({ text: '空气湿度', value: this.humidity.toFixed(0) + "%RH" })
      }
      .margin({ left: 30 })
      .height('88%')
      .width('45%')
      .borderRadius(30)
      .backgroundColor($r('app.color.background_blue'))
      .onClick(() => {
        router.push({
          url: 'pages/ThermometerDeviceControl',
        })
      })

      Column() {
        Row() {
          cardItem({ text: $r('app.string.smartLight'), url: 'pages/LightDeviceControl' })
          cardItem({ text: $r('app.string.smartCurtain'), url: 'pages/CurtainDeviceControl' })
        }

        Row() {
          cardItem({ text: '视频播放', url: 'pages/VideoPlayer' })
          cardItem({ text: '家庭备忘录', url: 'pages/FamilyNote' })
        }
        .margin({ top: 25 })
      }
    }
    .width('100%')
    .height('100%')
    .backgroundImage(this.isNight ? $r('app.media.bgDot_night') : $r('app.media.bgDot_day'))
    .backgroundImageSize(ImageSize.Cover)
  }
}