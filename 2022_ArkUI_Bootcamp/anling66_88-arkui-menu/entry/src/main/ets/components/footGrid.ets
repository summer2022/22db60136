import router from '@ohos.router'
import curves from '@ohos.curves'

import { FoodInfo, Category } from '../model/TypeModels'
import { getFoods, getFoodCategories } from '../model/DataUtil'


@Component
export struct FoodCategory {
  @State currentTabIndex: number = 0
  private foodItems: FoodInfo[] = getFoods()
  private foodCategories: Category[] = getFoodCategories()

  @Builder tabBarItemBuilder(value: Resource, index: number) {
    Text(value)
      .fontColor(this.currentTabIndex === index ? 'rgba(0,0,0,0.9)' : 'rgba(0,0,0,0.6)')
      .fontSize(this.currentTabIndex === index ? 24 : 18)
      .margin({ top: 2 })
      .height(56)
  }

  build() {
    Tabs() {
      TabContent() {
        FoodGrid({ foodItems: this.foodItems })
      }.tabBar(this.tabBarItemBuilder($r('app.string.all_label'), 0))

      ForEach(this.foodCategories, (foodCategory: Category, index) => {
        TabContent() {
          FoodGrid({ foodItems: this.foodItems.filter(item => (item.categoryId === foodCategory.id)) })
        }.tabBar(this.tabBarItemBuilder(foodCategory.name, index + 1))
      })
    }
    .animationDuration(0)
    .barMode(BarMode.Scrollable)
    .onChange((index) => {
      this.currentTabIndex = index
    })
  }
}


@Component
struct FoodGrid {
  private foodItems: FoodInfo[]
  private gridRowTemplate: string = ''
  private heightValue: number

  aboutToAppear() {
    // 计算行数
    var rows = Math.round(this.foodItems.length / 2);
    // 重复多少个 1fr
    this.gridRowTemplate = '1fr '.repeat(rows);
    // 计算总高度
    this.heightValue = rows * 192 - 8;
  }

  build() {
    Scroll() {
      Grid() {
        ForEach(this.foodItems, (item: FoodInfo) => {
          GridItem() {
            FoodGridItem({ foodItem: item })
          }
        }, (item: FoodInfo) => item.id.toString())
      }
      .rowsTemplate(this.gridRowTemplate)
      .columnsTemplate('1fr 1fr') // 显示两列
      .columnsGap(8)  // 相隔8vp
      .rowsGap(8)  // 相隔8vp
      .height(this.heightValue) // 高度
    }
    .scrollBar(BarState.Off)
    .padding({ left: 16, right: 16 })
  }
}


@Component
struct FoodGridItem {
  private foodItem: FoodInfo

  build() {
    Column() {
      // 显示食物图片
      Row() {
        Image(this.foodItem.image)
          .objectFit(ImageFit.Contain)
          .height(152)
          .width('100%')
          .sharedTransition(this.foodItem.letter, { duration: 400, curve: curves.cubicBezier(0.2, 0.2, 0.1, 1.0), delay: 100})
      }.backgroundColor('#FFf1f3f5')
      // 显示食物名称和卡路里
      Flex({ justifyContent: FlexAlign.Start, alignItems: ItemAlign.Center }) {
        // 食物名称宽度占剩余空间
        Text(this.foodItem.name)
          .fontSize(14)
          .flexGrow(1)
          .padding({ left: 8 })
        // 卡路里宽度为文本长度
        Text($r('app.string.kcal_key', this.foodItem.calories.toString()))
          .fontSize(14)
          .margin({ right: 6 })
      }
      .height(32)
      .width('100%')
      .backgroundColor('#FFe5e5e5')
    }
    .height(184)
    .width('100%')
    .onClick(() => {
      router.push({ url: 'pages/foodDetail', params: { foodId: this.foodItem } })
    })
  }
}
