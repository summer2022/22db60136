import { Category, FoodInfo } from '../model/TypeModels'

@Component
export struct Nutrition {
  @Link foodItemObj: FoodInfo

  build() {
    Column({space: 30}) {
      Text($r('app.string.nutrition_key')).layoutWeight(1).fontSize(20)
        .fontWeight(700)
        .align(Alignment.Start)
        .padding({left: 10})
        .width('100%')

      Row({space: 10}) {
        Column({space: 10}) {
          Stack() {
            Progress({value: 50, total: 100, style: ProgressStyle.Ring})
              .style({strokeWidth: 10})
              .color(Color.Orange)
              .backgroundColor('#eeeeee')
            Text('50%').fontSize(17)
          }
          .width(100)
          .margin(0)

          Text($r('app.string.protein_key'))
          Text($r('app.string.gram_key', this.foodItemObj.protein.toString()))
        }

        Column({space: 10}) {
          Stack() {
            Progress({value: 10, total: 100, style: ProgressStyle.Ring})
              .style({ strokeWidth: 10})
              .color(Color.Yellow)
              .backgroundColor('#eeeeee')
            Text('10%').fontSize(17)
          }
          .width(100)
          .margin(0)

          Text($r('app.string.fat_key'))
          Text($r('app.string.gram_key', this.foodItemObj.fat.toString()))
        }

        Column({space: 10}) {
          Stack() {
            Progress({value: 38, total: 100, style: ProgressStyle.Ring})
              .style({strokeWidth: 10})
              .color('#78d05c')
              .backgroundColor('#eeeeee')
            Text('38%').fontSize(17)
          }
          .width(100)
          .margin(0)

          Text($r('app.string.carbohydrates_key'))
          Text($r('app.string.gram_key', this.foodItemObj.carbohydrates.toString()))
        }
      }
      .alignSelf(ItemAlign.Center)
      .align(Alignment.Center)
      .alignItems(VerticalAlign.Center)
      .justifyContent(FlexAlign.Center)

    }
    .width('100%')
    .height(280)
    .borderRadius(20)
    .backgroundColor('#ffffff')
    .padding({top: 10, bottom: 10, left: 10, right: 10})
  }
}
