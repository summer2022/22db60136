第一次参加训练营,感谢这次训练营让我有机会自己动手制作了一只小贱钟

# 学习心得及参与原因
---
一直以来就对鸿蒙系统比较感兴趣,但一直没有机会上手,这次在立创EDA公众号上看到了本次训练营的有关信息,这是第一次接触OpenHarmony系统,训练营的老师们通过直播手把手教学,让我完成了从电路设计到软件设计的项目开发全过程,让我对OpenHarmony有了基础的认知,在老师的帮助下,我整个学习的流程少走了不少的弯路
其次也要感谢嘉立创提供的PCB打样卷和元器件卷,在嘉立创的支持下,设计的电路板很快就能到手.


# 参与过程
---

## **外型设计**:
使用SoildWorks进行建模

![小贱钟3D模型](https://gitee.com/wuchengtao1/contest/raw/master/2022_itcast_LCEDA_OpenHarmony_camp/plotclock%E5%B0%8F%E8%B4%B1%E9%92%9F/%E5%9B%BE%E7%89%87/%E5%B0%8F%E8%B4%B1%E9%92%9F3D%E6%A8%A1%E5%9E%8B.png)


在常规的plotclock后面加了个龟壳,用来放电路板以及电池,加个盖子将旋转编码器引出,实现小贱钟的自定义书写

## **元器件选型**:

主控采用传智HI3861鸿蒙IOTWiFi开发模组，这个是训练营指定的模组，不能改。

![HI3861](https://gitee.com/wuchengtao1/contest/raw/master/2022_itcast_LCEDA_OpenHarmony_camp/plotclock%E5%B0%8F%E8%B4%B1%E9%92%9F/%E5%9B%BE%E7%89%87/HI3861.png)

控制芯片采用PCA9685，这个芯片可以提供16路PWM，目前只用到8路，多出的引脚引出便于以后升级。

![PCA9685](https://gitee.com/wuchengtao1/contest/raw/master/2022_itcast_LCEDA_OpenHarmony_camp/plotclock%E5%B0%8F%E8%B4%B1%E9%92%9F/%E5%9B%BE%E7%89%87/PCA9685.jpg)

舵机采用SG90舵机,价格便宜性能稳定。
![SG90](https://gitee.com/wuchengtao1/contest/raw/master/2022_itcast_LCEDA_OpenHarmony_camp/plotclock%E5%B0%8F%E8%B4%B1%E9%92%9F/%E5%9B%BE%E7%89%87/SG90.png)


电源由于芯片是3.3V的，舵机是5V的，因此我采用7.4V锂电池，降压到5V，然后降压到3.3V分别给芯片和舵机供电。

7.4V 电池

![7.4V电池](https://gitee.com/wuchengtao1/contest/raw/master/2022_itcast_LCEDA_OpenHarmony_camp/plotclock%E5%B0%8F%E8%B4%B1%E9%92%9F/%E5%9B%BE%E7%89%87/7.4V%E7%94%B5%E6%B1%A0.png)

LDO线性稳压器(7.4V转5V)

LM-1084S-5.0

C259973(立创商城编号)

![LDO](https://gitee.com/wuchengtao1/contest/raw/master/2022_itcast_LCEDA_OpenHarmony_camp/plotclock%E5%B0%8F%E8%B4%B1%E9%92%9F/%E5%9B%BE%E7%89%87/LDO.png)

AMS1117-3.3 (5V转3V3)

C347222(立创商城编号)

![AMS1117-3.3](https://gitee.com/wuchengtao1/contest/raw/master/2022_itcast_LCEDA_OpenHarmony_camp/plotclock%E5%B0%8F%E8%B4%B1%E9%92%9F/%E5%9B%BE%E7%89%87/AMS1117-3.3.png)

## **原理图设计**:

![原理图](https://gitee.com/wuchengtao1/contest/raw/master/2022_itcast_LCEDA_OpenHarmony_camp/plotclock%E5%B0%8F%E8%B4%B1%E9%92%9F/%E5%9B%BE%E7%89%87/%E5%8E%9F%E7%90%86%E5%9B%BE.png)

## **电路板绘制**:
![电路板](https://gitee.com/wuchengtao1/contest/raw/master/2022_itcast_LCEDA_OpenHarmony_camp/plotclock%E5%B0%8F%E8%B4%B1%E9%92%9F/%E5%9B%BE%E7%89%87/%E7%94%B5%E8%B7%AF%E6%9D%BF.png)

# 实物展示
---

项目已开源至[plotclock自定义书写小贱钟][https://oshwhub.com/iwichbtfy/yi-zhi-gou-tui-zi]
![实物](https://gitee.com/wuchengtao1/contest/raw/master/2022_itcast_LCEDA_OpenHarmony_camp/plotclock%E5%B0%8F%E8%B4%B1%E9%92%9F/%E5%9B%BE%E7%89%87/%E5%AE%9E%E7%89%A9.jpg)