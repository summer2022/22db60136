## 学习心得——小贱钟

碍于自己是第一次参加嘉立创的训练营，并且对于绘制PCB等相关操作都不是很熟悉。故选择了基础营（小贱钟）。本次训练营大部分的元器件及PCB均由嘉立创、立创商城给予支持。



## 制作开始

####1.根据b站立创EDA的直播课及回放视频绘制原理图及PCB

将主要的元器件放置在原理图上，用矩形划分不同分区。随后放置外围元件，各元件之间的引脚利用网络连接。

####2.将PCB文件交由审核团队审核

提交报名表

####3.板厂打样

嘉立创EDA可以直接将文件发给嘉立创制PCB。

####4.焊接与烧录程序

焊接加焊油可以令焊点光滑圆润，解决密集引脚的连锡问题。先小后大，先贴片后插件的顺序焊接。

####5.组装

## 问题与分析

1.在绘制PCB时要注意此处的电流，并且要使布线的粗细与铜箔相匹配

2.对于易发热的元器件应设计散热孔

3.焊接完成后要检查是否有虚焊、短路等情况

4、对于舵机角度与直角坐标的正反解在教程中也有说明，学习之后基本可以看懂源码

5.注意舵机的线序

项目链接：[原工程](https://oshwhub.com/lhh233/yi-ge-xiao-jian-zhong)