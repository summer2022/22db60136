首先感谢这次训练营让我有机会和动力自己动手制作一只机器狗

## 参加训练营原因
- 我想没有人能够拒绝一只能遥控又会动的东西,更何况是自己动手制作的
- 最近一直对 4G遥控车很感兴趣,虽然机器狗速度比不上遥控车,但是它和机器狗一样都能动,所以刚好适合用来验证想法

## 拆解分析
- 有了做 4G 遥控的想法以后就开始考虑拆解分析如何一步步实现
- 机器狗本体: 学习训练营课程制作
- 4G 遥控: 由于对应硬件设计比较陌生,实现 4G 遥控需要 4G模块,摄像头模块,语音模块对我来说比较困难,所以选择用旧手机搭配自己制作 APP 来代替这些工作

## 基本功能
- 机器狗可以进行前进 后退 左转 右转行走,同时可以根据每条腿的舵机旋转角度做出各种动作
- 使用手机APP远程控制和实时音视频通话
- 使用手表APP手势控制机器狗的行走
## 硬件部分
### 电路板
- 主控使用鸿蒙 HI3861搭配 PCA9685 进行多路舵机控制
-  电路按照训练营课程绘制,为了方便控制电池电量,加了一个船型开关焊盘
### 外壳
- 外壳参考训练营课程内容使用 SW 设计
- 为了方便放手机把前后左右挡板顶部加高了一部分
- 为了安装船型开关底部开了一个长方形槽,但是没考虑到厚度问题,船型开关安装的不是特别牢固.
### 其他
- 机器狗背部放置旧手机,提供音视频通话和网络热点
- 为了平放手机时能拍到前方画面,在手机摄像头位置加了一个三棱镜改变光的角度
- 狗脚加了创可贴防滑

## 软件部分
### 固件
- 在训练营提供的固件基础上修改主程序,改为 TCP 客户端接收指令,参考如下
     ```
       //创建socket
        if ((sock_fd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
        {
            perror("create socket failed!\r\n");
            exit(1);
        }

        //初始化预连接的服务端地址
        send_addr.sin_family = AF_INET;
        send_addr.sin_port = htons(TCP_PORT);
        // send_addr.sin_addr.s_addr = inet_addr(TCP_SERVER_ADRESS);
        addr_length = sizeof(send_addr);

        connect(sock_fd, (struct sockaddr *)&send_addr, addr_length);

        while (1)
        {
            bzero(recv_buf, sizeof(recv_buf));
            int recv_len;
            recv_len = recv(sock_fd, recv_buf, sizeof(recv_buf), 0);
            if(recv_len == -1)
            {
                //TODO::处理掉线
                break;
            }
            // printf("len: %d data: %s\r\n", recv_len, recv_buf);
            dog_execJson(recv_buf,recv_len);
        }
        //关闭这个 socket
        closesocket(sock_fd);
    ```

- 在解析 json 位置加了校验防止接收到非 json 字符串时重启
  ```
   typejson = cJSON_GetObjectItem(recv_json, "type");
    if (typejson) {
        ....
    }
  ```

### 控制端
#### 手机APP
- APP使用Swift开发,其中上半部分为视频显示,同时支持语音对讲,也就是音视频通话,由于没人一起测试所以音频通话部分视频里没有展示.
- 使用虚拟摇杆控制机器狗四个方向行走
- 同时提供了四个按钮进行控制机器狗的四个预设姿势.

#### 手表 APP
- 使用手表的陀螺仪数据判断手势,上抬前进,下摆后退,手腕左右转控制机器狗左右转向
- 界面只简单显示了一下识别结果就不截图了,使用效果参看视频
### 后台服务
- 音视频通话(WebRTC)
- 信令服务由 swift 编写,主要作用提供一个 websocket 服务对设备进行配对
- TURN 使用coturn
### 远程控制
- 控制服务为 golang 编写,简单搭建一个 tcp 服务,用于把收到的指令消息转发给机器狗,代码参考如下
   ```
  for {
        reader := bufio.NewReader(conn)
        message, err := reader.ReadString('\n')

        if err != nil {
            break
        }

        for item := range openConnections {
            if item != conn {
                item.Write([]byte(message))
            }
        }
    }
  ```
- 为了解决手表不能使用 tcp 直接通信的问题,在里面加了一个 http 服务转发.
### 端口转发
- 由于没有服务器,所以以上服务全是在本地运行,通过路由器端口映射出去(需要有公网 IP)供机器狗和手机远程访问

## 实物展示
- ![D0748C0ABE0706CEF5664B71DD274238.jpg](https://image.lceda.cn/pullimage/qUl4bDgggIpyAdLp7H9TCNUP5bsQccIfacGAGfKd.jpeg)

- [点击查看 B 站视频](https://www.bilibili.com/video/BV1P34y1n78f/)
