#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include "utils_file.h"
#include "genki_env_base.h"
#include "genki/switcher.h"

static void save_html(const char *page, const char *html) {
    int fd = UtilsFileOpen(page, O_CREAT_FS | O_TRUNC_FS | O_WRONLY_FS, 0);

    if (fd > 0) {
        UtilsFileWrite(fd, html, strlen(html));
        UtilsFileClose(fd);
    }
}

static int doHtml(genki_web_request_t *request, genki_web_response_t *response) {
    if (request->method != GET) {
        return -1;
    }
    const char *page = "switch.html";
    unsigned int size;
    char buf[128];
    if (UtilsFileStat(page, &size) >= 0) {
        // 读取文件缓存
        sprintf(buf, "%d", size);
        response->setHeader(response, "Content-Length", buf);

        int fd = UtilsFileOpen(page, O_RDONLY_FS, 0);
        if (fd > 0) {
            unsigned char num;
            while ((num = UtilsFileRead(fd, buf, 128)) > 0) {
                response->write(response, buf, num);
            }
            UtilsFileClose(fd);
        } else {
            sprintf(buf, "<html><body><h2 id='a'>ERROR</h2></body></html>");
            response->write(response, buf, strlen(buf));
        }
    } else {
        // 从内存加载
        const char* html = "<!DOCTYPE html><html lang=\"en\"><head><meta charset=\"UTF-8\"><title>传智鸿蒙Switch</title><style> body {text-align: center;}  table {width: 400px;height: 400px;margin: 0 auto;}  button {height: 10rem;line-height: 10rem;padding: 0 3rem 0 3rem;font-size: 5rem;}</style></head><body><h2>传智鸿蒙开关控制</h2><br><table><tr><td><button id=\"close\">关</button></td></tr><tr><td><button id=\"restore\">丨</button></td></tr><tr><td><button id=\"open\">开</button></td></tr></table><script type=\"application/javascript\"> function btn_init(btn, fn) {btn.addEventListener('click', fn);}function net(path) {console.log(path);let url = path;let xhr = new XMLHttpRequest();xhr.onreadystatechange = function () {if (this.readyState === 4 && this.status === 200) {console.log(xhr.response);}};xhr.open(\"GET\", url, true);xhr.send();}function fn_open() {net(\"/switch/open\");}function fn_restore() {net(\"/switch/restore\");}function fn_close() {net(\"/switch/close\");}let btnOpen = document.getElementById('open');let btnRestore = document.getElementById('restore');let btnClose = document.getElementById('close');btn_init(btnOpen, fn_open);btn_init(btnRestore, fn_restore);btn_init(btnClose, fn_close);</script></body></html>";
        size_t len = strlen(html);

        char buf[128];
        sprintf(buf, "%d", len);
        response->setHeader(response, "Content-Type", "text/html; charset=UTF-8");
        response->setHeader(response, "Content-Length", buf);
        response->write(response, html, len);
        save_html(page, html);
    }
    return 1;
}

static int doClose(genki_web_request_t *request, genki_web_response_t *response) {
    GenkiSwitch_switch_close();
    return 1;
}
static int doRestore(genki_web_request_t *request, genki_web_response_t *response) {
    GenkiSwitch_switch_restore();
    return 1;
}
static int doOpen(genki_web_request_t *request, genki_web_response_t *response) {
    GenkiSwitch_switch_open();
    return 1;
}
genki_web_service_t *newWebService(void) {


    genki_web_service_t *service = genki_web_newService("SWITCH");
    service->addFilter(service, "/switch", doHtml);
    service->addFilter(service, "/switch/close", doClose);
    service->addFilter(service, "/switch/restore", doRestore);
    service->addFilter(service, "/switch/open", doOpen);
    service->link_name = "开关控制";
    service->link_url = "/switch";
    return service;
}