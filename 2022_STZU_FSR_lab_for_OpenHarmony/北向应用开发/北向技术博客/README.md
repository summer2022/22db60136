# 北向应用开发技术博客

* [【FFH】关于鸿蒙的一些事](https://ost.51cto.com/posts/10200)
* [【FFH】BearPi开发板上部署Hap工程](https://ost.51cto.com/posts/10220)
* [【FFH】北向开发JS UI自定义组件详细解读](https://ost.51cto.com/posts/10245)
  * [源码Codelabs-JSCustomComponent](../北向Codelabs)
* [【FFH】小熊派北向通过JS自定义接口调用南向驱动(以点亮LED为例)](https://ost.51cto.com/posts/10458)
  * [源码Codelabs-JSApi_LED](../北向Codelabs)

* [【FFH】如何配置横屏PageAbility，开发横屏应用](https://ost.51cto.com/posts/10485)

* [【FFH】JS API简单三步完成组网内设备拉起](https://ost.51cto.com/posts/10689)
  * [源码Codelabs-distrubtedPullingUp](../北向Codelabs)

* [【FFH】分布式数据服务简单实现(OpenHarmony JS UI)](https://ost.51cto.com/posts/13780)
  * [源码Codelabs-distrubutedDataDemo](../北向Codelabs)

* [【FFH】分布式任务调度应用拉起实现(OpenHarmony JSUI）](https://ost.51cto.com/posts/13891)
  * [源码Codelabs-remoteStartFADemo](../北向Codelabs)

* [【FFH】AI作诗之httpRequest实战](https://ost.51cto.com/posts/14671)
  * [源码Codelabs-AIPoetry](../北向Codelabs)
* [【FFH】实时聊天室之WebSocket实战](https://ost.51cto.com/posts/14927)
  * [源码Codelabs-chatWSDemo](../北向Codelabs)

+ [【FFH】OpenHarmony与HarmonyOS实现信息交流（一）](https://ost.51cto.com/posts/13846)

+ [【FFH】从零开始的鸿蒙机器学习之旅-NLP情感分析 ](https://ost.51cto.com/posts/13898)

+ [【FFH】OpenHarmony与HarmonyOS实现中文交流(二) ](https://ost.51cto.com/posts/1403)
  + [源码Codelabs-Serverless-random](../北向Codelabs)
+  [【FFH】华为智慧屏分布式语音音乐软件，可见即可说](https://ost.51cto.com/posts/14071)
  + [源码Codelabs-Voicemusicplayer](../北向Codelabs)

+ [【FFH】HarmonyOS手机遥控Dayu开发板相机(一)](https://ost.51cto.com/posts/14174)
  + [源码Codelabs-Dayu200camera](../北向Codelabs)

+ [【FFH】NFC碰一碰实战，拉起任何应用，无需企业认证！](https://ost.51cto.com/posts/14704)

+ [【FFH】JS实现华为账号授权服务，一键登录！](https://ost.51cto.com/posts/14771)
  + [源码Codelabs-huaweiAccount](../北向Codelabs)

+ [【FFH】这个盛夏，来一场“清凉”的代码雨！](https://ost.51cto.com/posts/14807)
  + [源码Codelabs-numrain](../北向Codelabs)

+ [【FFH】曲线救国，JS表情包卡片！](https://ost.51cto.com/posts/14940)
  + [源码Codelabs-Carddemo](../北向Codelabs)

+ [【FFH】从零实现原子服务一键分享，免安装！](https://ost.51cto.com/posts/15013)
  + [源码Codelabs-ShareCard](../北向Codelabs)

* [【FFH】canvas帧动画](https://ost.51cto.com/posts/13840)
  * [源码Codelabs-frameAnimation](../北向Codelabs)

* [【FFH】实现组件拖拽(ArkUI)](https://ost.51cto.com/posts/13882)

* [【FFH】Js API8之新增Dom的动态添加](https://ost.51cto.com/posts/14736)

* [【FFH】JS自定义组件：DIY一个随点随用的键盘！](https://ost.51cto.com/posts/15079)
  * [源码Codelabs-liteKeyboard](../北向Codelabs)

* [【FFH】ArkUI(JS)复刻手机备忘录(rdb数据库)](https://ost.51cto.com/posts/15269)
  * [源码Codelabs-memorandum](../北向Codelabs)