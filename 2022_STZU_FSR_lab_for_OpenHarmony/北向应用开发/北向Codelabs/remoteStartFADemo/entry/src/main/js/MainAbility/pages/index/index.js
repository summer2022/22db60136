import RemoteDeviceModel from"../../model/RemoteDeviceModel";
import featureAbility from '@ohos.ability.featureAbility';
export default {
    data: {
        DEVICE_LIST_LOCALHOST: "",
        deviceList:[],
        remoteDeviceModel: new RemoteDeviceModel()
    },
    onInit() {
        this.grantPermission()
        console.info('onInit begin');
        this.DEVICE_LIST_LOCALHOST = {
            name: '本机',
            id: 'localhost'
        };
        this.deviceList = [this.DEVICE_LIST_LOCALHOST];
        console.info('onInit end');
    },

    //获取用户权限
    grantPermission() {
        console.info(`FSRxxxxx grantPermission`)
        let context = featureAbility.getContext()
        context.requestPermissionsFromUser(['ohos.permission.DISTRIBUTED_DATASYNC'], 666, function (result) {
            console.info(`FSRxxxxx grantPermission,requestPermissionsFromUser`)
        })
    },
    //获取设备列表
    onContinueFAClick() {
        console.info('FSRxxxxx onContinueAbilityClick begin');
        const self = this;
        this.remoteDeviceModel.registerDeviceListCallback(() => {
            console.info('FSRxxxxx registerDeviceListCallback, callback entered');
            const list = [];
            //第一个设备默认本机
            list[0] = this.DEVICE_LIST_LOCALHOST;
            let deviceList;
            if (self.remoteDeviceModel.discoverList.length > 0) {
                deviceList = self.remoteDeviceModel.discoverList;
            } else {
                deviceList = self.remoteDeviceModel.deviceList;
            }
            console.info('FSRxxxxx on remote device updated, count=' + deviceList.length);
            for (let i = 0; i < deviceList.length; i++) {
                console.info('device ' + i + '/' + deviceList.length + ' deviceId='
                + deviceList[i].deviceId + ' deviceName=' + deviceList[i].deviceName + ' deviceType='
                + deviceList[i].deviceType);
                list[i + 1] = {
                    name: deviceList[i].deviceName,
                    id: deviceList[i].deviceId
                };
            }
            self.deviceList = list;
        });
        this.$element('continueFADialog').show();
        console.info('onContinueFAClick end');
    },
    startFAContinuation(deviceId, deviceName) {
        this.$element('continueFADialog').close();
        console.info('FSRxxxxx featureAbility.startAbility deviceId=' + deviceId
        + ' deviceName=' + deviceName);
        const wantValue = {
            bundleName: 'com.example.remotestartfademo',
            //这个要注意是packagename+ability名字
            abilityName: 'com.example.entry.MainAbility',
            deviceId: deviceId
        };
        //拉起目标设备FA
        featureAbility.startAbility({
            want: wantValue
        }).then((data) => {
            console.info('featureAbility.startAbility finished, ' + JSON.stringify(data));
        });
        //关闭本设备FA
        featureAbility.terminateSelf();
        console.info('featureAbility.startAbility want=' + JSON.stringify(wantValue));
        console.info('featureAbility.startAbility end');
    },
    onRadioChange(inputValue, e) {
        console.info('FSRxxxxx onRadioChange ' + inputValue + ', ' + e.value);
        if (inputValue === e.value) {
            for (let i = 0; i < this.remoteDeviceModel.deviceList.length; i++) {
                if (this.remoteDeviceModel.deviceList[i].deviceId === e.value) {
                    this.startFAContinuation(this.remoteDeviceModel.deviceList[i].deviceId, this.remoteDeviceModel.deviceList[i].deviceName);
                }
            }
        }
    },
    onDismissDialogClicked() {
        this.dismissDialog();
    },
    dismissDialog() {
        this.$element('continueFADialog').close();
        this.remoteDeviceModel.unregisterDeviceListCallback();
    },
}



