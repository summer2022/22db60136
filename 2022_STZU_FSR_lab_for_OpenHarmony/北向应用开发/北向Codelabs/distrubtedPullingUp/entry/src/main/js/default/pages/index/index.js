export default {
    data: {
        title: "",
        deviceList: [],
    },
    onInit() {
        this.title = "拉起设备";
    },
    // 获取并显示在线设备列表
    async showDeviceList() {
        let ret = await FeatureAbility.getDeviceList(0);
        this.deviceList = new Array();

        if (ret.code === 0) {
            for (let i = 0; i < ret.data.length; i++) {
                this.deviceList[i] = {
                    deviceName: ret.data[i].deviceName,
                    networkId: ret.data[i].networkId,
                    checked: false
                }
            }
        }

        this.$element('showDialog').show();
    },
    // 选择设备
    selectDevice(index, e) {
        this.deviceList[index].checked = e.checked;
    },

    // 拉起在线设备并传递参数
    async chooseComform() {
        this.$element('showDialog').close();

        for (let i = 0; i < this.deviceList.length; i++) {
            if (this.deviceList[i].checked) {
                //发送给拉起设备同步数据
                let actionData = {
                    //                    title: this.$t('strings.remote'),//改。。。
                    playDistributeDataList: this.playDistributeDataList
                };

                let target = {
                    bundleName: 'com.example.distrubtedpullingup',
                    abilityName: 'com.example.distrubtedpullingup.MainAbility',
                    url: 'pages/index/index',
                    networkId: this.deviceList[i].networkId,
                    data: actionData
                };

                await FeatureAbility.startAbility(target);
            }
        }
    },

    // 取消弹框
    chooseCancel() {
        this.$element('showDialog').close();
    },
}
