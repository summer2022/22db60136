import { ActionReady } from '../../common/model/dymaticPlay'
import prompt from '@system.prompt';

export default {
    data: {
        isShow: true,
        top: 0,
        left: 0,
        globalY: 0,
        globalX: 0,
        isAble: true,
        offsetY: 0,
        offsetX: 0,
        width: 640,
        height: 1024
    },

    onShow() { //---显示初始画面
        let ele = this.$refs.canvas_1;
        let ctx = ele.getContext('2d');
        this.offsetX = 0;
        this.offsetY = 0;
        let img_1 = new Image();
        img_1.src = 'common/images/eat/eat_01.jpg';
        ctx.drawImage(img_1, this.offsetX, this.offsetY, this.width, this.height);
    },

    play(e) {
        if (this.isAble) {
            this.showToast("开始播放");
            let id = e.target.id;
            let e1e = this.$refs.canvas_1; //---获取对应的画布
            let ctx = e1e.getContext('2d');
            let promise = ActionReady(id, ctx);
            this.isAble = false;
            promise.then((res) => {
                this.isAble = res;
                this.showToast("播放完毕");
            })
        }
    },

    showToast(mes) {
        prompt.showToast({
            message: mes,
            duration: 2000,
        })
    }
}



