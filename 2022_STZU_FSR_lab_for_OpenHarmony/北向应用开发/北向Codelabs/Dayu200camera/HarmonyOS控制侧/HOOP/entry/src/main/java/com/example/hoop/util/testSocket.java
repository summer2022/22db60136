package com.example.hoop.util;

import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.io.*;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

public class testSocket {
    private static final HiLogLabel LABEL_LOG = new HiLogLabel(3, 0xD000F00, "sockettest");
    private Context context;
    private byte [] revbuff;
    private int len=0;
    public testSocket(Context context){
      this.context = context;
    }
    public  byte [] getRevbuff(){
        return revbuff;
    }
    public  int getLen(){
        return len;
    }
    public  void testSocket(){

        Mythread.inBG(new Runnable() {

            @Override

            public void run() {

                System.out.println("----HiExecutor:在异步线程执行任务");

                Socket s = new Socket();

                try {

//                    Socket  socket = new Socket("192.168.0.103",10006);

                    Socket  socket = new Socket("10.161.116.158",10006);

                    s = socket;

                } catch (IOException e) {

                    e.printStackTrace();

                }

                // 建立连接后获得输出流

                try {

                    testSocket.this.readSpec(s);

                } catch (Exception e) {

                    e.printStackTrace();
                    HiLog.error(LABEL_LOG,"读取失败");

                }

//                HiExecutor.runUI(new Runnable() {
//
//                    @Override
//
//                    public void run() {
//
//                        System.out.println("----HiExecutor:在UI线程执行任务");
//                        HiLog.error(LABEL_LOG,"----HiExecutor:在UI线程执行任务");
//
//                    }
//
//                });

            }

        });

    }
    public void rec(){
        try (DatagramSocket socket = new DatagramSocket(10006)){
            DatagramPacket packet = new DatagramPacket(new byte[255], 255);
            while (true) {
                socket.receive(packet);
                String message = new String(packet.getData(), packet.getOffset(), packet.getLength(), "UTF-8");
                System.out.println(message);
            }
        } catch (IOException e) {
            e.printStackTrace();
            HiLog.error(LABEL_LOG, "开启失败");
        }
    }
    public void readSpec(Socket socket) throws Exception{

        byte[] b1 = new byte[10000];

        byte[] b2 = new byte[10000];

        revbuff = new byte[10000];



        OutputStream os = null;

        InputStream is = null;

        os = socket.getOutputStream();

        DataOutputStream dos = new DataOutputStream(os);

//        b1[0] = "1".getBytes(StandardCharsets.UTF_8);
//
//        b1[1] = 2;
//
//        b1[2] = 3;
//
//        b1[3] = 4;
//
//        b1[4] = 5;
//
//        b1[5] = 6;
//
//        b1[6] = 7;
//
//        b1[7] = 8;
        b1 = "1".getBytes(StandardCharsets.US_ASCII);
        byte[] buffer = "A NEW WORLD".getBytes("ASCII");
        int k = buffer.length;

        dos.write(buffer, 0,  k);

        is = socket.getInputStream();

        while(true){

            len = is.available();

            if(len != 0)

                break;

        }

        DataInputStream dis = new DataInputStream(is);

        dis.read(revbuff, 0, len);


        dos.flush();

        dos.close();



    }

}