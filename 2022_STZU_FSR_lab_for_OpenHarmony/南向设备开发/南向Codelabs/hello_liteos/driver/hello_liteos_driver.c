#include "hdf_device_desc.h"  // Header file that describes the APIs provided by the HDF to the driver.
#include "hdf_log.h"          // Header file that describes the log APIs provided by the HDF.
#include "gpio_if.h"
#define HDF_LOG_TAG hello_liteos_driver   // Tag contained in logs. If no tag is not specified, the default HDF_TAG is used.

// The driver service struct definition
struct ITestDriverService {
    struct IDeviceIoService ioService;       // The first member of the service structure must be a member of type IDeviceIoService
};

// The driver service interface must be bound to the HDF for you to use the service capability.
static int32_t HdfHelloLiteosDriverBind(struct HdfDeviceObject *deviceObject) {
    HDF_LOGI("hello_liteos driver bind success");
    return 0;
}

// Initialize the driver service.
static int32_t HdfHelloLiteosDriverInit(struct HdfDeviceObject *deviceObject) {
    HDF_LOGI("Hello LiteOS");
    //绿色LED亮
    GpioSetDir(19,GPIO_DIR_OUT);
    GpioWrite(19,GPIO_VAL_HIGH);
    return 0;
}

// Release the driver resources.
static void HdfHelloLiteosDriverRelease(struct HdfDeviceObject *deviceObject) {
    HDF_LOGI("hello_liteos driver Release success");
    return;
}

// Define the object of the driver entry. The object must be a global variable of the HdfDriverEntry type (defined in hdf_device_desc.h).
struct HdfDriverEntry g_hello_liteosDriverEntry = {
    .moduleVersion = 1,
    .moduleName = "hello_liteos_driver",
    .Bind = HdfHelloLiteosDriverBind,
    .Init = HdfHelloLiteosDriverInit,
    .Release = HdfHelloLiteosDriverRelease,
};

// Call HDF_INIT to register the driver entry with the HDF framework. When loading the driver, call the Bind function and then the Init function. If the Init function fails to be called, the HDF will call Release to release the driver resource and exit.
HDF_INIT(g_hello_liteosDriverEntry);
