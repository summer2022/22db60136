

#include <unistd.h>
#include "hi_wifi_api.h"
#include "lwip/ip_addr.h"
#include "lwip/netifapi.h"
#include "ohos_init.h"
#include "cmsis_os2.h"
// tcp
#include <stdio.h>
#include "lwip/sockets.h"
// io
#include "iot_gpio.h"
#include <hi_gpio.h>
#include <hi_io.h>
#define GPIO0 0
#define GPIO1 1
#define GPIO9 9
#define GPIO10 10

#define _PROT_ 8888
const char *IP_argv[] = {"192.168.43.61"};
char recv_buf[64];

#define APP_INIT_VAP_NUM 2
#define APP_INIT_USR_NUM 2

static struct netif *g_lwip_netif = NULL;

void my_car_init(void)
{
    IoTGpioInit(GPIO0);
    IoTGpioInit(GPIO1);
    IoTGpioInit(GPIO10);
    IoTGpioInit(GPIO9);

    hi_io_set_func(GPIO0, 0); //在hi_io.h里有定义
    hi_io_set_func(GPIO1, 0);
    hi_io_set_func(GPIO10, HI_IO_FUNC_GPIO_10_GPIO);
    hi_io_set_func(GPIO9, 0);

    IoTGpioSetDir(GPIO0, 1);
    IoTGpioSetDir(GPIO1, 1);
    IoTGpioSetDir(GPIO10, 1);
    IoTGpioSetDir(GPIO9, 1);

    IoTGpioSetOutputVal(GPIO0, IOT_GPIO_VALUE1);
    IoTGpioSetOutputVal(GPIO1, IOT_GPIO_VALUE1);
    IoTGpioSetOutputVal(GPIO9, IOT_GPIO_VALUE1);
    IoTGpioSetOutputVal(GPIO10, IOT_GPIO_VALUE1);
}

void car_backward(void)
{
    IoTGpioSetOutputVal(GPIO0, IOT_GPIO_VALUE1); //左
    IoTGpioSetOutputVal(GPIO1, IOT_GPIO_VALUE1);
    IoTGpioSetOutputVal(GPIO9, IOT_GPIO_VALUE1); //右轮
    IoTGpioSetOutputVal(GPIO10, IOT_GPIO_VALUE1);
}

void car_forward(void)
{
    IoTGpioSetOutputVal(GPIO0, IOT_GPIO_VALUE1);
    IoTGpioSetOutputVal(GPIO1, IOT_GPIO_VALUE0);
    IoTGpioSetOutputVal(GPIO9, IOT_GPIO_VALUE1);
    IoTGpioSetOutputVal(GPIO10, IOT_GPIO_VALUE0);
}

void car_left(void)
{
    IoTGpioSetOutputVal(GPIO0, IOT_GPIO_VALUE0);
    IoTGpioSetOutputVal(GPIO1, IOT_GPIO_VALUE0);
    IoTGpioSetOutputVal(GPIO9, IOT_GPIO_VALUE1);
    IoTGpioSetOutputVal(GPIO10, IOT_GPIO_VALUE0);
}

void car_right(void)
{
    IoTGpioSetOutputVal(GPIO0, IOT_GPIO_VALUE1);
    IoTGpioSetOutputVal(GPIO1, IOT_GPIO_VALUE0);
    IoTGpioSetOutputVal(GPIO9, IOT_GPIO_VALUE1);
    IoTGpioSetOutputVal(GPIO10, IOT_GPIO_VALUE1);
}

void car_stop(void)
{
    IoTGpioSetOutputVal(GPIO0, IOT_GPIO_VALUE1);
    IoTGpioSetOutputVal(GPIO1, IOT_GPIO_VALUE1);
    IoTGpioSetOutputVal(GPIO9, IOT_GPIO_VALUE1);
    IoTGpioSetOutputVal(GPIO10, IOT_GPIO_VALUE1);
}

/* 清除ip，网关和子网掩码 */
void hi_sta_reset_addr(struct netif *pst_lwip_netif)
{
    ip4_addr_t st_gw;
    ip4_addr_t st_ipaddr;
    ip4_addr_t st_netmask;

    if (pst_lwip_netif == NULL)
    {
        printf("hisi_reset_addr::Null param of netdev\r\n");
        return;
    }

    IP4_ADDR(&st_gw, 0, 0, 0, 0);
    IP4_ADDR(&st_ipaddr, 0, 0, 0, 0);
    IP4_ADDR(&st_netmask, 0, 0, 0, 0);

    netifapi_netif_set_addr(pst_lwip_netif, &st_ipaddr, &st_netmask, &st_gw);
}

/* 注册回调函数*/
void wifi_wpa_event_cb(const hi_wifi_event *hisi_event)
{
    if (hisi_event == NULL)
        return;

    switch (hisi_event->event)
    {
    case HI_WIFI_EVT_SCAN_DONE: // STA扫描完成
        printf("WiFi: Scan results available\n");
        break;
    case HI_WIFI_EVT_CONNECTED: // WIFI已连接
        printf("WiFi: Connected\n");
        netifapi_dhcp_start(g_lwip_netif);
        break;
    case HI_WIFI_EVT_DISCONNECTED: // WiFi断开连接
        printf("WiFi: Disconnected\n");
        netifapi_dhcp_stop(g_lwip_netif);
        hi_sta_reset_addr(g_lwip_netif);
        break;
    case HI_WIFI_EVT_WPS_TIMEOUT: // WPS事件超时
        printf("WiFi: wps is timeout\n");
        break;
    default:
        break;
    }
}

int hi_wifi_start_connect(void)
{
    int ret;
    errno_t rc;
    hi_wifi_assoc_request assoc_req = {0};

    /* copy SSID to assoc_req */
    rc = memcpy_s(assoc_req.ssid, HI_WIFI_MAX_SSID_LEN + 1, "M20P", 4);
    if (rc != EOK)
    {
        return -1;
    }

    //开放WIFI
    // assoc_req.auth = HI_WIFI_SECURITY_OPEN;

    // WIFI加密方式
    assoc_req.auth = HI_WIFI_SECURITY_WPA2PSK;

    /* WIFI密码 */
    memcpy(assoc_req.key, "12345678", 8);

    // WIFI连接
    ret = hi_wifi_sta_connect(&assoc_req);
    if (ret != HISI_OK)
    {
        return -1;
    }

    return 0;
}

int hi_wifi_start_sta(void)
{
    int ret;
    char ifname[WIFI_IFNAME_MAX_SIZE + 1] = {0};
    int len = sizeof(ifname);
    const unsigned char wifi_vap_res_num = APP_INIT_VAP_NUM;
    const unsigned char wifi_user_res_num = APP_INIT_USR_NUM;
    unsigned int num = WIFI_SCAN_AP_LIMIT;

    // ret = hi_wifi_init(wifi_vap_res_num, wifi_user_res_num); // WiFi初始化
    // if (ret != HISI_OK)
    // {
    //     return -1;
    // }

    ret = hi_wifi_sta_start(ifname, &len); // sta初始化
    if (ret != HISI_OK)
    {
        return -1;
    }

    /* 注册回调函数接口*/
    ret = hi_wifi_register_event_callback(wifi_wpa_event_cb);
    if (ret != HISI_OK)
    {
        printf("register wifi event callback failed\n");
    }

    /* 获取网络接口进行IP的操作 */
    g_lwip_netif = netifapi_netif_find(ifname);
    if (g_lwip_netif == NULL)
    {
        printf("%s: get netif failed\n", __FUNCTION__);
        return -1;
    }

    /* 扫描WIFI */
    ret = hi_wifi_sta_scan();
    if (ret != HISI_OK)
    {
        return -1;
    }

    sleep(5); /* sleep 5s, waiting for scan result. */

    //创建pst_results存放WiFi扫描结果
    hi_wifi_ap_info *pst_results = malloc(sizeof(hi_wifi_ap_info) * WIFI_SCAN_AP_LIMIT);
    if (pst_results == NULL)
    {
        return -1;
    }

    //获取station扫描结果
    ret = hi_wifi_sta_scan_results(pst_results, &num);
    if (ret != HISI_OK)
    {
        free(pst_results);
        return -1;
    }

    //打印WIFI扫描结果
    for (unsigned int loop = 0; (loop < num) && (loop < WIFI_SCAN_AP_LIMIT); loop++)
    {
        printf("SSID: %s\n", pst_results[loop].ssid);
    }
    free(pst_results);

    /* 进行WIFI连接 */
    ret = hi_wifi_start_connect();
    if (ret != 0)
    {
        return -1;
    }
    sleep(5);
    /**********************************************/
    struct sockaddr_in sock_addr = {0};

    int s;

    sock_addr.sin_family = AF_INET;
    sock_addr.sin_port = PP_HTONS(_PROT_);
    sock_addr.sin_addr.s_addr = inet_addr(IP_argv[0]);

    s = socket(AF_INET, SOCK_STREAM, 0);
    if (s < 0)
    {
        printf("socket false\n");
    }
    int ret_a;
    ret_a = connect(s, (struct sockaddr *)&sock_addr, sizeof(sock_addr));
    if (ret_a != 0)
    {
        printf("connect false\n");
    }

    my_car_init();
    while (1)
    {
        (void)memset_s(recv_buf, sizeof(recv_buf), 0, sizeof(recv_buf));
        ret_a = lwip_read(s, recv_buf, sizeof(recv_buf) - 1);
        printf("server:%d!\n", recv_buf[0]);

        if (recv_buf[0] == 49)
        {
            car_forward();
            printf("car_forward!\n");
        }
        else if (recv_buf[0] == 50)
        {
            car_backward();
            printf("car back!");
        }
        else if (recv_buf[0] == 51)
        {
            car_left();
        }
        else if (recv_buf[0] == 52)
        {
            car_right();
        }
    }

    return 0;
}

void hi_wifi_stop_sta(void)
{
    int ret;

    ret = hi_wifi_sta_stop();
    if (ret != HISI_OK)
    {
        printf("failed to stop sta\n");
    }

    ret = hi_wifi_deinit();
    if (ret != HISI_OK)
    {
        printf("failed to deinit wifi\n");
    }

    g_lwip_netif = NULL;
}

void *hi_wifi_text(const char *arg)
{
    printf("******************************\n");
    hi_wifi_start_sta();

    printf("******************************\n");
}

void wifi_car_demo(void)
{
    osThreadAttr_t attr;

    attr.name = "WIFITask";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = 4096;
    attr.priority = 25;

    if (osThreadNew((osThreadFunc_t)hi_wifi_text, NULL, &attr) == NULL)
    {
        printf("[LedExample] Falied to create LedTask!\n");
    }
}

SYS_RUN(wifi_car_demo);
